<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class Vendas extends Model {

    public $useTable = 'tbl_pedido';

    public $primaryKey = 'idPedido';

    public $displayField = 'idUsuarioFK';

    public $belongsTo = array(       
        
        'UsuarioFK' => array(

           'className' => 'Usuario', //className – define o model que será associado.
           'foreignKey' => 'idUsuarioFK', //foreignKey – define o nome da chave estrangeira encontrada no model atual. Isto é especialmente útil se você precisa definir múltiplos relacionamentos belongsTo.
           'conditions' => array(), //conditions – define as condições utilizadas em uma consulta SQL.
           'fields' => array('idUsuario', 'nome', 'tipoPessoa', 'cnpj', 'cpf', 'email'), //fields - lista de campos a serem recuperados quando os dados do model associado são coletados. Retorna todos os campos por padrão.
           'counterCache' => 'true', //counterCache e counterScope servem para contar registros. O uso é indicado para quem já tem uma certa experiência em CakePHP.
           'counterScope' => array(),
           'order' => array() //order – define a ordem de retorno das linhas associadas.

        ),

        'EnderecoFK' => array(

            'className' => 'Endereco', //className – define o model que será associado.
            'foreignKey' => 'idEnderecoFK', //foreignKey – define o nome da chave estrangeira encontrada no model atual. Isto é especialmente útil se você precisa definir múltiplos relacionamentos belongsTo.
            'conditions' => array(), //conditions – define as condições utilizadas em uma consulta SQL.
            'fields' => array('idEnderecos', 'Nome', 'cep', 'idEstadoFK', 'idCidadeFK', 'bairro', 'endereco', 'numero', 'dadosAdicionais', 'telefone'), //fields - lista de campos a serem recuperados quando os dados do model associado são coletados. Retorna todos os campos por padrão.
            'counterCache' => 'true', //counterCache e counterScope servem para contar registros. O uso é indicado para quem já tem uma certa experiência em CakePHP.
            'counterScope' => array(),
            'order' => array() //order – define a ordem de retorno das linhas associadas.
 
        ),

    );


    public function itensPedidos($situacao, $idUsuario){
        // Agenda.data                      AS Data,
        return($this->query("SELECT 
                                pedido.idPedido, 
                                pedProd.idPedidoProduto,
                                pedProd.quantidade, 
                                pedProd.precoUnitario, 
                                pedProd.precoTotal, 
                                pedProd.situacao, 
                                produto.idProdutos,
                                produto.nomeProduto
                            from 
                                tbl_pedido as pedido 
                                inner join tbl_pedido_produto as pedProd on pedido.idPedido = pedProd.idPedidoFK
                                inner join tbl_produto as produto on pedProd.idProdutoFK = produto.idProdutos 
                                inner join users on users.idUsuario = produto.idUsuarioFK  
                            where 
                                pedProd.situacao = '$situacao'
                                and users.idUsuario = $idUsuario "));  
    } 


    public function verificaEndereco($idUsuario){
        // Agenda.data                      AS Data,
        return($this->query("SELECT idenderecofk from tbl_carrinho as carrinho where idusuariofk = $idUsuario "));  
    } 



}
