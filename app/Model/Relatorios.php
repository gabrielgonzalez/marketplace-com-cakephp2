<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class Relatorios extends Model {

    public $useTable = 'tbl_pedido';

    public $primaryKey = 'idPedido';

    public $displayField = 'idUsuarioFK';

    public $belongsTo = array(       
        
        'UsuarioFK' => array(

           'className' => 'Usuario', //className – define o model que será associado.
           'foreignKey' => 'idUsuarioFK', //foreignKey – define o nome da chave estrangeira encontrada no model atual. Isto é especialmente útil se você precisa definir múltiplos relacionamentos belongsTo.
           'conditions' => array(), //conditions – define as condições utilizadas em uma consulta SQL.
           'fields' => array('idUsuario', 'nome', 'tipoPessoa', 'cnpj', 'cpf', 'email'), //fields - lista de campos a serem recuperados quando os dados do model associado são coletados. Retorna todos os campos por padrão.
           'counterCache' => 'true', //counterCache e counterScope servem para contar registros. O uso é indicado para quem já tem uma certa experiência em CakePHP.
           'counterScope' => array(),
           'order' => array() //order – define a ordem de retorno das linhas associadas.

        ),

        'EnderecoFK' => array(

            'className' => 'Endereco', //className – define o model que será associado.
            'foreignKey' => 'idEnderecoFK', //foreignKey – define o nome da chave estrangeira encontrada no model atual. Isto é especialmente útil se você precisa definir múltiplos relacionamentos belongsTo.
            'conditions' => array(), //conditions – define as condições utilizadas em uma consulta SQL.
            'fields' => array('idEnderecos', 'Nome', 'cep', 'idEstadoFK', 'idCidadeFK', 'bairro', 'endereco', 'numero', 'dadosAdicionais', 'telefone'), //fields - lista de campos a serem recuperados quando os dados do model associado são coletados. Retorna todos os campos por padrão.
            'counterCache' => 'true', //counterCache e counterScope servem para contar registros. O uso é indicado para quem já tem uma certa experiência em CakePHP.
            'counterScope' => array(),
            'order' => array() //order – define a ordem de retorno das linhas associadas.
 
        ),

    );


    public function rel001($dtInicio, $dtFim, $usuario){
        
        return($this->query("SELECT 
                                pedido.idPedido, pedido.precoTotal, pedido.formaPGTO, pedido.dtPedido,
                                users.idUsuario, users.nome, users.tipoPessoa, users.cnpj, users.cpf, users.rg, 
                                users.telefone, users.celular, users.email,
                                ende.Nome, ende.cep, ende.bairro, ende.endereco, ende.numero, ende.dadosAdicionais, 
                                cidade.nome, estado.nome
                            from 
                                tbl_pedido as pedido
                                inner join users on pedido.idUsuarioFK = users.idUsuario 
                                inner join tbl_endereco as ende on pedido.idEnderecoFK = ende.idEnderecos
                                inner join tbl_cidade as cidade on ende.idCidadeFK = cidade.idCidade
                                inner join tbl_estados as estado on ende.idEstadoFK = estado.codigoIBGE
                            where 
                                users.idUsuario  = $usuario
                                and pedido.dtPedido between '$dtInicio'and '$dtFim'
                            order by 
                                idPedido DESC "));  
    } 


    public function rel002($dtInicio, $dtFim, $usuario){
        
        return($this->query("SELECT 
                                ItensPedidos.idPedidoProduto, ItensPedidos.idPedidoFK, ItensPedidos.idProdutoFK, ItensPedidos.quantidade, 
                                ItensPedidos.precoUnitario, ItensPedidos.precoTotal, ItensPedidos.situacao, 
                                PedidoFK.idPedido, PedidoFK.idUsuarioFK, PedidoFK.idEnderecoFK, PedidoFK.dtPedido,  
                                ProdutoFK.idProdutos, ProdutoFK.nomeProduto, ProdutoFK.quantidade, 
                                ProdutoFK.preco, ProdutoFK.idUsuarioFK 
                            FROM 
                                tbl_pedido_produto AS ItensPedidos 
                                LEFT JOIN tbl_pedido AS PedidoFK ON (ItensPedidos.idPedidoFK = PedidoFK.idPedido) 
                                LEFT JOIN tbl_produto AS ProdutoFK ON (ItensPedidos.idProdutoFK = ProdutoFK.idProdutos) 
                            WHERE 
                                ProdutoFK.idUsuarioFK  = $usuario
                                and PedidoFK.dtPedido between '$dtInicio'and '$dtFim' 
                                and ItensPedidos.situacao <> 9
                            order by 
                                idPedido DESC"));  
    }


}
