<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class Painel extends Model {

    public $useTable = 'tbl_departamento';

    public $primaryKey = 'idPainel';

    public $displayField = 'descricao';


    
    public function totalItensVendidos($idUsuario){
        
        return($this->query("select sum(pedProd.quantidade) AS quantidade, sum(pedProd.precoTotal) as totalLiquido from 
                            tbl_pedido_Produto as pedProd 
                            inner join tbl_produto as produto on produto.idProdutos = pedProd.idProdutoFK 
                            where produto.idUsuarioFK = $idUsuario
                            and pedProd.situacao NOT IN (9) 
                            LIMIT 1 "));  
    } 


    public function resumoPedidos($mes, $ano, $user){
        
        return($this->query("SELECT 
                                count(*) as TotalPedidos,
                                YEAR(dtpedido) as AnoPedido,
                                MONTH(dtpedido) as MesPedido
                            from 
                                tbl_pedido as pedido 
                                inner join tbl_pedido_produto as pedProd on pedido.idPedido = pedProd.idPedidoFK
                                inner join tbl_produto as produto on pedProd.idProdutoFK = produto.idProdutos
                            where 
                                YEAR(dtpedido) = '$ano'
                                and MONTH(dtpedido) = '$mes' 
                                and produto.idUsuarioFK = $user  
                            group by AnoPedido, MesPedido
                            order by AnoPedido, MesPedido"));  
    }


    public function ItemMaisVendido($user){
        
        return($this->query("SELECT 
                                SUM(pedProd.quantidade) as Total, 
                                produto.idProdutos, 
                                produto.nomeProduto, 
                                produto.idUsuarioFK
                            from 
                                tbl_pedido_produto as pedProd 
                                inner join tbl_produto as produto on pedProd.idProdutoFK = produto.idProdutos
                            where 
                                produto.idUsuarioFK = $user
                            group by 
                                produto.idProdutos, 
                                produto.nomeProduto, 
                                produto.idUsuarioFK
                            order by 
                                Total DESC
                            LIMIT 1 	"));  
    }
    


}
