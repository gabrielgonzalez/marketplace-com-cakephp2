<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class Endereco extends Model {

    public $useTable = 'tbl_endereco';

    public $primaryKey = 'idEnderecos';

    public $displayField = 'opEndereco';

    public $belongsTo = array(                            

        'EstadoFK' => array(

           'className' => 'Estado', //className – define o model que será associado.
           'foreignKey' => 'idEstadoFK', //foreignKey – define o nome da chave estrangeira encontrada no model atual. Isto é especialmente útil se você precisa definir múltiplos relacionamentos belongsTo.
           'conditions' => array('EstadoFK.situacao' => 'A'), //conditions – define as condições utilizadas em uma consulta SQL.
           'fields' => array('codigoIBGE', 'idEstado', 'nome', 'situacao'), //fields - lista de campos a serem recuperados quando os dados do model associado são coletados. Retorna todos os campos por padrão.
           'counterCache' => 'true', //counterCache e counterScope servem para contar registros. O uso é indicado para quem já tem uma certa experiência em CakePHP.
           'counterScope' => array(),
           'order' => array() //order – define a ordem de retorno das linhas associadas.

        ),

        'CidadeFK' => array(

            'className' => 'Cidades', //className – define o model que será associado.
            'foreignKey' => 'idCidadeFK', //foreignKey – define o nome da chave estrangeira encontrada no model atual. Isto é especialmente útil se você precisa definir múltiplos relacionamentos belongsTo.
            'conditions' => array('CidadeFK.situacao' => 'A'), //conditions – define as condições utilizadas em uma consulta SQL.
            'fields' => array('idCidade', 'nome', 'codigoIBGE'), //fields - lista de campos a serem recuperados quando os dados do model associado são coletados. Retorna todos os campos por padrão.
            'counterCache' => 'true', //counterCache e counterScope servem para contar registros. O uso é indicado para quem já tem uma certa experiência em CakePHP.
            'counterScope' => array(),
            'order' => array() //order – define a ordem de retorno das linhas associadas.
 
         ),

    );


    public $virtualFields = array(
        'opEndereco' => 'CONCAT(Endereco.endereco, ", ", Endereco.numero)'
    );

}
