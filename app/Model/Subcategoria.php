<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class Subcategoria extends Model {

    public $useTable = 'tbl_subcategoria';

    public $primaryKey = 'idSubCategoria';

    public $displayField = 'descricao';

    public $belongsTo = array(                            

        'CategoriaFK' => array(

           'className' => 'Categoria', //className – define o model que será associado.
           'foreignKey' => 'idCategoriaFK', //foreignKey – define o nome da chave estrangeira encontrada no model atual. Isto é especialmente útil se você precisa definir múltiplos relacionamentos belongsTo.
           'conditions' => array(), //conditions – define as condições utilizadas em uma consulta SQL.
           'fields' => array('idCategoria', 'descricao', 'descricaoReduzida', 'idDepartamentoFK'), //fields - lista de campos a serem recuperados quando os dados do model associado são coletados. Retorna todos os campos por padrão.
           'counterCache' => 'true', //counterCache e counterScope servem para contar registros. O uso é indicado para quem já tem uma certa experiência em CakePHP.
           'counterScope' => array(),
           'order' => array() //order – define a ordem de retorno das linhas associadas.

        ),

    );


    public function menu(){
        // Agenda.data                      AS Data,
        return($this->query("SELECT
                    dep.idDepartamento, 
                    dep.descricao, 
                    cat.idCategoria,
                    cat.descricao,
                    subcat.idSubCategoria,
                    subcat.descricao,
                    subcat.descricaoReduzida
                from tbl_departamento as dep 
                LEFT join tbl_categoria as cat on dep.idDepartamento = cat.idDepartamentoFK 
                LEFT join tbl_subcategoria as subcat on cat.idCategoria = subcat.idCategoriaFK  "));  
    } 


    public function Randon(){
        // Agenda.data                      AS Data,
        return($this->query("SELECT 
                            Sub.idSubCategoria, Sub.descricao, Cat.idCategoria, Cat.descricao, Dep.idDepartamento, Dep.descricao 
                        FROM 
                            tbl_subcategoria AS Sub
                            inner join tbl_categoria as Cat on Sub.idCategoriaFK = Cat.idCategoria 
                            inner join tbl_departamento as Dep ON Cat.idDepartamentoFK = Dep.idDepartamento
                        ORDER BY RAND()
                        LIMIT 1  "));  
    }

    

}
