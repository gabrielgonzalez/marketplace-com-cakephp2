<section class="page-section-pt">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <div class="section-title text-center">
                    <span>Sobre o projeto</span>
                    <h3 class="text-center">Seja bem vindo ao S-commerce</h3>
                </div>
            </div>
            <div class="col-md-8 text-center">
                <p class="mb-2">Este projeto tem como finalidade apresentar uma ferramenta de controle e auxílio para a criação de lojas virtuais, diminuindo gastos e aumentando o lucro de qualquer negócio que utilize essa ferramenta.</p>
                <p>Com o simples objetivo de trazer mais praticidade para os usuários e lucros para seus negócios, muitos empreendedores estão investindo nas lojas virtuais para aumentarem suas vendas e a satisfação de seus clientes. </p>
            </div> 
        </div>
        <div class="about-image">
            <img class="img-fluid center-block" src="/scommerce/img/capa2.jpg" alt="">
        </div>
    </div>
</section>