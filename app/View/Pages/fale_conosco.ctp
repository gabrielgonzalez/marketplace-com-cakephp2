<?php echo $this->Form->create('FaleConosco', array('url' => array('controller' => 'Pages', 'action' => 'fale_conosco'), 
                                                                            'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                'label' => false,
                                                                                                                'class' => 'form-control'
                                                                                                            )) ); ?>

    <section class="page-section-pb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mb-4">Envie sua mensagem</h3>
                </div>
                <div class="col-md-12">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo $this->Form->input('nome', array('label' => 'Seu Nome', 'type' => 'text', 'class' => 'form-control input-default', 'required') ); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo $this->Form->input('email', array('label' => 'Email', 'type' => 'email', 'class' => 'form-control input-default', 'required') ); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?php echo $this->Form->input('Menssagem', array('label' => 'Menssagem', 'type' => 'textarea', 'class' => 'form-control input-default', 'required') ); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button id="submit" name="submit" type="submit" value="Send" class="button"><span>Enviar Mensagem</span></button>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

<?= $this->Form->end() ?> 