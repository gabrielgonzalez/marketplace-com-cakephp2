<!--=================================
intro-title -->
<section class="intro-title bg bg-overlay-black-70" style="background:url(/scommerce/img/capa7.jpg) fixed;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="intro-content">
                        <div class="intro-name">
                            <h3 class="text-white">S Commerce</h3>
                            <h6 class="text-white">Um jeito fácil de fazer negócio</h6>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

<!--=================================
intro-title -->

    <section class="page-section-ptb gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <h3 class="mb-2">O céu é o limite</h3>
                    <p class="mb-3">Temos a missão de construir, crescer e manter comunidades leais. Isso significa que você pode alcançar seus objetivos de negócios através da nossa ferramenta.</p>
                    <div class="accordion icon">
                        <div class="acd-group acd-active">
                            <a href="#" class="acd-heading acd-active"><span class="ti-bar-chart-alt text-blue"></span>Estamos sempre prontos para te ajudar.</a>
                            <div class="acd-des" style="display: block;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>
                        <div class="acd-group">
                            <a href="#" class="acd-heading text-black"><span class="ti-pulse text-blue"></span> Nós entregamos as melhores classificações.</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>
                        <div class="acd-group">
                            <a href="#" class="acd-heading text-black"><span class="ti-shield text-blue"></span> Alta taxa de retenção de clientes.</a>
                            <div class="acd-des" style="display: none;">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 text-center">
                    <?php echo  $this->Html->image("img5.jpg", array( 'class'=>"img-fluid"));?>
                </div>
            </div>
        </div>
    </section>

<!--=================================
welcome -->

  <section class="shop grid page-section-ptb">
        <div class="container">
            <div class="row">
             <div class="col-md-3 order-md-2">
                <div class="sidebar-widgets-wrap mt-0">
                     <div class="sidebar-widget">
                     <h5>Encontre um produto</h5>
                        <div class="widget-search">
                            <i class="fa fa-search"></i> 
                            <?php echo $this->Form->create('Pesquisa', array('url' => array('controller' => 'Pages', 'action' => 'pesquisa'), 
                                                                                        'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                            'label' => false,
                                                                                                                            'class' => 'form-control'
                                                                                                                        )) ); ?> 

                                <div class="modal-body">                                                                    
                                                                                                                                                        
                                    <div class="basic-form">
                                        
                                        <div class="form-row">
                                            <div class="form-group col-md-9">
                                                
                                                <?php echo $this->Form->input('pesquisa', array('label'=>false, 'type'=>"text", 'class' => 'form-control placeholder', 'placeholder'=> "Pesquisar...." )); ?> 

                                            </div>

                                            <div class="form-group col-md-3">
                                                
                                                <button type="submit" title="Buscar" class="button ti-search"> </button>
                                                
                                            </div>
                                        </div>
                                        
                                    </div> 
                                                                                        
                                </div>                 

                            <?= $this->Form->end() ?>   
                        </div>
                 </div>
                <div class="sidebar-widget">
                
                    
                 </div>
               <div class="sidebar-widget">
                    <h5>Adicionados Recentemente</h5>
                
                    <?php foreach ($AddRecent as $key => $Recent) { ?>

                        <div class="recent-item clearfix">
                            <div class="recent-image">
                                <?php echo $this->Html->link($this->Html->image("/Imagens/".$Recent['FotosProdutoFK']['0']['caminho'], array( 'border' => '0', 'width'=>'60', 'height'=>'60')), array( 'controller' => 'Pages', 'action' => 'view', $Recent['Anuncios']['idProdutos']), array('escape' => false)); ?>
                            </div>
                            <div class="recent-info">
                                <div class="recent-title">
                                    <?php echo $this->Html->link($Recent['Anuncios']['nomeProduto'], array( 'controller' => 'Pages', 'action' => 'view', $Recent['Anuncios']['idProdutos']), array('escape' => false)); ?>
                                </div>
                                <div class="recent-meta">
                                    <ul class="list-style-unstyled">
                                        <li class="color"> 
                                            <?php 
                                            
                                                $preco = str_replace(".",",", $Recent['Anuncios']['preco']); 

                                                echo 'R$ '. $preco;
                                            
                                            ?> 
                                        </li>                                        
                                    </ul>    
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                    
                </div>
                                
                </div>
               </div>
                <div class="col-md-9 mt-md-0 mt-3">

                    <div class="row"> 

                        <div class='intro-name'> 

                            <ul class="breadcrumb mt-1">
                                <li class="breadcrumb-item"><a href="#"><?php echo $randon['0']['Dep']['descricao']; ?></a></li>
                                <li class="breadcrumb-item text-black"><?php echo $randon['0']['Cat']['descricao']; ?> </li>
                                <li class="breadcrumb-item active"><?php echo $randon['0']['Sub']['descricao']; ?></li>
                            </ul>                        

                        </div>

                    </div>                    

                    <div class="row">

                        <?php foreach ($produtos as $key => $produto) { ?>
                            
                            <div class="col-md-4">
                                <div class="product text-center mb-4">
                                    <div class="product-image">
                                        <img class="img-fluid center-block" src="/scommerce/Imagens/<?php echo $produto['FotosProdutoFK']['0']['caminho']; ?>" alt="">
                                        <div class="product-overlay">
                                            <div class="add-to-cart">
                                                <?php echo $this->Html->link('Conferir', array( 'controller' => 'Pages', 'action' => 'view', $produto['Anuncios']['idProdutos']), array('escape' => false)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-des">
                                        <div class="product-title">
                                            <?php echo $this->Html->link($produto['Anuncios']['nomeProduto'], array( 'controller' => 'Pages', 'action' => 'view', $produto['Anuncios']['idProdutos']), array('escape' => false)); ?>
                                        </div>
                                    </div>
                                    <div class="product-price">
                                        <ins>
                                            <?php 
                                            
                                                $preco = str_replace(".",",", $produto['Anuncios']['preco']); 

                                                echo 'R$ '. $preco;
                                            
                                            ?>
                                    
                                        </ins>
                                    </div>
                                </div>
                            </div>
                            
                        <?php } ?> 
                        
                    </div>
                </div>
                </div>
               </div>
              </div>

            </div>
        </div>
    </section>
 
<!--=================================
welcome -->