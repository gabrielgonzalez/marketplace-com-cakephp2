<div id="loading">
    <div id="loading-center">
        <img src="/scommerce/img/loader4.gif" alt="">
    </div>
</div>

<?php if (isset($produtos)) { ?>

    <section class="blog-page page-section-ptb">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                
                    <?php 
                    $disabled = '';
                    $totalItens = 0;
                    $total = 0;
                    
                    foreach ($produtos as $key => $produto) { 
                        
                        $valorTotal = $produto['ProdutoFK']['preco'] * $produto['Carrinho']['quantidade'];
                        
                        ?>
                        
                        <div class="blog-entry without-image mb-5">
                            <div class="blog-detail">
                                <div class="entry-title mb-1">
                                    <h3><?php echo $produto['ProdutoFK']['nomeProduto']; ?> </h3>
                                </div>
                                <div class="entry-content">

                                    <div class='row'>

                                        <div class='col-md-3'> 

                                            <img class="img-fluid center-block" src="/scommerce/Imagens/<?php echo $produto['Img']['caminho']; ?>" alt="">

                                        </div> 

                                        <div class='col-md-9'> 
                                            
                                            <p>Cód.: <?php echo $produto['ProdutoFK']['idProdutos']; ?> </p>      

                                            <p>Preço: R$ <?php echo number_format($produto['ProdutoFK']['preco'], 2, ',', '.'); ?> </p>     
                                                                                
                                                <div class="input-group">

                                                
                                                    <span class="input-group-btn">
                                                        <?php echo $this->Html->link('<span class="ti-minus"></span>', array( 'controller' => 'Pages', 'action' => 'verificar_Item_menos', $produto['Carrinho']['idCarrinho']), array('escape' => false, 'class' => 'btn mb-1 btn-rounded btn-info'));  ?>
                                                    </span>

                                                    <div class='col-md-2'>
                                                        <input type="text" id='quantidade<?php echo $produto['ProdutoFK']['idProdutos']; ?>' value="<?php echo $produto['Carrinho']['quantidade']; ?>" class="form-control input-number" value="1" min="1" max="<?php echo $produto['ProdutoFK']['quantidade']; ?>" readonly>                        

                                                        <input type="hidden" id='qtdMax<?php echo $produto['ProdutoFK']['idProdutos']; ?>' value="<?php echo $produto['ProdutoFK']['quantidade']; ?>">
                                                    </div>

                                                    <span class="input-group-btn">
                                                        <?php echo $this->Html->link('<span class="ti-plus"></span>', array( 'controller' => 'Pages', 'action' => 'verificar_Item_mais', $produto['Carrinho']['idCarrinho']), array('escape' => false, 'class' => 'btn mb-1 btn-rounded btn-info'));  ?>
                                                    </span> 
                                                    
                                                </div>

                                                <div class="product-detail-meta">

                                                    <?php if ($produto['Carrinho']['quantidade'] > $produto['ProdutoFK']['quantidade']) { 
                                                        $disabled = 'disabled';    
                                                    ?>
                                                        <p><span class='text-red'>A quantidade de itens selecionado não esta disponivel no estoque </span></p>
                                                    <?php } ?>
                                                    
                                                </div>

                                            <?php 

                                                $totalItens += $produto['Carrinho']['quantidade'];                                                

                                            ?>                                             

                                        </div>


                                    </div>                                    
                                
                                </div>
                                
                                <hr>

                                <div class="entry-share clearfix">
                                    <div class="entry-button">
                                        R$ <span id='valorTotal'> <?php echo number_format($valorTotal, 2, ',', '.'); ?> </span>
                                    </div>
                                    <div class="social list-style-none pull-right">
                                        
                                        <ul>
                                            <li>
                                                <?php echo $this->Html->link('<h3> <i class="fa fa-times"></i> </h3>', array( 'controller' => 'Pages', 'action' => 'remove_cart', $produto['Carrinho']['idCarrinho']), array('escape' => false, 'title' => 'Clique para remover este item da lista')); ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php 
                        $total += $valorTotal;
                    } ?>
                    
                    <!-- ============================================ -->
                    
                </div>
                <div class="col-md-3 order-2">
                    <div class="sidebar-widgets-wrap">
                        <div class="sidebar-widget">
                            
                            <h5>Resumo da Venda</h5> 

                            <div class="sidebar-widget">
                                
                                <div class="recent-item clearfix">                                
                                    
                                    Total de Itens: <?php echo $totalItens; ?>
                                    
                                    <br>

                                    Subtotal: <?php echo number_format($total, 2, ',', '.'); ?> 

                                    <br>

                                    valor total: <?php echo number_format($total, 2, ',', '.'); ?>

                                </div>

                            </div>

                            <hr>

                            <div class="sidebar-widget">
                                <?php echo $this->Html->link('Finalizar Compra', array( 'controller' => 'Pedidos', 'action' => 'add'), array('escape' => false, 'class' => "btn mb-1 btn-success  $disabled"));  ?>
                                <br>
                                <?php echo $this->Html->link('Continuar Comprando', array( 'controller' => 'Pages', 'action' => 'home'), array('escape' => false, 'class' => 'btn mb-1 btn-warning'));  ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php } else { ?> 

    <section class="blog-page page-section-ptb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                
                    <div class="blog-entry without-image mb-5">
                        <div class="blog-detail">
                            <div class="entry-title mb-1">
                                <h3> Seu carrinho está vazio </h3>
                            </div>
                            <div class="entry-content">

                                <p>Adicione produtos clicando no botão “Comprar” na página de produtos.</p>                                 
                                
                                <br>

                                <?php echo $this->Html->link('Voltar para a página inicial', array( 'controller' => 'Pages', 'action' => 'home'), array('escape' => false, 'class' => 'button border-bule'));  ?>
                            
                            </div>
                        </div>
                    </div>
                    
                    <!-- ============================================ -->
                    
                </div>
            </div>
        </div>
    </section>

<?php } ?>

