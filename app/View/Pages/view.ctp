<hr>

<!--=================================
shop -->

<section class="shop-single page-section-ptb gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6">
                        <div class="slider-slick">
                            <div class="slider slider-for detail-big-car-gallery">

                                <?php foreach ($FotosProduto as $key => $Foto) { ?>
                                    <img class="img-fluid" src="/scommerce/Imagens/<?php echo $Foto['Img']['caminho']; ?>" alt="">
                                <?php } ?>

                            </div>
                            <div class="slider slider-nav"> 

                                <?php foreach ($FotosProduto as $key => $Foto) { ?>
                                    <img class="img-fluid" src="/scommerce/Imagens/<?php echo $Foto['Img']['caminho']; ?>" alt="" width='60' height='60'>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="product-detail clearfix">
                            <div class="product-detail-title mb-2">
                                <h4 class="mb-1"> <?php echo $produto['Anuncios']['nomeProduto']; ?> </h4>
                            </div>
                            <div class="clearfix mb-3">
                                <div class="product-detail-price">
                                    <ins><?php echo 'R$ '. $produto['Anuncios']['preco']; ?> </h4></ins>
                                </div>
                                <div class="product-detail-rating pull-right">
                                    <!-- <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                    <i class="fa fa-star-o"></i> -->
                                </div>
                            </div>
                            <div class="product-detail-quantity clearfix mb-3">

                                <?php if ($produto['Anuncios']['quantidade'] > 0) { ?>
                                    
                                    <?php echo $this->Form->create('Carrinho',  array('url' => array('controller' => 'pages', 'action' => 'add_cart'), 
                                                                                'class' => 'form-horizontal'
                                                                                    ), array('inputDefaults' => array(
                                                                                                                    'label' => false,
                                                                                                                    'class' => 'form-control'
                                                                                                                )) ); ?>

                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-number" data-type="minus" onclick="quantMenos();">
                                                    <span class="ti-minus"></span>
                                                </button>
                                            </span>

                                            <input type="text" id='quantidade' name="data[Carrinho][quantidade]" class="form-control input-number" value="1" min="1" max="<?php echo $produto['Anuncios']['quantidade']; ?>" readonly>                        

                                            <input type="hidden" id='qtdMax' value="<?php echo $produto['Anuncios']['quantidade']; ?>">

                                            <input type="hidden" name="data[Carrinho][idProdutoFK]" value="<?php echo $produto['Anuncios']['idProdutos']; ?>">

                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-number" data-type="plus" onclick="quantMais();">
                                                    <span class="ti-plus"></span>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="product-detail add-to-cart">
                                            <button type="submit" class="btn btn-primary px-3 ml-4">Comprar</button>
                                        </div>
                                        </div>

                                    <?php echo $this->Form->end(); ?>
                                
                                <?php } else { ?>

                                    <p><span class='text-red'>Item fora de estoque</span></p>

                                <?php } ?>  

                                
                                    
                            </div>
                            <div class="product-detail-meta">
                                <span>Código: <?php echo $produto['Anuncios']['idProdutos']; ?> </span>
                                <span>Categoria: <a href="#"> <?php echo $produto['SubcategoriaFK']['descricao']; ?> </a>  </span>
                            </div>
                            <div class="product-detail-social">
                                
                                <?php 
                                    ///vERIFICO SE O PRODUTO ESTA ADICIONADO NA LISTA DE DESEJOS
                                    if (!empty($listado)) {
                                        echo $this->Html->link('', array( 'controller' => 'ListaDesejos', 'action' => 'remove', $listado['ListaDesejos']['idListaDesejo'], $produto['Anuncios']['idProdutos'] ), array('escape' => false, 'class' => 'btn mb-1 btn-rounded btn-danger ti-heart', 'title' => 'Remover da Lista de Desejos')); 
                                    } else {
                                        echo $this->Html->link('', array( 'controller' => 'ListaDesejos', 'action' => 'add', $produto['Anuncios']['idProdutos'] ), array('escape' => false, 'class' => 'btn mb-1 btn-rounded btn-info ti-heart', 'title' => 'Adicionar a Lista de Desejos')); 
                                    }                    
                                    
                                ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="tab">
                        <ul class="nav nav-tabs simple" role="tablist">
                            <li role="presentation">
                                <a class="active" href="#tab-01" aria-controls="tab-01" role="tab" data-toggle="tab"> Descrição</a>
                            </li>
                            <li role="presentation">
                                <a href="#tab-02" aria-controls="tab-02" role="tab" data-toggle="tab"> Avaliações (<?php echo count($Avaliacoes); ?>)</a>
                            </li>
                        </ul>
                        <!-- tab-content -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab-01">
                                <h6><b>Informações adicionais do produto</b></h6>   
                                
                                <p class="mt-2"><?php echo $produto['Anuncios']['descricao']; ?></p>
                                
                            </div> 
                            <div role="tabpanel" class="tab-pane" id="tab-02">
                                <div class="blog-comments mt-4">

                                    <?php if (!empty($Avaliacoes)) { 
                                        
                                        foreach ($Avaliacoes as $key => $avaliacao) { ?>

                                            <div class="comments-1">
                                                <div class="comments-photo">
                                                    <?php 
                                                        if (!empty($avaliacao['UsuarioFK']['fotoPerfil'])) {

                                                            echo $this->Html->image("avatar/".$avaliacao['UsuarioFK']['fotoPerfil'], array(
                                                                'width'=>"80", 'height'=>"40"
                                                            ));

                                                        } else {

                                                            echo $this->Html->image("avatar/sem_foto.jpg", array(
                                                                'width'=>"80", 'height'=>"40"
                                                            ));

                                                        }
                                                        
                                                    ?>
                                                </div>
                                                <div class="comments-info">
                                                    <h6> <?php echo $avaliacao['UsuarioFK']['nome']; ?> <span> <?php echo date('d/m/Y', strtotime($avaliacao['Avaliacoes']['dtCriacao'])); ?> </span></h6>
                                                    <div class="port-post-social pull-right">                                            
                                                    </div>
                                                    <blockquote class="default">
                                                        <p class="mt-1"><?php echo $avaliacao['Avaliacoes']['descricao']; ?></p>
                                                    </blockquote>
                                                    
                                                </div>
                                            </div>

                                    <?php }
                                    } else { ?>

                                        <h6><b>Este produto não tem avaliações</b></h6>   

                                    <?php } ?>
                                    
                                    <hr>
                                    
                                    <div class="comments review-button text-right">

                                        <?php echo $this->Form->create('Avaliacao',  array('url' => array('controller' => 'Avaliacoes', 'action' => 'add', $produto['Anuncios']['idProdutos']), 
                                                                    'class' => 'form-horizontal'
                                                                        ), array('inputDefaults' => array(
                                                                                                        'label' => false,
                                                                                                        'class' => 'form-control'
                                                                                                    )) ); ?>
                                            <div class="form-group">                                    
                                                <?php echo $this->Form->input('descricao', array('label'=>false, 'type'=>"textarea", 'class' => 'form-control placeholder', 'cols'=>"30", 'rows'=>"2", 'placeholder'=> "Nos conte o que achou do produto" )); ?> 
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <button type="submit" class="btn btn-primary px-3 ml-4">Adicionar Avaliação</button>
                                            </div>
                                        
                                        <?php echo $this->Form->end(); ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div> 
                    </div>

                    <div class="col-md-12">
                        <div class="title mt-3 mb-3">
                            <h6>Produtos relacionados</h6>
                        </div>
                        <div class="owl-carousel" data-nav-dots="false" data-nav-arrow="true" data-items="3" data-sm-items="2" data-lg-items="3" data-md-items="3" data-xs-items="2" data-autoplay="false">
                            
                            <?php foreach ($Relacionados as $key => $Relacionado) {  ?>
                                
                                <div class="item">
                                    <div class="product">
                                        <div class="product-image">
                                            <img class="img-fluid center-block" src="/scommerce/Imagens/<?php echo $Relacionado['FotosProdutoFK']['0']['caminho']; ?>" alt="">
                                            <div class="product-overlay">
                                                <div class="add-to-cart">
                                                    <?php echo $this->Html->link('Conferir', array( 'controller' => 'Pages', 'action' => 'view', $Relacionado['Anuncios']['idProdutos']), array('escape' => false)); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-des">
                                            <div class="product-title">
                                                <?php echo $this->Html->link($Relacionado['Anuncios']['nomeProduto'], array( 'controller' => 'Pages', 'action' => 'view', $Relacionado['Anuncios']['idProdutos']), array('escape' => false)); ?>
                                            </div>
                                        </div>
                                        <div class="product-price">
                                            <ins>
                                                <?php 
                                                        
                                                    $preco = str_replace(".",",", $Relacionado['Anuncios']['preco']); 

                                                    echo 'R$ '. $preco;
                                                
                                                ?>
                                            </ins>
                                        </div>
                                    </div>
                                </div> 
                            <?php } ?>

                            
                    
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="sidebar-widgets-wrap">
                    <div class="sidebar-widget">
                        <h5>Encontre um produto</h5>
                        <div class="widget-search">
                            <i class="fa fa-search"></i> 
                            <?php echo $this->Form->create('Pesquisa', array('url' => array('controller' => 'Pages', 'action' => 'pesquisa'), 
                                                                                        'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                            'label' => false,
                                                                                                                            'class' => 'form-control'
                                                                                                                        )) ); ?> 

                                <div class="modal-body">                                                                    
                                                                                                                                                        
                                    <div class="basic-form">
                                        
                                        <div class="form-row">
                                            <div class="form-group col-md-9">
                                                
                                                <?php echo $this->Form->input('pesquisa', array('label'=>false, 'type'=>"text", 'class' => 'form-control placeholder', 'placeholder'=> "Pesquisar...." )); ?> 

                                            </div>

                                            <div class="form-group col-md-3">
                                                
                                                <button type="submit" title="Buscar" class="button ti-search"> </button>
                                                
                                            </div>
                                        </div>
                                        
                                    </div> 
                                                                                        
                                </div>                 

                            <?= $this->Form->end() ?>   
                        </div>
                    </div>
                    
                    <div class="sidebar-widget">
                        <h5>Adicionados Recentemente</h5>

                        <?php foreach ($AddRecent as $key => $Recent) { ?>

                            <div class="recent-item clearfix">
                                <div class="recent-image">
                                    <?php echo $this->Html->link($this->Html->image("/Imagens/".$Recent['FotosProdutoFK']['0']['caminho'], array( 'border' => '0', 'width'=>'60', 'height'=>'60')), array( 'controller' => 'Pages', 'action' => 'view', $Recent['Anuncios']['idProdutos']), array('escape' => false)); ?>
                                </div>
                                <div class="recent-info">
                                    <div class="recent-title">
                                        <?php echo $this->Html->link($Recent['Anuncios']['nomeProduto'], array( 'controller' => 'Pages', 'action' => 'view', $Recent['Anuncios']['idProdutos']), array('escape' => false)); ?>
                                    </div>
                                    <div class="recent-meta">
                                        <ul class="list-style-unstyled">
                                            <li class="color"> 
                                                <?php 
                                                
                                                    $preco = str_replace(".",",", $Recent['Anuncios']['preco']); 

                                                    echo 'R$ '. $preco;
                                                
                                                ?> 
                                            </li>                                        
                                        </ul>    
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>        
    </div>
</section>

<!--=================================
shop -->



<!-- ***************** ADICIONAR ITEM **************** -->
<div class="modal fade" id="add_item" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false" >    
    <div id="AddItem">
            
    </div>
</div> 

<script>

    function quantMenos() {

        var quantidade = document.getElementById('quantidade').value;

        if (quantidade > 1) {

            document.getElementById('quantidade').value = quantidade - 1;

        } else {

            document.getElementById('quantidade').value = 1;

        }
        
    }

    function quantMais() {

        var quantidade = parseInt(document.getElementById('quantidade').value);
        
        var qtdMax = parseInt(document.getElementById('qtdMax').value);

        quantidade++;

        if (quantidade <= qtdMax) {

            document.getElementById('quantidade').value = quantidade;

        }        
        
    }

</script>