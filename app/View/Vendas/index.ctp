<div class="content-body">

    <div class="container-fluid mt-3">
        <div class='row'>

            <div class='col-md-12'>

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Produtos Vendidos</h4>
                        <!-- Nav tabs -->
                        <div class="custom-tab-1">
                            <ul class="nav nav-tabs mb-3">
                                <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#tab1">Itens para Provação (<?php echo count($itensPendentes); ?>)</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab2">Itens Aguardando Faturamento (<?php echo count($itensFaturar); ?>)</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab3">Itens Aguardando Envio (<?php echo count($itensEnviar); ?>)</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab4">Itens em transito (<?php echo count($itensEnviados); ?>)</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab5">Itens Entregues (<?php echo count($itensEntregues); ?>)</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="tab1" role="tabpanel">
                                    <div class="p-t-15">
                                        <h4>Itens aguardando aprovação</h4>
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th scope="col" width='30%'> </th>
                                                    <th scope="col" width='5%'><?= $this->Paginator->sort('idPedido', 'Nº Pedido') ?></th>
                                                    <th scope="col" width='30%'><?= $this->Paginator->sort('UsuarioFK.nome', 'Nome Produto') ?></th>
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('dtPedido', 'Quantidade') ?></th>                                                
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('precoUnit', 'Preço Unit.') ?></th>
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('precoTotal', 'Preço Total') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($itensPendentes as $Pedido): ?>
                                                    <tr>
                                                        <td class="actions">
                                                            
                                                            <!-- Botão para visualizar registro -->
                                                            <?php echo $this->Html->link(
                                                                'Detalhes', array("controller"=> 'Pedidos', "action" => "view", $Pedido['pedProd']['idPedidoProduto']), 
                                                                array(                                                  
                                                                    'class' => 'btn mb-1 btn-outline-warning', 
                                                                    'target' => '_blank',
                                                                    'escape' => false
                                                                )
                                                            );?>

                                                            <button type="button" class="btn mb-1 btn-outline-info" data-toggle="modal" data-target="#aprovacao<?php echo $Pedido['pedProd']['idPedidoProduto']; ?>">Aprovar</button>

                                                        </td>
                                                        <td><?= $this->Number->format($Pedido['pedido']['idPedido']) ?></td>
                                                        <td><?= h($Pedido['produto']['nomeProduto']) ?></td>
                                                        <td><?= h($Pedido['pedProd']['quantidade']); ?></td>                                                    
                                                        <td><?= h($Pedido['pedProd']['precoUnitario']) ?></td> 
                                                        <td><?= h($Pedido['pedProd']['precoTotal']) ?></td> 
                                                    </tr> 

                                                    <div class="modal fade" id="aprovacao<?php echo $Pedido['pedProd']['idPedidoProduto']; ?>">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Pedido Nº <?php echo $Pedido['pedProd']['idPedidoProduto']; ?></h5>
                                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Produto: <?= h($Pedido['produto']['nomeProduto']) ?> 
                                                                    <br>
                                                                    Quantidade: <?= h($Pedido['pedProd']['quantidade']); ?>  
                                                                    <br>
                                                                    Preço Total do item: <?= h($Pedido['pedProd']['precoTotal']) ?>                                                                     
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                                                    
                                                                    <?php echo $this->Html->link(
                                                                            '<i class="ti-close"> Rejeitar</i>', array("action" => "sit_pendente", $Pedido['pedProd']['idPedidoProduto'], '9'), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-danger', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>

                                                                    <?php echo $this->Html->link(
                                                                            '<i class="ti-check"> Aprovar</i>', array("action" => "sit_pendente", $Pedido['pedProd']['idPedidoProduto'], '1'), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-info', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab2">
                                    <div class="p-t-15">
                                        <h4>Itens aguardando o faturamento</h4>
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th scope="col" width='30%'> </th>
                                                    <th scope="col" width='5%'><?= $this->Paginator->sort('idPedido', 'Nº Pedido') ?></th>
                                                    <th scope="col" width='30%'><?= $this->Paginator->sort('UsuarioFK.nome', 'Nome Produto') ?></th>
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('dtPedido', 'Quantidade') ?></th>                                                
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('precoUnit', 'Preço Unit.') ?></th>
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('precoTotal', 'Preço Total') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($itensFaturar as $Pedido): ?>
                                                    <tr>
                                                        <td class="actions">
                                                            
                                                            <!-- Botão para visualizar registro -->
                                                            <?php echo $this->Html->link(
                                                                'Detalhes do Item', array("action" => "view", $Pedido['pedProd']['idPedidoProduto']), 
                                                                array(                                                  
                                                                    'class' => 'btn mb-1 btn-outline-warning', 
                                                                    'escape' => false
                                                                )
                                                            );?>

                                                            <button type="button" class="btn mb-1 btn-outline-info" data-toggle="modal" data-target="#faturar<?php echo $Pedido['pedProd']['idPedidoProduto']; ?>">Faturar</button>

                                                        </td>
                                                        <td><?= $this->Number->format($Pedido['pedido']['idPedido']) ?></td>
                                                        <td><?= h($Pedido['produto']['nomeProduto']) ?></td>
                                                        <td><?= h($Pedido['pedProd']['quantidade']); ?></td>                                                    
                                                        <td><?= h($Pedido['pedProd']['precoUnitario']) ?></td> 
                                                        <td><?= h($Pedido['pedProd']['precoTotal']) ?></td> 
                                                    </tr>

                                                    <div class="modal fade" id="faturar<?php echo $Pedido['pedProd']['idPedidoProduto']; ?>">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Pedido Nº <?php echo $Pedido['pedProd']['idPedidoProduto']; ?></h5>
                                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Produto: <?= h($Pedido['produto']['nomeProduto']) ?> 
                                                                    <br>
                                                                    Quantidade: <?= h($Pedido['pedProd']['quantidade']); ?>  
                                                                    <br>
                                                                    Preço Total do item: <?= h($Pedido['pedProd']['precoTotal']) ?>                                                                     
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                                                    
                                                                    <?php echo $this->Html->link(
                                                                            '<i class="ti-close"> Cancelar</i>', array("action" => "sit_pendente", $Pedido['pedProd']['idPedidoProduto'], '9'), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-danger', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>

                                                                    <?php echo $this->Html->link(
                                                                            '<i class="ti-check"> Faturar</i>', array("action" => "sit_pendente", $Pedido['pedProd']['idPedidoProduto'], '2'), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-info', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab3">
                                    <div class="p-t-15">
                                        <h4>Itens aguardando o envio para o destinatário </h4>
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th scope="col" width='30%'> </th>
                                                    <th scope="col" width='5%'><?= $this->Paginator->sort('idPedido', 'Nº Pedido') ?></th>
                                                    <th scope="col" width='30%'><?= $this->Paginator->sort('UsuarioFK.nome', 'Nome Produto') ?></th>
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('dtPedido', 'Quantidade') ?></th>                                                
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('precoUnit', 'Preço Unit.') ?></th>
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('precoTotal', 'Preço Total') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($itensEnviar as $Pedido): ?>
                                                    <tr>
                                                        <td class="actions">
                                                            
                                                            <!-- Botão para visualizar registro -->
                                                            <?php echo $this->Html->link(
                                                                'Detalhes do Item', array("action" => "view", $Pedido['pedProd']['idPedidoProduto']), 
                                                                array(                                                  
                                                                    'class' => 'btn mb-1 btn-outline-warning', 
                                                                    'escape' => false
                                                                )
                                                            );?>

                                                            <button type="button" class="btn mb-1 btn-outline-info" data-toggle="modal" data-target="#enviar<?php echo $Pedido['pedProd']['idPedidoProduto']; ?>">Despachar</button>

                                                        </td>
                                                        <td><?= $this->Number->format($Pedido['pedido']['idPedido']) ?></td>
                                                        <td><?= h($Pedido['produto']['nomeProduto']) ?></td>
                                                        <td><?= h($Pedido['pedProd']['quantidade']); ?></td>                                                    
                                                        <td><?= h($Pedido['pedProd']['precoUnitario']) ?></td> 
                                                        <td><?= h($Pedido['pedProd']['precoTotal']) ?></td> 
                                                    </tr>

                                                    <div class="modal fade" id="enviar<?php echo $Pedido['pedProd']['idPedidoProduto']; ?>">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Pedido Nº <?php echo $Pedido['pedProd']['idPedidoProduto']; ?></h5>
                                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Produto: <?= h($Pedido['produto']['nomeProduto']) ?> 
                                                                    <br>
                                                                    Quantidade: <?= h($Pedido['pedProd']['quantidade']); ?>  
                                                                    <br>
                                                                    Preço Total do item: <?= h($Pedido['pedProd']['precoTotal']) ?>                                                                     
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                                                    
                                                                    <?php echo $this->Html->link(
                                                                            '<i class="ti-check"> Item despachado</i>', array("action" => "sit_pendente", $Pedido['pedProd']['idPedidoProduto'], '3'), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-info', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab4">
                                    <div class="p-t-15">
                                        <h4>Itens enviados para o destinatário </h4>
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th scope="col" width='30%'> </th>
                                                    <th scope="col" width='5%'><?= $this->Paginator->sort('idPedido', 'Nº Pedido') ?></th>
                                                    <th scope="col" width='30%'><?= $this->Paginator->sort('UsuarioFK.nome', 'Nome Produto') ?></th>
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('dtPedido', 'Quantidade') ?></th>                                                
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('precoUnit', 'Preço Unit.') ?></th>
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('precoTotal', 'Preço Total') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($itensEnviados as $Pedido): ?>
                                                    <tr>
                                                        <td class="actions">
                                                            
                                                            <!-- Botão para visualizar registro -->
                                                            <?php echo $this->Html->link(
                                                                'Detalhes do Item', array("action" => "view", $Pedido['pedProd']['idPedidoProduto']), 
                                                                array(                                                  
                                                                    'class' => 'btn mb-1 btn-outline-warning', 
                                                                    'escape' => false
                                                                )
                                                            );?>
                                                            
                                                            <button type="button" class="btn mb-1 btn-outline-info" data-toggle="modal" data-target="#confirmaEntrega<?php echo $Pedido['pedProd']['idPedidoProduto']; ?>">Confirmar Entrega</button>

                                                        </td>
                                                        <td><?= $this->Number->format($Pedido['pedido']['idPedido']) ?></td>
                                                        <td><?= h($Pedido['produto']['nomeProduto']) ?></td>
                                                        <td><?= h($Pedido['pedProd']['quantidade']); ?></td>                                                    
                                                        <td><?= h($Pedido['pedProd']['precoUnitario']) ?></td> 
                                                        <td><?= h($Pedido['pedProd']['precoTotal']) ?></td> 
                                                    </tr>

                                                    <div class="modal fade" id="confirmaEntrega<?php echo $Pedido['pedProd']['idPedidoProduto']; ?>">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Pedido Nº <?php echo $Pedido['pedProd']['idPedidoProduto']; ?></h5>
                                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Produto: <?= h($Pedido['produto']['nomeProduto']) ?> 
                                                                    <br>
                                                                    Quantidade: <?= h($Pedido['pedProd']['quantidade']); ?>  
                                                                    <br>
                                                                    Preço Total do item: <?= h($Pedido['pedProd']['precoTotal']) ?>                                                                     
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                                                    
                                                                    <?php echo $this->Html->link(
                                                                            '<i class="ti-check"> Confirmar Entrega</i>', array("action" => "sit_pendente", $Pedido['pedProd']['idPedidoProduto'], '4'), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-info', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab5">
                                    <div class="p-t-15">
                                        <h4>Itens entregues para o destinatário </h4>
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th scope="col" width='30%'> </th>
                                                    <th scope="col" width='5%'><?= $this->Paginator->sort('idPedido', 'Nº Pedido') ?></th>
                                                    <th scope="col" width='30%'><?= $this->Paginator->sort('UsuarioFK.nome', 'Nome Produto') ?></th>
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('dtPedido', 'Quantidade') ?></th>                                                
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('precoUnit', 'Preço Unit.') ?></th>
                                                    <th scope="col" width='10%'><?= $this->Paginator->sort('precoTotal', 'Preço Total') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($itensEntregues as $Pedido): ?>
                                                    <tr>
                                                        <td class="actions">
                                                            
                                                            <!-- Botão para visualizar registro -->
                                                            <?php echo $this->Html->link(
                                                                'Detalhes do Item', array("action" => "view", $Pedido['pedProd']['idPedidoProduto']), 
                                                                array(                                                  
                                                                    'class' => 'btn mb-1 btn-outline-warning', 
                                                                    'escape' => false
                                                                )
                                                            );?>

                                                        </td>
                                                        <td><?= $this->Number->format($Pedido['pedido']['idPedido']) ?></td>
                                                        <td><?= h($Pedido['produto']['nomeProduto']) ?></td>
                                                        <td><?= h($Pedido['pedProd']['quantidade']); ?></td>                                                    
                                                        <td><?= h($Pedido['pedProd']['precoUnitario']) ?></td> 
                                                        <td><?= h($Pedido['pedProd']['precoTotal']) ?></td> 
                                                    </tr>

                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>

        </div>

    </div>

</div>

