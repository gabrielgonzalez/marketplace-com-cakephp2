<?php echo $this->Form->create('Users', array('url' => array('controller' => 'Users', 'action' => 'add'), 
                                                                            'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                'label' => false,
                                                                                                                'class' => 'form-control'
                                                                                                            )) ); ?> 

<br><br>
    <div class="login-form-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content">
                        <div class="card login-form mb-0">
                            <div class="card-body pt-5">
                                
                                <h4>Registre uma conta</h4>
        
                                <form class="mt-5 mb-5 login-input">
                                    <div class="form-group">
                                        <?php echo $this->Form->input('nome', array('label'=>false, 'type'=>"text", 'class' => 'form-control', 'placeholder'=> "Nome Completo", 'required' )); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('email', array('label'=>false, 'type'=>"email", 'class' => 'form-control', 'placeholder'=> "Email", 'required' )); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('username', array('label'=>false, 'type'=>"text", 'class' => 'form-control', 'placeholder'=> "Usuário", 'required' )); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('password', array('label'=>false, 'type'=>"password", 'class' => 'form-control', 'placeholder'=> "Senha", 'required' )); ?>
                                    </div>
                                    <button class="btn login-form__btn submit w-100">Criar</button>
                                </form>
                                    <p class="mt-5 login-form__footer">Já possui uma conta? <?php echo $this->Html->link('Clique aqui',
                                        array(
                                            'controller' => 'Users',
                                            'action' => 'login'
                                        ),
                                        array('class' => 'text-center')
                                    ); ?> para acessar</p>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php echo $this->Form->end(); ?>