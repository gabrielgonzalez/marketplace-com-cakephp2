<?php echo $this->Flash->render('auth'); ?>
<?php echo $this->Form->create('User');?>

<br><br>
    <div class="login-form-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content">
                        <div class="card login-form mb-0">
                            <div class="card-body pt-5">
                                
                                <h4>Login</h4>
        
                                <form class="mt-5 mb-5 login-input">
                                    <div class="form-group">
                                        <?php echo $this->Form->input('username', array('label'=>'Usuário', 'type'=>"text", 'class' => 'form-control', 'placeholder'=> "Usuário", 'required' )); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('password', array('label'=>'Senha', 'type'=>"password", 'class' => 'form-control', 'placeholder'=> "Senha", 'required' )); ?>
                                    </div>
                                    <button class="btn login-form__btn submit w-100">Login</button>
                                </form>
                                    <p class="mt-5 login-form__footer">Não possuí conta? <?php echo $this->Html->link('Clique aqui',
                                        array(
                                            'controller' => 'Users',
                                            'action' => 'add'
                                        ),
                                        array('class' => 'text-center')
                                    ); ?> para criar uma.</p>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php echo $this->Form->end(); ?>