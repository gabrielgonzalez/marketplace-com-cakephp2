<div class="content-body">

    <div class="container-fluid mt-3">
    
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body pb-0 d-flex justify-content-between">
                                <div>
                                    <h4 class="mb-1">Histórico de Itens Vendidos</h4>
                                    
                                </div>
                                <div>
                                    <ul>
                                        <li class="d-inline-block mr-3">

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            
                            <hr>

                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    
                                    <?php echo $this->Form->create('Relatorios',  array('url' => array('controller' => 'Relatorios', 'action' => 'rel002_imprimir'),
                                                  'class' => 'form-horizontal', 'target' => '_blank') ); ?>

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                A partir de: <br>
                                                <input id="date1" type="date" class='form-control input-default' name='data[Relatorios][dtInicio]'>
                                            </div>

                                            <div class="form-group col-md-6">
                                                Até: <br>
                                                <input id="date2" type="date" class='form-control input-default' name='data[Relatorios][dtFim]'>
                                            </div>
                                        </div>

                                        <div class='form-row'>

                                            <button type="submit" class="btn mb-1 btn-outline-info"><i class="ti-check"> Imprimir</i></button> 
                                        
                                        </div>

                                    <?= $this->Form->end() ?>  

                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-between">
                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>

</div>
