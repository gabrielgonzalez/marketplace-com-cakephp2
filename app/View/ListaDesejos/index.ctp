<!--=================================
intro-title -->
<section class="intro-title bg bg-overlay-black-70" style="background:url(/scommerce/img/capa7.jpg) fixed;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="intro-content">
                        <div class="intro-name">
                            <h3 class="text-white">Lista de Desejos</h3>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

<!--=================================
intro-title -->

<!--=================================
welcome -->

  <section class="shop grid page-section-ptb">
        <div class="container">
            <div class="row">
             
                <div class="col-md-12 mt-md-0 mt-3">

                    <h3>Esta é a sua lista de desejos</h3>
                    <h6>Seus produtos favoritos ficam aqui para você ver sempre que quiser.</h6>

                    <hr>

                    <div class="row">

                        <?php foreach ($produtos as $key => $produto) { ?>
                            
                            <div class="col-md-4">
                                <div class="product text-center mb-4">
                                    <div class="product-image">
                                        <img class="img-fluid center-block" src="/scommerce/Imagens/<?php echo $produto['FotosProdutoFK']['0']['caminho']; ?>" alt="">
                                        <div class="product-overlay">
                                            <div class="add-to-cart">
                                                <?php echo $this->Html->link('<i class="icon-basket-loaded menu-icon"></i> Comprar', array( 'controller' => 'Pages', 'action' => 'view', $produto['Anuncios']['idProdutos']), array('escape' => false)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-des">
                                        <div class="product-title">
                                            <?php echo $this->Html->link($produto['Anuncios']['nomeProduto'], array( 'controller' => 'Pages', 'action' => 'view', $produto['Anuncios']['idProdutos']), array('escape' => false)); ?>
                                        </div>
                                    </div>
                                    <div class="product-price">
                                        <ins>
                                            <?php 
                                            
                                                $preco = str_replace(".",",", $produto['Anuncios']['preco']); 

                                                echo 'R$ '. $preco;
                                            
                                            ?>
                                    
                                        </ins>
                                    </div>
                                </div>
                            </div>
                            
                        <?php } ?> 
                        
                    </div>
                </div>
                </div>
               </div>
              </div>

            </div>
        </div>
    </section>
 
<!--=================================
welcome -->