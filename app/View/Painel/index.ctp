<!--**********************************
    Content body start
***********************************-->
    <div class="content-body">

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="card gradient-1">
                        <div class="card-body">
                            <h3 class="card-title text-white">Itens com Pedidos realizados</h3>
                            <div class="d-inline-block">
                                <h2 class="text-white"><?php echo $totalItensVendidos['quantidade']; ?></h2>
                                <p class="text-white mb-0"><?php echo date('d/m/Y'); ?></p>
                            </div>
                            <span class="float-right display-5 opacity-5"><i class="fa fa-shopping-cart"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card gradient-2">
                        <div class="card-body">
                            <h3 class="card-title text-white">Lucro Líquido</h3>
                            <div class="d-inline-block">
                                <h2 class="text-white">$ <?php echo $totalItensVendidos['totalLiquido']; ?></h2>
                                <p class="text-white mb-0"><?php echo date('d/m/Y'); ?></p>
                            </div>
                            <span class="float-right display-5 opacity-5"><i class="fa fa-money"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card gradient-3">
                        <div class="card-body">
                            <h3 class="card-title text-white">Pedidos pendentes de aprovação</h3>
                            <div class="d-inline-block">
                                <h2 class="text-white"><?php echo $NovosItens; ?></h2>
                                <p class="text-white mb-0"><?php echo date('d/m/Y'); ?></p>
                            </div>
                            <span class="float-right display-5 opacity-5"><i class="fa fa-check"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card gradient-4">
                        <div class="card-body">
                            <h3 class="card-title text-white">Pedidos Entregues</h3>
                            <div class="d-inline-block">
                                <h2 class="text-white"><?php echo $ItensEntregues; ?></h2>
                                <p class="text-white mb-0"><?php echo date('d/m/Y'); ?></p>
                            </div>
                            <span class="float-right display-5 opacity-5"><i class="fa fa-box"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Resumo dos Pedidos por mês</h4>
                                <div id="bar-pedidos" style="height: 250px;"></div>
                            </div>
                        </div>
                        
                    </div>    
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title px-4 mb-3">Total de Itens Cadastrados</h5>
                                <h2 class="mt-4">
                                    <?php 
                                        if (!empty($ItensCadastrados)) {
                                            echo $ItensCadastrados; 
                                        } else {
                                            echo '0';
                                        }
                                    ?>
                                </h2>
                                <span>Total de itens cadastrados para vendas</span>
                            
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body px-0">
                                <h4 class="card-title px-4 mb-3">Item com mais pedidos</h4>
                                <h5 class="px-4 mb-3">
                                    <?php
                                        if (!empty($ItemMaisVendido)) {
                                            echo $ItemMaisVendido['0']['produto']['nomeProduto']; 
                                        } else {
                                            echo '0';
                                        }                                        
                                    ?>
                                </h5>
                                <h6 class="px-4 mb-3 text-muted">
                                    Com um total de  <span>
                                    <?php if (!empty($ItemMaisVendido)) {echo $ItemMaisVendido['0']['0']['Total'];}else{echo '0';}; ?>
                                    </span> unidades vendidas
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
        <!-- #/ container -->
    </div>
<!--**********************************
    Content body end
***********************************-->


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>


<script>

    <?php echo $resumoPedidos; ?>

</script>