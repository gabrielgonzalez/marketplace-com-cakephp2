<div class="content-body" style="min-height: 804px;">

    <div class="container-fluid">
        <div class="row">
        
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Itens do pedido</h4>
                        <div class="bootstrap-modal">
                        
                            <table class="table table-bordered table-striped verticle-middle">
                                <thead>
                                    <tr>
                                        <th scope="col">Cód.</th>
                                        <th scope="col">Nome do item</th>
                                        <th scope="col">Preço Unit.</th>
                                        <th scope="col">Quantidade</th>
                                        <th scope="col">Valor total</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php 
                                    $valorTotal = 0;
                                    foreach ($produtos as $key => $produto) { 
                                        $valorTotal += $produto['ProdutoFK']['preco'] * $produto['Carrinho']['quantidade'];
                                    ?>
                                        
                                        <tr>
                                            <td><?php echo  $produto['ProdutoFK']['idProdutos']; ?></td>
                                            <td><?php echo  $produto['ProdutoFK']['nomeProduto']; ?></td>
                                            <td><?php echo  number_format($produto['ProdutoFK']['preco'], 2, ',', '.'); ?></td>
                                            <td><?php echo  $produto['Carrinho']['quantidade']; ?></td>
                                            <td><?php echo  'R$ '.number_format(($produto['ProdutoFK']['preco'] * $produto['Carrinho']['quantidade']), 2, ',', '.'); ?></td>
                                        </tr>

                                    <?php } ?>
                                    
                                </tbody>
                            </table>
                        
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Endereço de entrega</h4>
                        
                        <p> 
                            <b>Endereço:</b> <?php echo $Endereco['Endereco']['endereco'].', '.$Endereco['Endereco']['numero']; ?> <br>
                            <b>Bairro:</b> <?php echo $Endereco['Endereco']['bairro']; ?> <br>
                            <b>Cidade:</b> <?php echo $Endereco['CidadeFK']['nome'].'/'.$Endereco['EstadoFK']['idEstado']; ?> <br>
                            <b>CEP:</b> <?php echo $Endereco['Endereco']['cep']; ?> <br>
                            <b>Informações Adicionais:</b> <?php echo $Endereco['Endereco']['dadosAdicionais']; ?> <br>
                        </p>

                        <hr>

                        <button type="button" class="btn mb-1 btn-light" data-toggle="modal" data-target=".bd-endereco-modal-lg">Alterar endereço</button>
                        <div class="modal fade bd-endereco-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Selecione o novo endereço de entrega</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                        </button>
                                    </div>

                                    <?php echo $this->Form->create('atualizaEndereco', array('url' => array('controller' => 'Pedidos', 'action' => 'atualizaEndereco'), 
                                                                            'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                'label' => false,
                                                                                                                'class' => 'form-control'
                                                                                                            )) ); ?> 

                                        <div class="modal-body">
                                            <?php echo $this->Form->input('endereco', array('label' => 'Endereços cadastrados<span class="text-danger">*</span>', 'options' => $optionsEndereco, 'type' => 'select', 'class' => 'form-control input-default', 'required') ); ?>
                                            <br>
                                            
                                            <?php echo $this->Html->link(
                                                '<i class="ti-plus"> Informar um novo endereço</i>', array("action" => "add_endereco"), 
                                                array(                                                  
                                                    'class' => 'btn mb-1 btn-outline-primary', 
                                                    'escape' => false
                                                )
                                            );?>
                                            
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn mb-1 btn-danger" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn mb-1 btn-outline-info"><i class="ti-check"> Confirmar Alteração</i></button> 
                                        </div>

                                    <?= $this->Form->end() ?> 
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        
        </div>

        <div class="row">
        
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Escolha a forma de pagamento</h4>
                        
                            <div class="default-tab">
                                <ul class="nav nav-tabs mb-3" role="tablist">
                                    <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#home">Boleto bancário</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile">PagSeguro</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active show" id="home" role="tabpanel">
                                        <div class="p-t-15">
                                            <h4>Boleto Bancário</h4>
                                            <p>Total de itens: <?php echo count($produtos); ?> </p>
                                            <p>Valor total do Boleto: <?php echo $valorTotal; ?> </p>
                                            <p>Data de Vencimento: 
                                                <?php $timestamp = strtotime(date('Y-m-d') . "+7 days");
                                                echo date('d/m/Y', $timestamp); ?> 
                                            </p>
                                            
                                            <?php echo $this->Html->link(
                                                'Concluir pedido', array("action" => "confirmar_pedido", 1), 
                                                array(                                                  
                                                    'class' => 'btn mb-1 btn-outline-primary', 
                                                    'escape' => false
                                                )
                                            );?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="profile">
                                        <div class="p-t-15">
                                            <h4>Desculpe mas esta opção será uma integração futura.</h4>
                                            <p>Mas se preferir, clique em concluir pedido para continuar a apresentação.</p>

                                            <?php echo $this->Html->link(
                                                'Concluir pedido', array("action" => "confirmar_pedido", 2), 
                                                array(                                                  
                                                    'class' => 'btn mb-1 btn-outline-primary', 
                                                    'escape' => false
                                                )
                                            );?>

                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #/ container -->
</div>
