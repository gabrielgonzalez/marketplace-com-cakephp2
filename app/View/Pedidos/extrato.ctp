<?php

$server   = $_SERVER['SERVER_NAME']; //HTTP_REFERER'];  // $endereco = $_SERVER ['SERVER_SOFTWARE'];
$endereco = $_SERVER['REQUEST_URI'];
$endereco = str_replace("Pedidos/extrato/$idPedido", "", $endereco);
$link = 'http://'.$server.$endereco.'img/Logo_scommerce.png';
 

date_default_timezone_set('America/Sao_Paulo');
$hoje             = date('d/m/Y');
$HRA              = date("H:i:s");
$file_type = 'pdf';
$filename  = 'RelatorioConsulta'.date('d/m/Y H:i:s');

$html = '<!DOCTYPE html>
        <html>
            <head>
                ' . $this->Html->charset() . '
                <style type="text/css">

                    @page { margin: 10px 50px 30px; }

                    #header { position: fixed; left: 0px; top: -10px; right: 0px; height: 60px; width:60px;  }
                    #footer { position: fixed; left: 0px; bottom: 0; right: 0px; height: 30px;  }
                    #footer .page:after { content: counter(page, upper-roman); }                                        

                    #header table,
                    #footer table {
                            width: 800px;
                            border-collapse: collapse;
                            border: none;
                    }

                    #header td,
                    #footer td {
                        padding: 0;
                            width: 100%;
                    }


                    .row {
                            margin-right: -15px;
                            margin-left: -15px;  
                            }

                    .col-xs-6 {
                                width: 50%;
                                }

                    .cab_titulo {     
                                    font-family:"Times New Roman", Times, serif;
                                    font-weight: bold;
                                    font-size: 15px;
                                    text-align: center;
                                }
                    .cab_texto {
                                font-family:"Times New Roman", Times, serif;
                                font-size: 12px;
                            }
                    .titulo {
                                font-family: arial, helvetica, serif;
                                font-size: 12px;
                                font-weight: bold;
                                
                                
                            }
                    .titulo_tab {
                                font-family: arial, helvetica, serif;
                                font-size: 10px;
                                font-weight: bold;
                                text-align: left; 
                                background-color: #808080 ;
                                color: #fff;
                                
                            }
                    .texto {
                                font-family: arial, helvetica, serif;
                                font-size: 10px;
                                
                            } 
                                
                    .texto2 {
                                font-family: arial, helvetica, serif;
                                font-size: 10px;
                                
                                background-color: #E4E4E4;
                            }
                    .texto3 {
                                font-family: arial, helvetica, serif;
                                font-size: 12px;
                                
                            }
                    .texto_total {
                                font-family: arial, helvetica, serif;
                                font-size: 12px;
                                font-weight: bold;
                                text-align: right;                    
                            }

                    .total_geral {
                                font-family: arial, helvetica, serif;
                                font-size: 12px;
                                font-weight: bold;
                                text-align: right;                    
                                
                            }        


                    .table {
                            width: 700px;
                            margin-bottom: 20px;
                            
                            }



                </style>                
            </head>

            <body>';
            
            $l=1;
            if (!empty($pedido)) { 

                if ($pedido['pedido']['idPedido'] == '1') {
                    $formaPagamento = 'Boleto';
                } else {
                    $formaPagamento = 'PagSeguro';
                }

                $html .='<table border="0" class="table" width="100%">
                    <tr>
                        <td rowspan="4" style="width:150px;"><img src="'.$link.'" width="100px%"/></td>
                    </tr>

                    <tr>
                        <td class="titulo" style="text-align: center" colspan="3">S-commerce</td>                                                
                    </tr>
                    <tr>
                        <td class="cab_texto" colspan="4" style="text-align: right"> Data e Hora de emissão do relatório: '.$hoje.' - '.$HRA.'</td>          
                    </tr>
                    <tr>
                        <td class="titulo" style="text-align: center" colspan="3"><br/>Extrato do pedido Nº '.$pedido['pedido']['idPedido'].' </td>                                                
                    </tr>
                </table>
                
                <hr>

                <table class="table">
                    <tr> 
                        <td class="texto">
                            <b> Nº Pedido: </b> '.$pedido['pedido']['idPedido'].'
                        </td>    
                        <td class="texto">
                            <b> Preço Total: </b> <br/>'.$pedido['pedido']['precoTotal'].'
                        </td>   
                        <td class="texto">
                            <b> Forma de Pagamento: <br/></b> '.$formaPagamento.'
                        </td>  
                        <td class="texto">
                            <b> Data do Pedido: </b> <br/>'.date("d/m/Y H:i:s", strtotime($pedido['pedido']['dtPedido'])).'
                        </td>                  
                    </tr> 

                    <tr> 
                        <td class="texto">
                            <b> Cliente: </b> <br/>'.$pedido['users']['idUsuario'].' - '.$pedido['users']['nome'].'
                        </td>    
                        <td class="texto"> ';
                        
                            if ($pedido['users']['tipoPessoa']  == 'PF') {

                                $html .='<b> CPF: </b> <br/>'.$pedido['users']['cpf'].' <br/>
                                <b> RG: </b> <br/>'.$pedido['users']['rg'] ;

                            } else {

                                $html .='<b> CNPJ: </b> <br/>'.$pedido['users']['cnpj'];

                            }                        
                        
                        $html .='
                        </td>   
                        <td class="texto">
                            <b> Telefone: </b> '.$pedido['users']['telefone'].' <br/>
                            <b> Celular: </b> '.$pedido['users']['celular'].'
                        </td>  
                        <td class="texto">
                            <b> E-mail: </b> <br/>'.$pedido['users']['email'].'
                        </td>                  
                    </tr> 

                    <tr> 
                        <td class="texto">
                            <b> Nome do Destinatário: </b> <br/>'.$pedido['ende']['Nome'].'
                        </td>    
                        <td class="texto">
                            <b> Endereço: </b> <br/>'.$pedido['ende']['endereco'].', Nº '.$pedido['ende']['numero'].'
                        </td>   
                        <td class="texto">
                            <b> Bairro: </b> '.$pedido['ende']['bairro'].' <br/>
                            <b> CEP: </b> '.$pedido['ende']['cep'].'
                        </td>  
                        <td class="texto">
                            <b> Cidade: </b> <br/>'.$pedido['cidade']['nome'].' / '.$pedido['estado']['nome'].'
                        </td>                  
                    </tr> 
                                
                </table>

                <hr>

                <h5> Itens do Pedido </h5>
                
                <table border="0" class="table" width="100%"> 
                
                    <tr class="titulo_tab">
                        <td align="center" >Cód. Item</td>
                        <td align="center" >Nome do Item</td>
                        <td align="center" >Preço Unit.</td>
                        <td align="center" >Quantidade</td>
                        <td align="center" >Valor Total</td>
                    </tr>'; 

                    foreach ($produtos as $produto):
                                        
                        if ($l %2 == 0){$cor = '#FFFFFF';}
                        else{$cor = '#E4E4E4';}
                        
                        $html .= '<tr> 
                                    <td class="texto" style="background-color: '.$cor.'; text-align: center; ">'.$produto["ItensPedidos"]["idProdutoFK"].'</td>
                                    <td class="texto" style="background-color: '.$cor.'; text-align: left; "><b>'.$produto["ProdutoFK"]["nomeProduto"].'</b></td>
                                    <td class="texto" style="background-color: '.$cor.'; text-align: right; "><b>'.$produto["ItensPedidos"]["precoUnitario"].'</b></td>
                                    <td class="texto" style="background-color: '.$cor.'; text-align: right; ">'.$produto["ItensPedidos"]["quantidade"].'</td>
                                    <td class="texto" style="background-color: '.$cor.'; text-align: right; ">'.$produto["ItensPedidos"]["precoTotal"].'</td> 
                                </tr>
                                ';


                        $l++;

                    endforeach;

            }                                                     
                                    
                $html .= '
                </table>                    
            </body>
        </html>';

$this->Utilitarios->export_report_all_format($file_type, $filename, $html, 'portrait', '');

?>