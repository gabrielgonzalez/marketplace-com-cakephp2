<div class="container-fluid">
    <div class="row">
        <div class="col-lg-9">
            <div class="card">
                <div class="card-body">

                    

                    <h4 class="card-title">Pedido Nº <?php echo $pedido['Pedidos']['idPedido']; ?></h4>                     
                    
                    <hr>

                    <div class="general-button">

                        <div class='row'>

                            <div class='col-md-3'> 

                                Data do Pedido: <br>
                                <b><?= h(date('d/m/Y', strtotime($pedido['Pedidos']['dtPedido']))); ?></b>

                            </div>

                            <div class='col-md-3'> 

                                Total: <br>
                                <b><?= h($pedido['Pedidos']['precoTotal']); ?></b>

                            </div>

                            <div class='col-md-3'> 

                                Forma de Pagamento: <br>
                                <b><?php 
                                    if ($pedido['Pedidos']['formaPGTO'] == 1) {
                                        echo 'Boleto';
                                    } else {
                                        echo 'Paypal';
                                    }
                                ?></b>

                            </div>
                        
                        </div>  

                        <br>
                        <br>

                        <div class='row'>

                            <div class='col-md-12'>

                                <table class="table table-bordered table-striped verticle-middle">
                                    <thead>
                                        <tr>
                                            <th colspan='5'>Itens do Pedido</th>
                                        </tr>
                                        <tr>
                                            <th scope="col">Cód.</th>
                                            <th scope="col">Nome do item</th>
                                            <th scope="col">Preço Unit.</th>
                                            <th scope="col">Quantidade</th>
                                            <th scope="col">Valor total</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php foreach ($produtos as $key => $produto) {  ?>
                                            
                                            <tr>
                                                <td><?php echo  $produto['ProdutoFK']['idProdutos']; ?></td>
                                                <td><?php echo  $produto['ProdutoFK']['nomeProduto']; ?></td>
                                                <td><?php echo  number_format($produto['ProdutoFK']['preco'], 2, ',', '.'); ?></td>
                                                <td><?php echo  $produto['ItensPedidos']['quantidade']; ?></td>
                                                <td><?php echo  'R$ '.number_format(($produto['ProdutoFK']['preco'] * $produto['ItensPedidos']['quantidade']), 2, ',', '.'); ?></td>
                                            </tr>

                                        <?php } ?>
                                        
                                    </tbody>
                                </table>
                            
                            </div>
                        
                        </div>                  
                        
                    </div>
                </div>
            </div>
        </div>        

        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Endereço para Entrega</h4>
                    
                    <hr>

                    <div class="general-button">

                        <p> 
                            <b>Endereço:</b> <?php echo $pedido['EnderecoFK']['endereco'].', '.$pedido['EnderecoFK']['numero']; ?> <br>
                            <b>Bairro:</b> <?php echo $pedido['EnderecoFK']['bairro']; ?> <br>
                            <b>Cidade:</b> <?php echo $pedido['EnderecoFK']['idCidadeFK'].'/'.$pedido['EnderecoFK']['idEstadoFK']; ?> <br>
                            <b>CEP:</b> <?php echo $pedido['EnderecoFK']['cep']; ?> <br>
                            <b>Informações Adicionais:</b> <?php echo $pedido['EnderecoFK']['dadosAdicionais']; ?> <br>
                        </p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>