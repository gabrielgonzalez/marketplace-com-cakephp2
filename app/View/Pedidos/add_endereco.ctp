<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Cadastrar novo endereço</h5>
        </div>    

        <?php echo $this->Form->create('Endereco', array('url' => array('controller' => 'Pedidos', 'action' => 'add_endereco'), 
                        'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                            'label' => false,
                                                            'class' => 'form-control'
                                                        )) ); ?>                                                     

            <div class="modal-body">                                                                    
                                                                                        
                <div class="basic-form">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <?php echo $this->Form->input('Nome', array('label' => 'Nome do Destinatário', 'class' => 'form-control input-default', 'required') ); ?>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <?php echo $this->Form->input('cep', array('label' => 'CEP', 'id' => 'cep', 'class' => 'form-control input-default', 'required') ); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target='_blank'>Não lembra do seu CEP? Clique aqui!</a>
                        </div>
                    </div>  
                    
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <?php echo $this->Form->input('endereco', array('label' => 'Endereço', 'id' => 'rua', 'class' => 'form-control input-default', 'required') ); ?>
                        </div>
                    </div>  

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <?php echo $this->Form->input('numero', array('label' => 'Número', 'id' => 'numero', 'class' => 'form-control input-default', 'required') ); ?>
                        </div>

                        <div class="form-group col-md-6">
                            <?php echo $this->Form->input('bairro', array('label' => 'Bairro', 'id' => 'bairro', 'class' => 'form-control input-default', 'required') ); ?>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <?php echo $this->Form->input('cidade', array('label' => 'Cidade', 'id' => 'cidade', 'class' => 'form-control input-default', 'disabled') ); ?>
                            <?php echo $this->Form->input('ibge', array('type' => 'hidden', 'id' => 'ibge', 'class' => 'form-control input-default') ); ?>
                        </div>

                        <div class="form-group col-md-2">
                            <?php echo $this->Form->input('estado', array('label' => 'Estado', 'id' => 'uf', 'class' => 'form-control input-default', 'disabled') ); ?>
                        </div>
                    </div>  

                    <div class="form-row">
                        <div class="form-group col-md-9">
                            <?php echo $this->Form->input('dadosAdicionais', array('label' => 'Observações', 'class' => 'form-control input-default') ); ?>
                        </div>
                    </div>  

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <?php echo $this->Form->input('telefone', array('label' => 'Telefone', 'class' => 'form-control input-default') ); ?>
                        </div>
                    </div>  
                </div> 
                                                                    
            </div> 

            <div class="modal-footer">                                                                    
                                                                                        
                <button type="submit" class="btn mb-1 btn-outline-info"><i class="ti-check"> Salvar</i></button> 

                <?php echo $this->Html->link(
                        '<i class="ti-close"> Cancelar</i>', array("action" => "add"), 
                        array(                                                  
                            'class' => 'btn mb-1 btn-outline-dark', 
                            'escape' => false
                        )
                    );?>
                                                                                
            </div> 

        <?= $this->Form->end() ?>                                                              
        
    </div>
</div>

<script>

    $(document).ready(function() {

        function limpa_formulário_cep() {
            // Limpa valores do formulário de cep.
            $("#rua").val("");
            $("#bairro").val("");
            $("#cidade").val("");
            $("#uf").val("");
            $("#ibge").val("");
        }

        //Quando o campo cep perde o foco.
        $("#cep").blur(function() {
            
            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');
            
            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#rua").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#uf").val("...");
                    $("#ibge").val("...");

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#rua").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            $("#uf").val(dados.uf);
                            $("#ibge").val(dados.ibge);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep();
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        });
    });

</script>