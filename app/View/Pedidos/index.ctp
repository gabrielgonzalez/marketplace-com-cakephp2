<div class="content-body">

    <div class="container-fluid mt-3">
    
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body pb-0 d-flex justify-content-between">
                                <div>
                                    <h4 class="mb-1">Meus Pedidos</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li class="d-inline-block mr-3">

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th scope="col" width='20%'> </th>
                                                <th scope="col" width='5%'><?= $this->Paginator->sort('idPedido', 'Cód.') ?></th>
                                                <th scope="col" width='30%'><?= $this->Paginator->sort('UsuarioFK.nome', 'Cliente') ?></th>
                                                <th scope="col" width='20%'><?= $this->Paginator->sort('dtPedido', 'Dt. do Pedido') ?></th>                                                
                                                <th scope="col" width='5%'><?= $this->Paginator->sort('precoTotal', 'Preço Total') ?></th>
                                                <th scope="col" width='20%'><?= $this->Paginator->sort('situacao', 'Situação') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($Pedidos as $Pedido): ?>
                                                <tr>
                                                    <td class="actions">
                                                        
                                                        <!-- Botão para visualizar registro -->
                                                        <?php echo $this->Html->link(
                                                            'Detalhes', array("action" => "view", $Pedido['Pedidos']['idPedido']), 
                                                            array(                                                  
                                                                'class' => 'btn mb-1 btn-outline-warning', 
                                                                'escape' => false
                                                            )
                                                        );?>

                                                        <!-- Botão para visualizar registro -->
                                                        <?php echo $this->Html->link(
                                                            'Extrato', array("action" => "extrato", $Pedido['Pedidos']['idPedido']), 
                                                            array(                                                  
                                                                'class' => 'btn mb-1 btn-outline-info', 
                                                                'target' => '_blank',
                                                                'escape' => false
                                                            )
                                                        );?>

                                                    </td>
                                                    <td><?= $this->Number->format($Pedido['Pedidos']['idPedido']) ?></td>
                                                    <td><?= h($Pedido['UsuarioFK']['nome']) ?></td>
                                                    <td><?= h(date('d/m/Y H:i:s', strtotime($Pedido['Pedidos']['dtPedido']))); ?></td>                                                    
                                                    <td><?= h($Pedido['Pedidos']['precoTotal']) ?></td> 
                                                    <td><?php 
                                                        if ($Pedido['Pedidos']['situacao'] == '0') {
                                                            echo('Pedido Pendente de Aprovação');
                                                        } elseif ($Pedido['Pedidos']['situacao'] == '1') {
                                                            echo('Pedido Aprovado, aguardando faturamento');
                                                        } elseif ($Pedido['Pedidos']['situacao'] == '2') {
                                                            echo('Pedido Faturado, aguardando envio');
                                                        }elseif ($Pedido['Pedidos']['situacao'] == '3') {
                                                            echo('Produto(s) enviado(s)');
                                                        }elseif ($Pedido['Pedidos']['situacao'] == '4') {
                                                            echo('Produto(s) entregue(s)');
                                                        }
                                                    ?></td> 
                                                </tr>

                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-between">
                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>

</div>
