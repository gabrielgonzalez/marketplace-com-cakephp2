<div class="content-body">

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="media align-items-center mb-4">
                            <?php 
                                if (!empty($Usuarios['Usuario']['fotoPerfil'])) {

                                    echo $this->Html->image("avatar/".$Usuarios['Usuario']['fotoPerfil'], array(
                                        'width'=>"80", 'height'=>"80", 'class'=>"mr-3 circle-rounded"
                                    ));

                                } else {

                                    echo $this->Html->image("avatar/sem_foto.jpg", array(
                                        'width'=>"80", 'height'=>"80", 'class'=>"mr-3 circle-rounded"
                                    ));

                                }
                                
                            ?>
                            <div class="media-body">
                                <h3 class="mb-0"><?php echo $Usuarios['Usuario']['nome']; ?></h3>
                            </div>
                        </div>

                        <h4>Sobre mim</h4>
                        <p class="text-muted"><?php echo $Usuarios['Usuario']['biografia']; ?></p>
                        <ul class="card-profile__info">
                            <li class="mb-1"><strong class="text-dark mr-4">Celular</strong> <span><?php echo $Usuarios['Usuario']['celular']; ?></span></li>
                            <li><strong class="text-dark mr-4">Email</strong> <span><?php echo $Usuarios['Usuario']['email']; ?></span></li>
                        </ul>
                    </div>
                </div>  
            </div>
            <div class="col-lg-8 col-xl-9">
                
            
                <div class="card">
                    <div class="card-body">

                        <?php foreach ($produtos as $key => $produto) { ?>
                            
                            <div class="media media-reply">
                                <?php 
                                    if (!empty($produto['FotosProdutoFK']['0']['caminho'])) {

                                        echo $this->Html->image("../Imagens/".$produto['FotosProdutoFK']['0']['caminho'], array(
                                            'width'=>"50", 'height'=>"50", 'class'=>"mr-3 circle-rounded"
                                        ));

                                    } else {

                                        echo $this->Html->image("avatar/sem_foto.jpg", array(
                                            'width'=>"50", 'height'=>"50", 'class'=>"mr-3 circle-rounded"
                                        ));

                                    }
                                    
                                ?>

                                <div class="media-body">
                                    <?php foreach ($produto['Avaliacoes'] as $key => $Avaliacoes) { ?> 
                                    
                                        <div class="d-sm-flex justify-content-between mb-2">
                                            <h5 class="mb-sm-0"><?php echo $Avaliacoes['UsuarioFK']['nome']; ?> <small class="text-muted ml-3"><?php echo date('d/m/Y', strtotime($Avaliacoes['Avaliacoes']['dtCriacao'])); ?></small></h5>                                            
                                        </div>
                                        
                                        <p><?php echo $Avaliacoes['Avaliacoes']['descricao']; ?></p>                                        
                                        
                                        <hr>

                                    <?php } ?> 

                                </div>
                                
                            </div>
                            
                        <?php } ?> 
                        
                    </div>

                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #/ container -->
</div>