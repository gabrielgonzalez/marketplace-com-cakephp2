<div class="container-fluid mt-3">
    
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body pb-0 d-flex justify-content-between">
                            <div>
                                <h4 class="card-title">Detalhes do Anúncio: <?php echo $produto['Anuncios']['nomeProduto']; ?> </h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="basic-list-group">
                                <div class="row">
                                    <div class="col-xl-4 col-md-4 col-sm-3 mb-4 mb-sm-0">
                                        <div class="list-group" id="list-tab" role="tablist">
                                            
                                            <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home" >1. Em que categoria deseja Anunciar?</a> 
                                            <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile" >2. Adicione um Título e o preço do Anúncio </a> 
                                            <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages" >3. Acresecente mais detalhes</a> 
                                            <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings" >4. Adicione Imagens do produto</a>

                                        </div>
                                    </div>
                                    <div class="col-xl-8 col-md-8 col-sm-9">
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="list-home">
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('departamento', array('label' => 'Departamento <span class="text-danger">*</span>', 'value' => $Categoria['DepartamentoFK']['descricao'], 'type' => 'text', 'class' => 'form-control input-default', 'disabled') ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('categoria', array('label' => 'Categoria <span class="text-danger">*</span>', 'value' => $Categoria['Categoria']['descricao'], 'type' => 'text', 'class' => 'form-control input-default', 'disabled') ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('subcategoria', array('label' => 'Subcategoria <span class="text-danger">*</span>', 'value' => $Subcategoria['Subcategoria']['descricao'], 'type' => 'text', 'class' => 'form-control input-default', 'disabled') ); ?>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane fade" id="list-profile" role="tabpanel">
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('nomeProduto', array('label' => 'Qual o título do anúncio? <span class="text-danger">*</span>', 'value' => $produto['Anuncios']['nomeProduto'], 'type' => 'text', 'class' => 'form-control input-default', 'disabled' ) ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <?php 
                                                            $preco = str_replace(".",",", $produto['Anuncios']['preco']);
                                                            echo $this->Form->input('preco', array('value' => $preco, 'label' => 'Qual o preço deste produto? <span class="text-danger">*</span>', 'type' => 'text', 'class' => 'form-control input-default', 'disabled' ) ); ?>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="tab-pane fade" id="list-messages">
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('descricao', array('value' => $produto['Anuncios']['descricao'], 'label' => 'Descrição detalhada do produto <span class="text-danger">*</span>', 'type' => 'textarea', 'disabled', 'class' => 'form-control input-default' ) ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('marca', array('value' => $produto['Anuncios']['marca'], 'label' => 'Marca <span class="text-danger">*</span>', 'disabled', 'type' => 'text', 'class' => 'form-control input-default' ) ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('modelo', array('value' => $produto['Anuncios']['modelo'], 'label' => 'Modelo <span class="text-danger">*</span>', 'disabled', 'type' => 'text', 'class' => 'form-control input-default' ) ); ?>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane fade" id="list-settings">
                                                
                                                <div class="portlet box grey">
                                                    <div class="portlet-title">
                                                        <div class="caption">Adicionar Imagens</div>
                                                    </div>
                                                    <div class="portlet-body"> 
                                                        <div class="table-responsive">
                                                            
                                                            <table class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Imagem</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="anexosimg">

                                                                    <?php 
                                                                    $i=0;
                                                                    foreach ($FotosProduto as $key => $Fotos) { ?>

                                                                        <tr id="ImagemProduto<?php echo $i; ?>" >
                                                                            <td>
                                                                                <?php echo  $this->Html->image("/Imagens/".$Fotos['Img']['caminho'], array( 'border' => '0', 'width'=>'60', 'height'=>'60'));?>
                                                                            </td>
                                                                            <td>
                                                                                <?php echo $Fotos['Img']['caminho']; ?>
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                    <?php 
                                                                    $i++;
                                                                    } ?>

                                                                </tbody>
                                                            </table>
                                                            
                                                        </div> 
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <div class="card-footer">
                            <div class="d-flex justify-content-between">

                                Data de Criação: <?php echo $produto['Anuncios']['dtCriacao']; ?>
                                <br>
                                Data da última Alteração: <?php echo $produto['Anuncios']['dtAlteracao']; ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div> 