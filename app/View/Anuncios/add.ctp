<?php echo $this->Form->create('Produto', array('enctype' => 'multipart/form-data')); ?>

<div class="container-fluid mt-3">
    
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body pb-0 d-flex justify-content-between">
                            <div>
                                <h4 class="card-title">Anunciar</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="basic-list-group">
                                <div class="row">
                                    <div class="col-xl-4 col-md-4 col-sm-3 mb-4 mb-sm-0">
                                        <div class="list-group" id="list-tab" role="tablist">
                                            
                                            <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home" >1. Em que categoria deseja Anunciar?</a> 
                                            <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile" >2. Adicione um Título e o preço do Anúncio </a> 
                                            <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages" >3. Acresecente mais detalhes</a> 
                                            <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings" >4. Adicione Imagens do produto</a>

                                        </div>
                                    </div>
                                    <div class="col-xl-8 col-md-8 col-sm-9">
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="list-home">
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('departamento', array('label' => 'Departamento <span class="text-danger">*</span>', 'options' => $optionsDepartamento, 'type' => 'select', 'class' => 'form-control input-default', 'required', 'onclick' => 'BuscaCategorias(this, \'categoriaFK\', \''.$this->Html->url(array("controller" => "Anuncios", "action" => "lista_categoria")).'\')') ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <label for="categoria">Categoria <span class="text-danger">*</span></label>
                                                        <select name="data[Produto][categoria]" class="form-control input-default" 
                                                                                      onclick="BuscaCategorias(this, 'subcategoriaFK', '/scommerce/Anuncios/lista_subcategoria')"
                                                                                      onkeypress="return handleEnter(this, event)" required                                                                          
                                                                                      id="categoriaFK" tabindex="-1">
                                                                <select/>  
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <label for="categoria">Subcategoria <span class="text-danger">*</span></label>
                                                        <select name="data[Produto][idSubcategoriaFK]" class="form-control input-default" 
                                                                                    onkeypress="return handleEnter(this, event)" required                                                                           
                                                                                    id="subcategoriaFK" tabindex="-1">
                                                                <select/>  
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane fade" id="list-profile" role="tabpanel">
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('nomeProduto', array('label' => 'Qual o título do anúncio? <span class="text-danger">*</span>', 'placeholder' => 'Ex.: Camiseta branca tamanho G', 'type' => 'text', 'required', 'class' => 'form-control input-default' ) ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <?php echo $this->Form->input('preco', array('label' => 'Qual o preço deste produto? <span class="text-danger">*</span>', 'placeholder' => '0,00', 'type' => 'text', 'class' => 'form-control input-default', 'required', 'onkeypress' => "return(MascaraMoeda(this,'.',',',event))" ) ); ?>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="tab-pane fade" id="list-messages">
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('descricao', array('label' => 'Descrição detalhada do produto <span class="text-danger">*</span>', 'type' => 'textarea', 'required', 'class' => 'form-control input-default' ) ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('marca', array('label' => 'Marca <span class="text-danger">*</span>', 'placeholder' => 'Ex.: Nike', 'required', 'type' => 'text', 'class' => 'form-control input-default' ) ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('modelo', array('label' => 'Modelo <span class="text-danger">*</span>', 'placeholder' => 'Ex.: Air Jordan', 'required', 'type' => 'text', 'class' => 'form-control input-default' ) ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('quantidade', array('label' => 'Quantidade <span class="text-danger">*</span>', 'placeholder' => 'Ex.: 1', 'required', 'type' => 'text', 'class' => 'form-control input-default' ) ); ?>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="tab-pane fade" id="list-settings">
                                                
                                                <div class="portlet box grey">
                                                    <div class="portlet-title">
                                                        <div class="caption">Adicionar Imagens</div>
                                                        <div class="actions">   
                                                                                                                    
                                                            <button type="button" class="btn mb-1 btn-outline-info" onclick="return addItem('/scommerce/Anuncios/add_imagem', 'anexosimg')"><i class="icon-plus"></i> Adicionar nova Imagem</button>

                                                        </div>
                                                    </div>
                                                    <div class="portlet-body"> 
                                                        <div class="table-responsive">
                                                            
                                                            <table class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Imagem</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="anexosimg">
                                                                    <tr id="ImagemProduto0" >
                                                                        <th>
                                                                            <?php echo $this->Html->link('<i class="fa fa-close"></i>', $this->Html->url("#"), 
                                                                                                        array('class' => 'btn mb-1 btn-danger', 
                                                                                                            'onClick' => 'deleteRow(\'ImagemProduto0'.'\')',
                                                                                                            'escape' => false,
                                                                                                            'id' => 'ExcluiItem0',
                                                                                                            'title' => 'Clique aqui para remover esta imagem'
                                                                                                            ) 
                                                                                                        ); ?>
                                                                        </th>
                                                                        <td>
                                                                            <input type="file" name="data[caminho][0]" accept="image/*" id="caminhoArquivo0">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            
                                                        </div> 
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <div class="card-footer">
                            <div class="d-flex justify-content-between">

                                <ul>
                                    <li class="d-inline-block mr-3">

                                        <button type="submit" class="btn mb-1 btn-info" onclick='return validaCampos();'> <i class='ti-save'></i> Salvar</button>

                                        <?php echo $this->Html->link(
                                                                "<i class='icon-close'></i> Cancelar", array("action" => "index"), 
                                                                array(                                                  
                                                                    'class' => 'btn mb-1 btn-danger', 
                                                                    'escape' => false, 
                                                                    'title' => 'Click para sair do cadastro do anúncio'
                                                                )
                                                            );?>

                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div> 

<?php echo $this->Form->end(); ?>

<script>

    $(document).ready(function(){
        window.sessionStorage['i'] = 1;
    });


    function BuscaCategorias(id, codDiv, url) {	
        
        var id = $(id).val();
        var div = '#'+codDiv;
        
        var link = url+'/'+id;  
        
        $.ajax({
                    url: link
                }).done(function(data) {
                
                    $(div).html(data);   

                });
        
    } 


    function addItem(url, div) {
        //document.getElementById('btn_additem').disabled = true;
        //$( '#btn_additem' ).addClass('disabled');
    
        var i = window.sessionStorage['i'];	
            var url = url+'/'+i;
            $.ajax({
                    url: url
                }).done(function(data) {
                    $('#'+div).append(data); 
                    i = parseInt(i) + parseInt(1);
                    window.sessionStorage['i'] = i; 
                    
                });        
            
    }


    function deleteRow(c){
        var row = document.getElementById(c);            
        row.parentNode.removeChild(row);         
    }


    function validaCampos(){ /// There are 4 predefined ones: "warning", "error", "success" and "info".

        if (document.getElementById('ProdutoDepartamento').value == '') {

            swal("Atenção!", "Você precisa selecionar um departamento!", "error");

            document.getElementById("list-home-list").click();

            return false;
            
        } else if (document.getElementById('categoriaFK').value == '') {

            swal("Atenção!", "Você precisa selecionar uma categoria!", "error"); 

            document.getElementById("list-home-list").click();

            return false;

        } else if (document.getElementById('subcategoriaFK').value == '') {

            swal("Atenção!", "Você precisa selecionar uma subcategoria!", "error");

            document.getElementById("list-home-list").click();

            return false;

        } else if (document.getElementById('ProdutoNomeProduto').value == '') {

            swal("Atenção!", "Você precisa colocar um nome no seu anúncio!", "error");

            document.getElementById("list-profile-list").click();

            return false;

        } else if (document.getElementById('ProdutoPreco').value == '') {

            swal("Atenção!", "Você precisa informar o preço deste produto!", "error");

            document.getElementById("list-profile-list").click();

            return false;

        } else if (document.getElementById('ProdutoDescricao').value == '') {

            swal("Atenção!", "Seu anúncio não pode ficar sem uma descrição detalhada!", "error");

            document.getElementById("list-messages-list").click();

            return false;

        } else if (document.getElementById('ProdutoMarca').value == '') {

            swal("Atenção!", "Você precisa informar a marca deste produto!", "error");

            document.getElementById("list-messages-list").click();

            return false;

        } else if (document.getElementById('ProdutoModelo').value == '') {

            swal("Atenção!", "Você precisa informar o modelo deste produto!", "error");

            document.getElementById("list-messages-list").click();

            return false;

        } else {

            return true;

        }        
        
        
    }

</script>