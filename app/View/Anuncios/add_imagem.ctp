<tr id="<?php echo 'ImagemProduto'.$i; ?>">        
    <td class="actions">
        <div class="radio-list">
            <?php echo $this->Html->link('<i class="ti-trash"></i>', $this->Html->url("#"), 
                                        array('class' => 'btn mb-1 btn-danger', 
                                            'onClick' => 'deleteRow(\'ImagemProduto'.$i.'\')',
                                            'escape' => false,
                                            'id' => 'ExcluiItem'.$i,
                                            'title' => 'Clique aqui para remover esta imagem'
                                            ) 
                                        ); ?>
        </div>
    </td>

    <td colspan='2'>            
        <input type="file" name="data[caminho][<?php echo $i; ?>]" accept="image/*" id="caminhoArquivo<?php echo $i; ?>">    
    </td>

</tr>