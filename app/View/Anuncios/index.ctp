<?php echo $this->Form->create('Departamento', array('url' => array('controller' => 'Departamento', 'action' => 'add'), 
                                                                            'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                'label' => false,
                                                                                                                'class' => 'form-control'
                                                                                                            )) ); ?>     

    <div class="content-body">

        <div class="container-fluid mt-3">
        
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body pb-0 d-flex justify-content-between">
                                    <div>
                                        <h4 class="mb-1">Seus Anúncios</h4>
                                    </div>
                                    <div>
                                        <ul>
                                            <li class="d-inline-block mr-3">

                                                <!-- Botão para acionar registro -->
                                                <?php echo $this->Html->link('<i class="ti-plus"> </i> Novo Anúncio', array( 'controller' => 'Anuncios', 'action' => 'add' ), array('escape' => false, 'class'=>"btn mb-1 btn-sm btn-outline-info", 'title'=>'Clique para cadastar um novo Anuncio')); ?>

                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="d-flex justify-content-between">

                                        <?php if (!empty($Anuncios)) { ?>

                                            <table class="table table-striped table-bordered zero-configuration">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" width='30%'> </th>
                                                        <th scope="col" width='10%'><?= $this->Paginator->sort('idProdutos', 'Cód.') ?></th>
                                                        <th scope="col" width='30%'><?= $this->Paginator->sort('nomeProduto', 'Nome do Produto') ?></th>
                                                        <th scope="col" width='30%'><?= $this->Paginator->sort('marca', 'Marca') ?></th>                                                
                                                        <th scope="col" width='30%'><?= $this->Paginator->sort('modelo', 'Modelo') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($Anuncios as $Anuncio): ?>
                                                        <tr>
                                                            <td class="actions">
                                                                
                                                                <!-- Botão para visualizar registro -->
                                                                <?php echo $this->Html->link(
                                                                                    '<i class="ti-eye"> Visualizar</i>', array("action" => "view", $Anuncio['Anuncios']['idProdutos']), 
                                                                                    array(                                                  
                                                                                        'class' => 'btn mb-1 btn-sm btn-outline-warning', 
                                                                                        'escape' => false, 
                                                                                        'title' => 'Clique para ver as informações deste anúncio'
                                                                                    )
                                                                                );?>

                                                                
                                                                <!-- Botão para editar registro -->
                                                                <?php echo $this->Html->link(
                                                                                    '<i class="ti-pencil-alt"> Editar</i>', array("action" => "edit", $Anuncio['Anuncios']['idProdutos']), 
                                                                                    array(                                                  
                                                                                        'class' => 'btn mb-1 btn-sm btn-outline-success', 
                                                                                        'escape' => false, 
                                                                                        'title' => 'Clique para editar as informações deste anúncio'
                                                                                    )
                                                                                );?>

                                                                <?php echo $this->Html->link(
                                                                                    '<i class="ti-trash"> Deletar</i>', array("action" => "delete", $Anuncio['Anuncios']['idProdutos']), 
                                                                                    array(                                                  
                                                                                        'class' => 'btn mb-1 btn-sm btn-outline-danger', 
                                                                                        'escape' => false, 
                                                                                        'confirm' => 'Tem certeza que deseja apagar este registro?'
                                                                                    )
                                                                                );?>
                                                            </td>
                                                            <td><?= $this->Number->format($Anuncio['Anuncios']['idProdutos']) ?></td>
                                                            <td><?= h($Anuncio['Anuncios']['nomeProduto']) ?></td>
                                                            <td><?= h($Anuncio['Anuncios']['marca']) ?></td>                                                    
                                                            <td><?= h($Anuncio['Anuncios']['modelo']) ?></td> 
                                                        </tr>

                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>

                                        <?php } else { ?>

                                            <h5>Nenhum Aníncio foi cadastrado, clique no botão "Novo Anúncio" para começar a vender</h5>

                                        <?php } ?>                                      
                                                                            
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="d-flex justify-content-between">
                                        
                                        <div class="paginator">
                                            <ul class="pagination">
                                                <?= $this->Paginator->first('<< ' . __('Primeiro ')) ?>
                                                <?= $this->Paginator->prev('< ' . __('Anterior ')) ?>
                                                <?= $this->Paginator->numbers() ?>
                                                <?= $this->Paginator->next(__('Próximo') . ' >') ?>
                                                <?= $this->Paginator->last(__('Último]') . ' >>') ?>
                                            </ul>
                                            <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registro(s) de um total de {{count}} ')]) ?></p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>     

    </div>


<?= $this->Form->end() ?>  