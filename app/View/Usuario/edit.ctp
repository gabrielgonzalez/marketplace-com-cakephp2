<?php echo $this->Form->create('Usuario', array('url' => array('controller' => 'Usuario', 'action' => 'edit'), 
                                                                                'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                    'label' => false,
                                                                                                                    'class' => 'form-control'
                                                                                                                )) ); ?>

    <div class="basic-list-group">
        <ul class="list-group">
            <li class="list-group-item list-group-item-dark">Informações Pessoais</li>
            <li class="list-group-item">
            
                <div class="card">
                    <div class="card-body">
                        <div class="basic-form">
                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <?php echo $this->Form->input('username', array('label' => 'Usuário', 'value' => $Usuarios['Usuario']['username'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <?php echo $this->Form->input('nome', array('label' => 'Nome Completo', 'value' => $Usuarios['Usuario']['nome'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <?php echo $this->Form->input('email', array('label' => 'Email', 'value' => $Usuarios['Usuario']['email'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <?php echo $this->Form->input('dtNascimento', array('label' => 'Dt. Nascimento', 'value' => $Usuarios['Usuario']['dtNascimento'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <?php echo $this->Form->input('telefone', array('label' => 'Telefone', 'value' => $Usuarios['Usuario']['telefone'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <?php echo $this->Form->input('celular', array('label' => 'Celular', 'value' => $Usuarios['Usuario']['celular'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                    </div>                                                    
                                </div>
                                
                                <?php if ($Usuarios['Usuario']['tipoPessoa'] == 'PJ') { ?> 

                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('tipoPes', array('label' => 'Tipo Pessoa', 'value' => 'Pessoa Jurídica', 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('cnpj', array('label' => 'CNPJ', 'value' => $Usuarios['Usuario']['cnpj'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>                                                   
                                    </div>
                                
                                <?php } else { ?>

                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('tipoPes', array('label' => 'Tipo Pessoa', 'value' => 'Pessoa Física', 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('cpf', array('label' => 'CPF', 'value' => $Usuarios['Usuario']['cpf'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('rg', array('label' => 'RG', 'value' => $Usuarios['Usuario']['rg'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>                                                    
                                    </div>

                                <?php } ?>
                                
                                <?php echo $this->Html->link('Alterar Informações', array( 'controller' => 'Usuario', 'action' => 'edit' ), array('class' => 'btn mb-1 btn-sm btn-outline-info') ); ?>
                            </form>
                        </div>
                    </div>
                </div>
            
            </li>
        </ul>
    </div>

<?= $this->Form->end() ?>   