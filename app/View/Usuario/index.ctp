<!-- Adicionando JQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>

<div>

    <div class="container-fluid mt-3">

        <div class="col-xl-12">
            
            <!-- INFORMAÇÕES PESSOAIS -->
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Informações Pessoais</h4>

                    <hr>
                    
                    <!-- ****** Informações Pessoais ****** -->
                    <div class="basic-list-group">
                        <ul class="list-group">
                            
                            <li class="list-group-item">
                            
                                <form>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <div class="media align-items-center mb-4">
                                                <?php 
                                                    if (!empty($Usuarios['Usuario']['fotoPerfil'])) {

                                                        echo $this->Html->image("avatar/".$Usuarios['Usuario']['fotoPerfil'], array(
                                                            'width'=>"80", 'height'=>"80", 'class'=>"mr-3 circle-rounded"
                                                        ));

                                                    } else {

                                                        echo $this->Html->image("avatar/sem_foto.jpg", array(
                                                            'width'=>"80", 'height'=>"80", 'class'=>"mr-3 circle-rounded"
                                                        ));

                                                    }
                                                    
                                                ?>
                                            </div>
                                            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <?php echo $this->Form->input('username', array('label' => 'Usuário', 'value' => $Usuarios['Usuario']['username'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <?php echo $this->Form->input('nome', array('label' => 'Nome Completo', 'value' => $Usuarios['Usuario']['nome'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <?php echo $this->Form->input('email', array('label' => 'Email', 'value' => $Usuarios['Usuario']['email'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('dtNascimento', array('label' => 'Dt. Nascimento', 'value' => date('d/m/Y', strtotime($Usuarios['Usuario']['dtNascimento'])), 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('telefone', array('label' => 'Telefone', 'value' => $Usuarios['Usuario']['telefone'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('celular', array('label' => 'Celular', 'value' => $Usuarios['Usuario']['celular'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>                                                    
                                    </div>
                                    
                                    <?php if ($Usuarios['Usuario']['tipoPessoa'] == 'PJ') { ?> 

                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <?php echo $this->Form->input('tipoPes', array('label' => 'Tipo Pessoa', 'value' => 'Pessoa Jurídica', 'class' => 'form-control input-default', 'disabled') ); ?>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <?php echo $this->Form->input('cnpj', array('label' => 'CNPJ', 'value' => $Usuarios['Usuario']['cnpj'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                            </div>                                                   
                                        </div>
                                    
                                    <?php } else { ?>

                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <?php echo $this->Form->input('tipoPes', array('label' => 'Tipo Pessoa', 'value' => 'Pessoa Física', 'class' => 'form-control input-default', 'disabled') ); ?>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <?php echo $this->Form->input('cpf', array('label' => 'CPF', 'value' => $Usuarios['Usuario']['cpf'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <?php echo $this->Form->input('rg', array('label' => 'RG', 'value' => $Usuarios['Usuario']['rg'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                            </div>                                                    
                                        </div>

                                    <?php } ?>

                                    <div class="form-row">
                                        <div class="form-group col-md-9">
                                            <?php echo $this->Form->input('biografia', array('label' => 'Biografia', 'value' => $Usuarios['Usuario']['biografia'], 'type' => 'textarea', 'class' => 'form-control input-default', 'disabled') ); ?>
                                        </div>                                                 
                                    </div>
                                    
                                    <!-- Botão para editar registro -->
                                    <button type="button" class="btn mb-1 btn-sm btn-outline-success" data-toggle="modal" data-target="#ModalEdit" title='Clique para editar'>
                                        <i class="ti-pencil-alt"> Alterar Informações</i>
                                    </button>
                                </form>
                            
                            </li>
                        </ul>
                    </div>

                </div>
            </div>

            <!-- ENDEREÇOS -->
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Endereços</h4>

                    <hr>

                    <!-- Botão para acionar registro -->
                    <button type="button" class="btn mb-1 btn-sm btn-outline-info" data-toggle="modal" data-target="#ModalAdd" title='Clique para cadastar um novo Departamento'>
                        <i class="ti-plus"> Novo Endereço</i>
                    </button>

                    <!-- ****** Modal Add ****** -->
                        <div class="modal fade bd-example-modal-lg" id='ModalAdd' tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Cadastrar novo endereço</h5>
                                        
                                        <?php echo $this->Html->link(
                                                    '<i class="ti-close"></i>', array("action" => "index"), 
                                                    array(                                                  
                                                        'class' => 'close', 
                                                        'escape' => false
                                                    )
                                                );?>

                                    </div>    

                                    <?php echo $this->Form->create('Endereco', array('url' => array('controller' => 'Endereco', 'action' => 'add'), 
                                                    'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                        'label' => false,
                                                                                        'class' => 'form-control'
                                                                                    )) ); ?>                                                     

                                        <div class="modal-body">                                                                    
                                                                                                                    
                                            <div class="basic-form">
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <?php echo $this->Form->input('Nome', array('label' => 'Nome do Destinatário', 'value' => $Usuarios['Usuario']['nome'], 'class' => 'form-control input-default', 'required') ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <?php echo $this->Form->input('cep', array('label' => 'CEP', 'id' => 'cep', 'class' => 'form-control input-default', 'required') ); ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target='_blank'>Não lembra do seu CEP? Clique aqui!</a>
                                                    </div>
                                                </div>  
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <?php echo $this->Form->input('endereco', array('label' => 'Endereço', 'id' => 'rua', 'class' => 'form-control input-default', 'required') ); ?>
                                                    </div>
                                                </div>  

                                                <div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <?php echo $this->Form->input('numero', array('label' => 'Número', 'id' => 'numero', 'class' => 'form-control input-default', 'required') ); ?>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <?php echo $this->Form->input('bairro', array('label' => 'Bairro', 'id' => 'bairro', 'class' => 'form-control input-default', 'required') ); ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <?php echo $this->Form->input('cidade', array('label' => 'Cidade', 'id' => 'cidade', 'class' => 'form-control input-default', 'disabled') ); ?>
                                                        <?php echo $this->Form->input('ibge', array('type' => 'hidden', 'id' => 'ibge', 'class' => 'form-control input-default') ); ?>
                                                    </div>

                                                    <div class="form-group col-md-2">
                                                        <?php echo $this->Form->input('estado', array('label' => 'Estado', 'id' => 'uf', 'class' => 'form-control input-default', 'disabled') ); ?>
                                                    </div>
                                                </div>  

                                                <div class="form-row">
                                                    <div class="form-group col-md-9">
                                                        <?php echo $this->Form->input('dadosAdicionais', array('label' => 'Observações', 'class' => 'form-control input-default') ); ?>
                                                    </div>
                                                </div>  

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <?php echo $this->Form->input('telefone', array('label' => 'Telefone', 'class' => 'form-control input-default') ); ?>
                                                    </div>
                                                </div>  
                                            </div> 
                                                                                                
                                        </div> 

                                        <div class="modal-footer">                                                                    
                                                                                                                    
                                            <button type="submit" class="btn mb-1 btn-outline-info"><i class="ti-check"> Salvar</i></button> 

                                            <?php echo $this->Html->link(
                                                    '<i class="ti-close"> Cancelar</i>', array("action" => "index"), 
                                                    array(                                                  
                                                        'class' => 'btn mb-1 btn-outline-dark', 
                                                        'escape' => false
                                                    )
                                                );?>
                                                                                                            
                                        </div> 

                                    <?= $this->Form->end() ?>                                                              
                                    
                                </div>
                            </div>
                        </div>
                    <!-- ****** Fim Modal Add ****** -->
                                            
                    <br>
                    
                    <!-- ****** Dados dos endereços de entrega e cobrança ****** -->
                    <table class="table table-striped table-bordered zero-configuration">

                        <thead>

                            <tr>
                                <th scope="col" width='25%'> </th>
                                <th scope="col">Destinatário</th>
                                <th scope="col">Endereço e Nº</th>
                                <th scope="col">Bairro</th>                                                
                                <th scope="col">Cidade/Estado</th>                                                
                            </tr>
                                
                        </thead>

                        <tbody>

                            <?php foreach ($Enderecos as $Endereco){ ?>
                                <tr>
                                    <td class="actions">
                                        
                                        <!-- Botão para visualizar registro -->
                                        <button type="button" class="btn mb-1 btn-sm btn-outline-warning" data-toggle="modal" data-target="#ModalView<?php echo $Endereco['Endereco']['idEnderecos']; ?>" title='Clique para ver mais informações'>
                                            <i class="ti-eye"> Ver</i>
                                        </button>
                                        
                                        <!-- Botão para editar registro -->
                                        <button type="button" class="btn mb-1 btn-sm btn-outline-success" data-toggle="modal" data-target="#ModalEdit<?php echo $Endereco['Endereco']['idEnderecos']; ?>" title='Clique para editar'>
                                            <i class="ti-pencil-alt"> Editar</i>
                                        </button>

                                        <?php echo $this->Html->link(
                                                            '<i class="ti-trash"> Deletar</i>', array('controller'=> 'Endereco', "action" => "delete", $Endereco['Endereco']['idEnderecos']), 
                                                            array(                                                  
                                                                'class' => 'btn mb-1 btn-sm btn-outline-danger', 
                                                                'escape' => false, 
                                                                'confirm' => 'Tem certeza que deseja apagar este registro?'
                                                            )
                                                        );?>
                                    </td>
                                    <td><?= h($Endereco['Endereco']['Nome']) ?></td>
                                    <td><?= h($Endereco['Endereco']['endereco'] . ', Nº ' . $Endereco['Endereco']['numero']) ?></td>
                                    <td><?= h($Endereco['Endereco']['bairro']) ?></td>                                                    
                                    <td><?= h($Endereco['CidadeFK']['nome'].'/'.$Endereco['EstadoFK']['idEstado']) ?></td>                                                    
                                </tr>

                                <!-- ****** Modal Edit ****** -->
                                    <div class="modal fade bd-example-modal-lg" id='ModalEdit<?php echo $Endereco['Endereco']['idEnderecos']; ?>' tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Endereco | <?= h($Endereco['Endereco']['Nome']) ?> </h5>
                                                    
                                                    <?php echo $this->Html->link(
                                                            '<i class="ti-close"></i>', array("action" => "index"), 
                                                            array(                                                  
                                                                'class' => 'close', 
                                                                'escape' => false
                                                            )
                                                        );?>
                                                    
                                                </div>    

                                                <?php echo $this->Form->create('Endereco', array('url' => array('controller' => 'Endereco', 'action' => 'edit', $Endereco['Endereco']['idEnderecos']), 
                                                                'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                    'label' => false,
                                                                                                    'class' => 'form-control'
                                                                                                )) ); ?>                                                     

                                                    <div class="modal-body">                                                                    
                                                                                                                    
                                                        <div class="basic-form">
                                                            <div class="form-row">
                                                                <div class="form-group col-md-12">
                                                                    <?php echo $this->Form->input('Nome', array('label' => 'Nome do Destinatário', 'value' => $Endereco['Endereco']['Nome'], 'class' => 'form-control input-default', 'required') ); ?>
                                                                </div>
                                                            </div>
            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <?php echo $this->Form->input('cep', array('label' => 'CEP', 'value' => $Endereco['Endereco']['cep'], 'id' => 'cep', 'class' => 'form-control input-default', 'required') ); ?>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target='_blank'>Não lembra do seu CEP? Clique aqui!</a>
                                                                </div>
                                                            </div>  
                                                            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <?php echo $this->Form->input('endereco', array('label' => 'Endereço', 'value' => $Endereco['Endereco']['endereco'], 'id' => 'rua', 'class' => 'form-control input-default', 'required') ); ?>
                                                                </div>
                                                            </div>  
            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-3">
                                                                    <?php echo $this->Form->input('numero', array('label' => 'Número', 'value' => $Endereco['Endereco']['numero'], 'id' => 'numero', 'class' => 'form-control input-default', 'required') ); ?>
                                                                </div>
            
                                                                <div class="form-group col-md-6">
                                                                    <?php echo $this->Form->input('bairro', array('label' => 'Bairro', 'value' => $Endereco['Endereco']['bairro'], 'id' => 'bairro', 'class' => 'form-control input-default', 'required') ); ?>
                                                                </div>
                                                            </div>
            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <?php echo $this->Form->input('cidade', array('label' => 'Cidade', 'value' => $Endereco['CidadeFK']['nome'], 'id' => 'cidade', 'class' => 'form-control input-default', 'disabled') ); ?>
                                                                    <?php echo $this->Form->input('ibge', array('type' => 'hidden', 'value' => $Endereco['CidadeFK']['codigoIBGE'], 'id' => 'ibge', 'class' => 'form-control input-default') ); ?>
                                                                </div>
            
                                                                <div class="form-group col-md-2">
                                                                    <?php echo $this->Form->input('estado', array('label' => 'Estado', 'value' => $Endereco['EstadoFK']['idEstado'], 'id' => 'uf', 'class' => 'form-control input-default', 'disabled') ); ?>
                                                                </div>
                                                            </div>  
            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-9">
                                                                    <?php echo $this->Form->input('dadosAdicionais', array('label' => 'Observações', 'value' => $Endereco['Endereco']['dadosAdicionais'], 'class' => 'form-control input-default') ); ?>
                                                                </div>
                                                            </div>  
            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <?php echo $this->Form->input('telefone', array('label' => 'Telefone', 'value' => $Endereco['Endereco']['telefone'], 'class' => 'form-control input-default') ); ?>
                                                                </div>
                                                            </div>  
                                                        </div> 
                                                                                                            
                                                    </div>

                                                    <div class="modal-footer">                                                                    
                                                                                                                                
                                                        <button type="submit" class="btn mb-1 btn-outline-info"><i class="ti-check"> Salvar</i></button> 

                                                        <?php echo $this->Html->link(
                                                            '<i class="ti-close"> Cancelar</i>', array("action" => "index"), 
                                                            array(                                                  
                                                                'class' => 'btn mb-1 btn-outline-dark', 
                                                                'escape' => false
                                                            )
                                                        );?>
                                                                                                                        
                                                    </div> 

                                                <?= $this->Form->end() ?>                                                              
                                                
                                            </div>
                                        </div>
                                    </div>
                                <!-- ****** Fim Modal Edit ****** -->

                                <!-- ****** Modal View ****** -->
                                    <div class="modal fade bd-example-modal-lg" id='ModalView<?php echo $Endereco['Endereco']['idEnderecos']; ?>' tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Endereco | <?= h($Endereco['Endereco']['Nome']) ?> </h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span><i class="ti-close"></i></span>
                                                    </button>
                                                </div>    

                                                <?php echo $this->Form->create('Endereco', array('url' => array('controller' => 'Endereco', 'action' => 'edit', $Endereco['Endereco']['idEnderecos']), 
                                                                'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                    'label' => false,
                                                                                                    'class' => 'form-control'
                                                                                                )) ); ?>                                                     

                                                    <div class="modal-body">                                                                    
                                                                                                                    
                                                        <div class="basic-form">
                                                            <div class="form-row">
                                                                <div class="form-group col-md-12">
                                                                    <?php echo $this->Form->input('Nome', array('label' => 'Nome do Destinatário', 'value' => $Endereco['Endereco']['Nome'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                                                </div>
                                                            </div>
            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <?php echo $this->Form->input('cep', array('label' => 'CEP', 'value' => $Endereco['Endereco']['cep'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                                                </div>
                                                            </div>  
                                                            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <?php echo $this->Form->input('endereco', array('label' => 'Endereço', 'value' => $Endereco['Endereco']['endereco'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                                                </div>
                                                            </div>  
            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-3">
                                                                    <?php echo $this->Form->input('numero', array('label' => 'Número', 'value' => $Endereco['Endereco']['numero'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                                                </div>
            
                                                                <div class="form-group col-md-6">
                                                                    <?php echo $this->Form->input('bairro', array('label' => 'Bairro', 'value' => $Endereco['Endereco']['bairro'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                                                </div>
                                                            </div>
            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <?php echo $this->Form->input('cidade', array('label' => 'Cidade', 'value' => $Endereco['CidadeFK']['nome'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                                                </div>
            
                                                                <div class="form-group col-md-2">
                                                                    <?php echo $this->Form->input('estado', array('label' => 'Estado', 'value' => $Endereco['EstadoFK']['idEstado'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                                                </div>
                                                            </div>  
            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-9">
                                                                    <?php echo $this->Form->input('dadosAdicionais', array('label' => 'Observações', 'value' => $Endereco['Endereco']['dadosAdicionais'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                                                </div>
                                                            </div>  
            
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6">
                                                                    <?php echo $this->Form->input('telefone', array('label' => 'Telefone', 'value' => $Endereco['Endereco']['telefone'], 'class' => 'form-control input-default', 'disabled') ); ?>
                                                                </div>
                                                            </div>  
                                                        </div> 
                                                                                                            
                                                    </div>

                                                    <div class="modal-footer">                                                                    
                                                                                                                                
                                                        <button type="button" class="btn mb-1 btn-outline-dark" data-dismiss="modal"><i class="ti-close"> Fechar</i></button>                                                                    
                                                                                                                        
                                                    </div> 

                                                <?= $this->Form->end() ?>                                                              
                                                
                                            </div>
                                        </div>
                                    </div>
                                <!-- ****** Fim Modal View ****** -->

                            <?php } ?>

                        </tbody>

                    </table>

                    <br>
                    <br>
                    <br>

                </div>
            </div>
        </div>

    </div>

</div>



<!-- ****** Modal Edit ****** -->
    <div class="modal fade bd-example-modal-lg" id='ModalEdit' tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Alterar Informações pessoais</h5>
                    
                    <?php echo $this->Html->link(
                            '<i class="ti-close"></i>', array("action" => "index"), 
                            array(                                                  
                                'class' => 'close', 
                                'escape' => false
                            )
                        );?>
                    
                </div>    

                <?php echo $this->Form->create('Usuario', array('url' => array('controller' => 'Usuario', 'action' => 'edit'), 
                                'class' => 'form-horizontal', 'enctype' => 'multipart/form-data'), array('inputDefaults' => array(
                                                                    'label' => false,
                                                                    'class' => 'form-control'
                                                                )) ); ?>                                                     

                    <div class="modal-body">                                                                    
                                                                                                
                        <div class="card">
                            <div class="card-body">
                                <div class="basic-form">
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                            <?php 
                                                if (!empty($Usuarios['Usuario']['fotoPerfil'])) {

                                                    echo $this->Html->image("avatar/".$Usuarios['Usuario']['fotoPerfil'], array(
                                                        'width'=>"80", 'height'=>"80", 'class'=>"mr-3 circle-rounded"
                                                    ));

                                                } else {

                                                    echo $this->Html->image("avatar/sem_foto.jpg", array(
                                                        'width'=>"80", 'height'=>"80", 'class'=>"mr-3 circle-rounded"
                                                    ));

                                                }
                                                
                                            ?>
                                            
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input type="file" name="data[Usuario][caminho]" class="form-control-file">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <?php echo $this->Form->input('username', array('label' => 'Usuário', 'value' => $Usuarios['Usuario']['username'], 'class' => 'form-control input-default', 'required') ); ?>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <?php echo $this->Form->input('nome', array('label' => 'Nome Completo', 'value' => $Usuarios['Usuario']['nome'], 'class' => 'form-control input-default', 'required') ); ?>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <?php echo $this->Form->input('email', array('label' => 'Email', 'value' => $Usuarios['Usuario']['email'], 'class' => 'form-control input-default', 'required') ); ?>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('dtNascimento', array('label' => 'Dt. Nascimento', 'id' => 'data1', 'onkeypress'=>"formatar_mascara(this,'##/##/####')", 'maxlength'=>"10", 'type'=>'text', 'value' => date('d/m/Y', strtotime($Usuarios['Usuario']['dtNascimento'])), 'class' => 'form-control input-default', 'required') ); ?>                                            
                                        </div>
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('telefone', array('label' => 'Telefone', 'onkeypress'=>"formatar_mascara(this,'## ####-####')", 'value' => $Usuarios['Usuario']['telefone'], 'maxlength'=>"12", 'class' => 'form-control input-default', 'required') ); ?>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('celular', array('label' => 'Celular', 'onkeypress'=>"formatar_mascara(this,'## #####-####')", 'value' => $Usuarios['Usuario']['celular'], 'maxlength'=>"13", 'class' => 'form-control input-default', 'required') ); ?>
                                        </div>                                                    
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <?php echo $this->Form->input('tipoPessoa', array('label' => 'Tipo Pessoa', 'value' => $Usuarios['Usuario']['tipoPessoa'], 'options' => array('PF' => 'Pessoa Física', 'PJ'=>'Pessoa Jurídica'), 'class' => 'form-control input-default', 'onclick'=>"Mudarestado(this.value)", 'required') ); ?>
                                        </div>                                                 
                                    </div>

                                    <?php 
                                    
                                        if ($Usuarios['Usuario']['tipoPessoa'] == 'PF') {

                                            $stylePF = "block";

                                            $stylePJ = "block";

                                        } elseif ($Usuarios['Usuario']['tipoPessoa'] == 'PJ') {
                                            
                                            $stylePF = "block";

                                            $stylePJ = "block";

                                        } else {
                                            
                                            $stylePF = "none";

                                            $stylePJ = "none";

                                        }

                                    ?>
                                    
                                    <div id='PJdiv' style='display: <?php echo $stylePJ; ?>;' >

                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <?php echo $this->Form->input('cnpj', array('label' => 'CNPJ', 'value' => $Usuarios['Usuario']['cnpj'], 'class' => 'form-control input-default', 'onkeypress'=>"formatar_mascara(this,'## #####-####')") ); ?>
                                            </div>                                                   
                                        </div>
                                    
                                    </div>

                                    <div id='PFdiv'  style='display: <?php echo $stylePJ; ?>;'>

                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <?php echo $this->Form->input('cpf', array('label' => 'CPF', 'value' => $Usuarios['Usuario']['cpf'], 'class' => 'form-control input-default', 'onkeypress'=>"formatar_mascara(this,'###.###.###-##')") ); ?>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <?php echo $this->Form->input('rg', array('label' => 'RG', 'value' => $Usuarios['Usuario']['rg'], 'class' => 'form-control input-default', 'onkeypress'=>"formatar_mascara(this,'##.###.###-#')", 'maxlength'=>"12") ); ?>
                                            </div>                                                    
                                        </div>

                                    </div>                                                                   

                                    <div class="form-row">
                                        <div class="form-group col-md-9">
                                            <?php echo $this->Form->input('biografia', array('label' => 'Biografia', 'value' => $Usuarios['Usuario']['biografia'], 'type' => 'textarea', 'class' => 'form-control input-default', 'maxlength' => '200') ); ?>
                                        </div>                                                 
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                                                                            
                    </div> 

                    <div class="modal-footer">                                                                    
                                                                                                
                        <button type="submit" class="btn mb-1 btn-outline-info" onsubmit='return VerificaCampos();'><i class="ti-check"> Salvar</i></button> 

                        <?php echo $this->Html->link(
                            '<i class="ti-close"> Cancelar</i>', array("action" => "index"), 
                            array(                                                  
                                'class' => 'btn mb-1 btn-outline-dark', 
                                'escape' => false
                            )
                        );?>
                                                                                        
                    </div> 

                <?= $this->Form->end() ?>                                                              
                
            </div>
        </div>
    </div>
<!-- ****** Fim Modal Edit ****** -->

<script>

    function Mudarestado(div) {
        
        var display = document.getElementById(div+'div').style.display;
        if(div == "PF"){
            document.getElementById('PFdiv').style.display = 'block';
            document.getElementById('PJdiv').style.display = 'none';
        } else {
            document.getElementById('PFdiv').style.display = 'none';
            document.getElementById('PJdiv').style.display = 'block';
        }
            
    }

    function VerificaCampos() {
        
        if (document.getElementById('UsuarioTipoPes').value == 'PF') {

            if (document.getElementById('UsuarioCpf').value == '') {

                swal("Atenção!", "O campo CPF precisa estar preenchido!", "error");

                return false;

            } 

            if (document.getElementById('UsuarioRg').value == '') {

                swal("Atenção!", "O campo RG deve estar preenchido!", "error");

                return false;

            }

        } else if (document.getElementById('UsuarioTipoPes').value == 'PJ') { 

            if (document.getElementById('UsuarioCnpj').value == '') {

                swal("Atenção!", "O campo CNPJ deve estar preenchido!", "error");

                return false;

            } 

        }

    }


    $(document).ready(function() {

        function limpa_formulário_cep() {
            // Limpa valores do formulário de cep.
            $("#rua").val("");
            $("#bairro").val("");
            $("#cidade").val("");
            $("#uf").val("");
            $("#ibge").val("");
        }
        
        //Quando o campo cep perde o foco.
        $("#cep").blur(function() {
            
            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');
            
            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#rua").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#uf").val("...");
                    $("#ibge").val("...");

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#rua").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            $("#uf").val(dados.uf);
                            $("#ibge").val(dados.ibge);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep();
                            swal("Atenção!", "CEP não encontrado.", "error");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    swal("Atenção!", "Formato de CEP inválido.", "error");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        });
    });    

</script>