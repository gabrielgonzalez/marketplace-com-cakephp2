<?php echo $this->Form->create('User',  array('url' => array('controller' => 'Usuario', 'action' => 'troca_senha'), 
                                              'class' => 'form-horizontal', 
                                              'onSubmit' => 'return verificaSenha();',
                                              'inputDefaults' => array( 'label' => false,
                                                                        'class' => 'form-control'
                                                                     )) ); ?>

    <div class="content-body">

        <div class="container-fluid mt-3">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Alteração de Senha</h4>
                                <div class="row">  
                                    <div class="col-md-12">
                                        <div class="form-group"> 
                                                <?php echo $this->Form->label('SenhaAtual', 'Senha Atual:', array('class'=>'col-md-3 control-label')); ?>
                                                <div class="col-md-4">
                                                <?php echo $this->Form->input('senha', array('label' => false, 'type' => 'password', 'class' => 'form-control', 'onkeypress' => 'return handleEnter(this, event)')); ?>
                                                </div>
                                        </div> 
                                    </div>
                                </div>
                                
                                <div class="row">  
                                    <div class="col-md-12">
                                        <div class="form-group"> 
                                                <?php echo $this->Form->label('novaSenha', 'Nova Senha:', array('class'=>'col-md-3 control-label')); ?>
                                                <div class="col-md-4">
                                                <?php echo $this->Form->input('novaSenha', array('label' => false, 'type' => 'password', 'class' => 'form-control', 'onkeypress' => 'return handleEnter(this, event)')); ?>
                                                </div>
                                        </div> 
                                    </div>
                                </div>
                                
                                <div class="row">  
                                    <div class="col-md-12">
                                        <div class="form-group"> 
                                                <?php echo $this->Form->label('novaSenha2', 'Repetir Nova Senha:', array('class'=>'col-md-3 control-label')); ?>
                                                <div class="col-md-4">
                                                <?php echo $this->Form->input('novaSenha2', array('label' => false, 'type' => 'password', 'class' => 'form-control', 'onkeypress' => 'return handleEnter(this, event)')); ?>
                                                </div>
                                        </div> 
                                    </div>
                                </div>

                                <?php echo $this->Form->button('<i class="fa-check"></i> Confirmar', array('type' => 'submit', 'id' =>'btn_confirmar', 'class' => 'btn mb-1 btn-outline-info', 'escape' => false)); ?>                              
                        </div>
                    </div>
                    
                </div> 
            </div>

        </div>    

    </div>

<?php echo $this->Form->end(); ?>

<script>

    function verificaSenha(){
        document.getElementById('btn_confirmar').disabled = true;
        var senha  = document.getElementById('UserNovaSenha').value;
        var senha2 = document.getElementById('UserNovaSenha2').value;
    
        if(senha != senha2){
            swal('As senhas são diferentes!');

            document.getElementById('btn_confirmar').disabled = false; 
            return false;
        }
    
        if(senha.length<8){
            swal('A senha deve ter no mínimo 8 caracteres!');
            document.getElementById('btn_confirmar').disabled = false; 
            return false;
        }  
    
        if( !(senha.match(/[a-z]+/)) || 
            !(senha.match(/[A-Z]+/)) || 
            !(senha.match(/[0-9]+/)) ){
                swal('A senha deve conter ao menos:\n Um caracter maiúsculo (A-Z) \n Um caracter minúsculo (a-z) \n E um caracter numérico (0-9)');
            document.getElementById('btn_confirmar').disabled = false; 
            return false;
        }
       
        
        return true;
    }
    
</script>   