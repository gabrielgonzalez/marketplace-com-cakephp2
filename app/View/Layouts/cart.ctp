<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'S-Commerce: um jeito fácil de fazer negócio');
//$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<?php echo $this->Html->charset(); ?> 

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="S-Commerce: um jeito fácil de fazer negócio" />
	<meta name="author" content="potenzaglobalsolutions.com" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title>S-Commerce: um jeito fácil de fazer negócio</title>
	<!-- Favicon -->
	<link rel="shortcut icon" href="images/favicon.ico" />

	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(
			array('/hub/css/style',
				'/hub/css/bootstrap.min',
				'/hub/css/mega-menu/mega_menu',
				'/hub/css/font-awesome.min',
				'/hub/css/themify-icons',
				'/hub/css/owl-carousel/owl.carousel',
				'/hub/revolution/css/settings',
                '/hub/css/responsive',
                '/hub/css/slick/slick.css',
                '/hub/css/slick/slick-theme.css'
		));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');


		echo $this->Html->script(
			array('/hub/js/jquery.min',
				'/hub/js/popper.min',
				'/hub/js/bootstrap.min',
				'/hub/js/mega-menu/mega_menu',
				'/hub/js/owl-carousel/owl.carousel.min',
				'/hub/js/custom',
                '/hub/js/magnific-popup/jquery.magnific-popup.min',
                '/hub/js/slick/slick.min.js'
		));
		
	?>
</head>
<body>

    <!--=================================
   header -->

   <header id="header" class="fancy">
        <div class="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="topbar-left text-center text-md-left">
                            <ul class="list-inline">
                                <li> <i class="ti-location-pin"> </i> Manhattan, New York</li>
                                <li> <i class="ti-headphone-alt"></i>+1 234 56789</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="topbar-right text-center text-md-right">
                            <ul class="list-inline">
                                <li><?php echo $this->Html->link('Criar um conta', array( 'controller' => 'Users', 'action' => 'add' ), array('escape' => false));  ?></li>
                                <li><?php echo $this->Html->link('Fale Conosco', array( 'controller' => 'Pages', 'action' => 'fale_conosco' ), array('escape' => false));  ?></li>
                                <li>
                                    <?php 
                                        if (!empty($this->Session->read('User.username'))) {
                                            echo $this->Html->link('<i class="ti-user"> </i> '. $this->Session->read('User.username'), array( 'controller' => 'Painel', 'action' => 'index' ), array('escape' => false)); 
                                        } else {
                                            echo $this->Html->link('<i class="ti-user"> </i> Login', array( 'controller' => 'Users', 'action' => 'login' ), array('escape' => false)); 
                                        }
                                        
                                    ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>    

    <!--=================================
header -->



<!--=================================
    mega menu -->

	<div id="container">
		<div id="content">

			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
	</div>
	<!--=================================
footer -->

    <footer class="footer footer-topbar page-section-pt">        
        <div class="container">
            <div class="row">
                
                <div class="col-md-6">
                    <div class="text-right">
                        <p>Copyright © 2019 S-Commerce. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

<!--=================================
footer -->

  

    <!--=================================
back to top 
    <div class="back-to-top">
        <span><img src="images/rocket.png" data-src="images/rocket.png" data-hover="images/rocket.gif" alt=""></span>
    </div>-->
    <!--=================================
back to top -->

</body>
</html>