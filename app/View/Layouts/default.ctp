<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

//$cakeDescription = __d('cake_dev', 'S-Commerce: um jeito fácil de fazer negócio');
//$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<?php echo $this->Html->charset(); ?> 

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="S-Commerce: um jeito fácil de fazer negócio" />
	<meta name="author" content="potenzaglobalsolutions.com" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title>S-Commerce: um jeito fácil de fazer negócio</title>
	<!-- Favicon -->
	<link rel="shortcut icon" href="scommerce/img/favicon.ico" />

	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(
			array('/hub/css/style',
				'/hub/css/bootstrap.min',
				'/hub/css/mega-menu/mega_menu',
				'/hub/css/font-awesome.min',
				'/hub/css/themify-icons',
				'/hub/css/owl-carousel/owl.carousel',
				'/hub/revolution/css/settings',
                '/hub/css/responsive',
                '/hub/css/slick/slick.css',
                '/hub/css/slick/slick-theme.css',
                '/lab/plugins/sweetalert/css/sweetalert.css'
		));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');


		echo $this->Html->script(
			array('/hub/js/jquery.min',
				'/hub/js/popper.min',
				'/hub/js/bootstrap.min',
				'/hub/js/mega-menu/mega_menu',
				'/hub/js/owl-carousel/owl.carousel.min',
				'/hub/js/custom',
                '/hub/js/magnific-popup/jquery.magnific-popup.min',
                '/hub/js/slick/slick.min.js',
                '/lab/plugins/sweetalert/js/sweetalert.min.js',
                '/lab/plugins/sweetalert/js/sweetalert.init.js'
		));
		
	?>
</head>
<body>

    <!--=================================
   header -->

   <header id="header" class="fancy">
        <div class="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="topbar-left text-center text-md-left">
                            <ul class="list-inline">
                                <li> <i class="ti-location-pin"> </i> Manhattan, New York</li>
                                <li> <i class="ti-headphone-alt"></i>+1 234 56789</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="topbar-right text-center text-md-right">
                            <ul class="list-inline">
                                <li><?php echo $this->Html->link('Criar um conta', array( 'controller' => 'Users', 'action' => 'add' ), array('escape' => false));  ?></li>
                                <li><?php echo $this->Html->link('Fale Conosco', array( 'controller' => 'Pages', 'action' => 'fale_conosco' ), array('escape' => false));  ?></li>
                                <li>
                                    <?php 
                                        if (!empty($this->Session->read('User.username'))) {
                                            echo $this->Html->link('<i class="ti-user"> </i> '. $this->Session->read('User.username'), array( 'controller' => 'Painel', 'action' => 'index' ), array('escape' => false)); 
                                        } else {
                                            echo $this->Html->link('<i class="ti-user"> </i> Login', array( 'controller' => 'Users', 'action' => 'login' ), array('escape' => false)); 
                                        }
                                        
                                    ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="menu">
            <!-- menu start -->
            <nav id="menu" class="mega-menu">
                <!-- menu list items container -->
                <section class="menu-list-items">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- menu logo -->
                                <ul class="menu-logo">
                                    <li>
                                        <?php 
                                            echo $this->Html->image("Logo_scommerce.png", array(
                                                "alt" => "S-Commerce: um jeito fácil de fazer negócio",
                                                'url' => array('controller' => 'Pages', 'action' => 'home')
                                            ));
                                        ?>
                                    </li>
                                </ul>
                                <!-- menu links -->
                                <ul class="menu-links">
                                    
                                    <?php 
                                    
                                    $Departamentos = $this->Session->read('Departamentos');

                                    foreach ($Departamentos as $key => $Departamento) { ?>
                                        
                                        <li><a href="javascript:void(0)"> <?php echo $Departamento['Departamentos']['descricao']; ?> <i class="fa fa-angle-down fa-indicator"></i></a>

                                        <!-- drop down multilevel  -->
                                        <div class="drop-down menu-bg grid-col-12">
                                            <!--grid row-->
                                            <div class="grid-row">
                                                <!--grid column 3-->

                                                        <?php foreach ($Departamento['Departamentos']['Categoria'] as $key => $Cat) { ?>

                                                            <div class="grid-col-3">
                                                                <a href="about-01.html"><h4><?php echo $Cat['descricao']; ?></h4></a>
                                                                <ul>
                                                                    <?php foreach ($Cat['Subcategoria'] as $key => $Subcat) { ?>

                                                                        <li>
                                                                            <?php echo $this->Html->link($Subcat['descricao'], array( 'controller' => 'Pages', 'action' => 'subcategoria', $Subcat['idSubCategoria']), array('escape' => false)); ?>
                                                                        </li>

                                                                    <?php } ?> 

                                                                </ul>
                                                            </div>

                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                
                                            <?php } ?>
                                            
                                        </li>                                         
                                        
                                    

                                    <li>
                                        

                                        <?php 

                                            $carrinho = $this->Session->read('Carrinho'); 
                                            
                                            if ($carrinho['Total'] > 0) {
                                                $title = "Você possui itens no carrinho!" ;
                                            } else {
                                                $title = "Você não tem nenhum item no carrinho." ;
                                            }

                                            echo $this->Html->link('<i class="fa fa-shopping-cart"></i> '. $carrinho['Total']. '<div class="mobileTriggerButton"></div>', array( 'controller' => 'Pages', 'action' => 'novo_cart'), array('escape' => false, 'data-toggle'=>"tooltip", 'data-placement'=>"bottom", 'title'=> $title));
                                        
                                        ?> 

                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </nav>
            <!-- menu end -->
        </div>
    </header>    

    <!--=================================
header -->



<!--=================================
    mega menu -->

	<div id="container">
		<div id="content">

			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
	</div>
	<!--=================================
footer -->

<footer class="footer footer-topbar page-section-pt">
        <div class="container">
            <div class="row top">
                <div class="col-lg-3 col-md-2">
                    <?php 
                        echo $this->Html->image("Logo_scommerce_footer.jpg", array(
                            "alt" => "S-Commerce: um jeito fácil de fazer negócio",
                            'url' => array('controller' => 'Pages', 'action' => 'home')
                        ));
                    ?>
                </div>
                <div class="col-lg-5 col-md-6">
                    <div class="footer-nav text-right">
                        <ul>
                            <li><?php echo $this->Html->link('Home', array( 'controller' => 'Pages', 'action' => 'home' ), array('escape' => false));  ?></li>
                            <li><?php echo $this->Html->link('Sobre o Projeto', array( 'controller' => 'Pages', 'action' => 'about' ), array('escape' => false));  ?></li>
                            <li><?php echo $this->Html->link('Fale Conosco', array( 'controller' => 'Pages', 'action' => 'fale_conosco' ), array('escape' => false));  ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-5">
                    <div class="about-content">
                        <h6 class="mb-2">Sobre nós</h6>
                        <p>Este projeto tem como finalidade apresentar uma ferramenta de controle e auxílio para a criação de lojas virtuais, diminuindo gastos e aumentando o lucro de qualquer negócio que utilize essa ferramenta.</p>
                        <p>Com o simples objetivo de trazer mais praticidade para os usuários e lucros para seus negócios, muitos empreendedores estão investindo nas lojas virtuais para aumentarem suas vendas e a satisfação de seus clientes. </p>
                    </div>
                </div>
            </div>
            <div class="row mt-8">
                <div class="col-md-4 mb-4">
                    <div class="contact-box">
                        <div class="contact-icon">
                            <i class="ti-direction-alt"></i>
                        </div>
                        <div class="contact-info">
                            <h5>25, King St. 20170</h5>
                            <span>Melbourne Australia</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="contact-box">
                        <div class="contact-icon">
                            <i class="ti-headphone-alt"></i>
                        </div>
                        <div class="contact-info">
                            <h5>0011 234 56789</h5>
                            <span>Mon-Fri 8:30am-6:30pm</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact-box">
                        <div class="contact-icon">
                            <i class="ti-email"></i>
                        </div>
                        <div class="contact-info">
                            <h5>info@yoursite.com</h5>
                            <span>24 X 7 online support</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright mt-6">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list-inline text-left">
                           <li><a href="#">Terms & Conditions </a> &nbsp;&nbsp;&nbsp;|</li>
                           <li><a href="#">API Use Policy </a> &nbsp;&nbsp;&nbsp;|</li>
                           <li><a href="#">Privacy Policy </a> &nbsp;&nbsp;&nbsp;|</li>
                           <li><a href="#">Cookies </a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <div class="text-right">
                            <p>Copyright © 2019 S-Commerce. All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

<!--=================================
footer -->

  

    <!--=================================
back to top 
    <div class="back-to-top">
        <span><img src="images/rocket.png" data-src="images/rocket.png" data-hover="images/rocket.gif" alt=""></span>
    </div>-->
    <!--=================================
back to top -->

</body>
</html>

<script> 


    $('[data-toggle="tooltip"]').tooltip()

</script>