<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>S-Commerce: um jeito fácil de fazer negócio</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(
            array('/lab/css/style',
            '/lab/plugins/sweetalert/css/sweetalert.css'
		));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');


		echo $this->Html->script(
			array('/lab/plugins/common/common.min',
				'/lab/js/custom.min',
				'/lab/js/settings',
                '/lab/js/styleSwitcher', 
                '/lab/plugins/sweetalert/js/sweetalert.min.js',
                '/lab/plugins/sweetalert/js/sweetalert.init.js'
        ));
		
	?>
    
</head>

<body class="h-100">
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <div id="container">
		<div id="content">
            <?php echo $this->Session->flash(); ?>
			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
	</div>

    
</body>
</html>





