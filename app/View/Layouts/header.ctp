<!--**********************************
    Nav header start
***********************************-->
<div class="nav-header">
    <div class="brand-logo">
        <?php 
            echo $this->Html->image("Logo_scommerce.png", array(
                "alt" => "S-Commerce: um jeito fácil de fazer negócio",
                'width' => '100px', 
                'url' => array('controller' => 'Pages', 'action' => 'home')
            ));
        ?>
    </div>
</div>
<!--**********************************
    Nav header end
***********************************-->


<!--**********************************
    Header start
***********************************-->
<div class="header">    
    <div class="header-content clearfix">
        
        <div class="nav-control">
            <div class="hamburger">
                <span class="toggle-icon"><i class="icon-menu"></i></span>
            </div>
        </div>
        <div class="header-right">
            <ul class="clearfix">
                
                <li class="icons dropdown">
                    <div class="user-img c-pointer position-relative"   data-toggle="dropdown">
                        <span class="activity active"></span>
                        <?php 
                            if (!empty($Usuarios['Usuario']['fotoPerfil'])) {

                                echo $this->Html->image("avatar/".$Usuarios['Usuario']['fotoPerfil'], array(
                                    'width'=>"40", 'height'=>"40"
                                ));

                            } else {

                                echo $this->Html->image("avatar/sem_foto.jpg", array(
                                    'width'=>"40", 'height'=>"40"
                                ));

                            }
                            
                        ?>
                    </div>
                    <div class="drop-down dropdown-profile   dropdown-menu">
                        <div class="dropdown-content-body">
                            <ul>
                                <li>
                                    <?php echo $this->Html->link('<i class="icon-user"></i> Meus Dados', array( 'controller' => 'Usuario', 'action' => 'index' ), array('escape' => false)); ?>
                                </li>
                                
                                <hr class="my-2">
                                <li>
                                    <?php echo $this->Html->link('<i class="icon-lock"></i> Trocar Senha', array( 'controller' => 'Usuario', 'action' => 'troca_senha' ), array('escape' => false)); ?>
                                </li>
                                <li>
                                    <?php echo $this->Html->link('<i class="icon-key"></i> Logout', array( 'controller' => 'Users', 'action' => 'logout' ), array('escape' => false)); ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--**********************************
    Header end ti-comment-alt
***********************************-->