<!--**********************************
    Sidebar start
***********************************-->
<div class="nk-sidebar">           
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label">Dashboard</li>
            <li>
                <?php echo $this->Html->link('<i class="icon-basket-loaded menu-icon"></i> Loja', array( 'controller' => 'Pages', 'action' => 'home' ), array('escape' => false)); ?>
            </li>
            <li>
                <?php echo $this->Html->link('<i class="icon-speedometer menu-icon"></i> Dashboard', array( 'controller' => 'Painel', 'action' => 'index' ), array('escape' => false)); ?>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-docs menu-icon"></i><span class="nav-text">Relatórios</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <?php echo $this->Html->link('Histórico de Compras', array( 'controller' => 'Relatorios', 'action' => 'rel001' ), array('escape' => false)); ?>
                    </li> 
                    <li><?php echo $this->Html->link('Histórico de Itens Vendidos', array( 'controller' => 'Relatorios', 'action' => 'rel002'), array('escape' => false)); ?></li>
                </ul>
            </li>
            
            <li>
                <?php echo $this->Html->link('<i class="icon-badge menu-icon"></i> <span class="nav-text">Avaliações</span>', array( 'controller' => 'Avaliacoes', 'action' => 'index' ), array('escape' => false)); ?>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-basket-loaded menu-icon"></i><span class="nav-text">Compras</span>
                </a>
                <ul aria-expanded="false">
                    <li>
                        <?php echo $this->Html->link('Lista de Desejos', array( 'controller' => 'ListaDesejos', 'action' => 'index' ), array('escape' => false)); ?>
                    </li> 
                    <li><?php echo $this->Html->link('Meus Pedidos', array( 'controller' => 'Pedidos', 'action' => 'index' )); ?></li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-tag menu-icon"></i> <span class="nav-text">Vendas</span>
                </a>
                <ul aria-expanded="false">
                    <li><?php echo $this->Html->link('Seus Anúncios', array( 'controller' => 'Anuncios', 'action' => 'index' )); ?></li>
                    <li><?php echo $this->Html->link('Vendas', array( 'controller' => 'Vendas', 'action' => 'index' )); ?></li>
                </ul>
            </li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-settings menu-icon"></i><span class="nav-text">Configurações</span>
                </a>
                <ul aria-expanded="false">
                    <li><?php echo $this->Html->link('Meus Dados', array( 'controller' => 'Usuario', 'action' => 'index' )); ?></li>
                    
                </ul>
            </li>            
        </ul>
    </div>
</div>
<!--**********************************
    Sidebar end
***********************************-->