<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php echo $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>S-Commerce: um jeito fácil de fazer negócio</title>

    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
    <!-- Favicon icon -->
    <link rel="icon" type="img/png" sizes="16x16" href="scommerce/img/favicon.png">

    <?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(
            array(
                '/lab/css/style', 
                '/lab/plugins/jquery-steps/css/jquery.steps', 
                '/lab/plugins/highlightjs/styles/darkula',
                '/lab/plugins/sweetalert/css/sweetalert.css'
            )
        );

        echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		
	?>

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">              
        
        <div id="header">
            
            <?php require_once 'header.ctp'; ?>
    
        </div>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <?php echo $this->fetch('content'); ?>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © 2019 S-Commerce. All Rights Reserved.</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************--> 

    <!--**********************************
        Scripts
    ***********************************-->
    <?php 

        echo $this->Html->script(
            array('/lab/plugins/common/common.min',
                '/lab/js/custom.min',
                '/lab/js/settings',
                '/lab/js/styleSwitcher',
                '/lab/plugins/highlightjs/highlight.pack.min',
                '/lab/js/gleek',
                '/lab/plugins/jquery-steps/build/jquery.steps.min',
                '/lab/plugins/jquery-validation/jquery.validate.min',
                '/lab/js/plugins-init/jquery-steps-init',
                '/lab/plugins/sweetalert/js/sweetalert.min.js',
                '/lab/plugins/sweetalert/js/sweetalert.init.js',
                '/hub/js/bootstrap.min',
                '/hub/js/mega-menu/mega_menu',
                '/hub/js/owl-carousel/owl.carousel.min',
                '/hub/js/custom',
                '/hub/js/magnific-popup/jquery.magnific-popup.min'
        ));

    ?>
    
    <script> 
        
        function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
            var sep = 0;
            var key = '';
            var i = j = 0;
            var len = len2 = 0;
            var strCheck = '0123456789';
            var aux = aux2 = '';
            var whichCode = (window.Event) ? e.which : e.keyCode;
            if (whichCode == 13 | whichCode == 0 | whichCode == 8) return true;
            key = String.fromCharCode(whichCode); // Valor para o código da Chave
            if (strCheck.indexOf(key) == -1) return false; // Chave inválida
            len = objTextBox.value.length;
            for(i = 0; i < len; i++)
                if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
            aux = '';
            for(; i < len; i++)
                if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
            aux += key;
            len = aux.length;
            if (len == 0) objTextBox.value = '';
            if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
            if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
            if (len > 2) {
                aux2 = '';
                for (j = 0, i = len - 3; i >= 0; i--) {
                    if (j == 3) {
                        aux2 += SeparadorMilesimo;
                        j = 0;
                    }
                    aux2 += aux.charAt(i);
                    j++;
                }
                objTextBox.value = '';
                len2 = aux2.length;
                for (i = len2 - 1; i >= 0; i--)
                objTextBox.value += aux2.charAt(i);
                objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
            }
            return false;
        }

    </script>

</body>

</html>