<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>S-Commerce: um jeito fácil de fazer negócio</title>
    <!-- Favicon icon -->
    <link rel="icon" type="img/png" sizes="16x16" href="img/favicon.png">

    <?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(
            array(
                'style', 
                '/plugins/jquery-steps/css/jquery.steps.css', 
                '/plugins/highlightjs/styles/darkula.css'
            )
        );

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');


		echo $this->Html->script(
			array('/plugins/common/common.min.js',
				'/js/custom.min.js',
                '/js/settings.js',
				'/js/styleSwitcher.js',
                '/plugins/highlightjs/highlight.pack.min.js',
                '/plugins/select2/js/select2.full.min.js',
                '/plugins/jquery-steps/build/jquery.steps.min.js',
                '/plugins/jquery-validation/jquery.validate.min.js',
                '/js/plugins-init/jquery-steps-init.js'
        )); 
		
	?>

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="index.html">
                    <b class="logo-abbr"><img src="img/logo.png" alt=""> </b>
                    <span class="logo-compact"><img src="./img/logo-compact.png" alt=""></span>
                    <span class="brand-title">
                        <img src="img/logo-text.png" alt="">
                    </span>
                </a>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <?php require_once 'header.ctp'; ?>

        <?php require_once 'sidebar.ctp'; ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <?php echo $this->fetch('content'); ?>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright &copy; Designed & Developed by <a href="https://themeforest.net/user/quixlab">Quixlab</a> 2018</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="./plugins/highlightjs/highlight.pack.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>

    <script>
        (function($) {
        "use strict"

            new quixSettings({
                version: "light", //2 options "light" and "dark"
                layout: "horizontal", //2 options, "vertical" and "horizontal"
                navheaderBg: "color_1", //have 10 options, "color_1" to "color_10"
                headerBg: "color_1", //have 10 options, "color_1" to "color_10"
                sidebarStyle: "full", //defines how sidebar should look like, options are: "full", "compact", "mini" and "overlay". If layout is "horizontal", sidebarStyle won't take "overlay" argument anymore, this will turn into "full" automatically!
                sidebarBg: "color_1", //have 10 options, "color_1" to "color_10"
                sidebarPosition: "static", //have two options, "static" and "fixed"
                headerPosition: "static", //have two options, "static" and "fixed"
                containerLayout: "wide",  //"boxed" and  "wide". If layout "vertical" and containerLayout "boxed", sidebarStyle will automatically turn into "overlay".
                direction: "ltr" //"ltr" = Left to Right; "rtl" = Right to Left
            });


        })(jQuery);
    </script>

    <script>

        function formatar_mascara(src, mascara) {
            var campo = src.value.length;
            var saida = mascara.substring(0,1);
            var texto = mascara.substring(campo);
            if(texto.substring(0,1) != saida) {
                src.value += texto.substring(0,1);
            }
        }

    </script>

</body>

</html>