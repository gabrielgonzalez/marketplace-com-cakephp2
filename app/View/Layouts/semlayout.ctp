<?php
/**
 *
 * PHP 5
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$titulo = $this->Session->read('Titulo'); 

$cakeDescription = __d('cake_dev', 'Clinica Escola');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
                 //<!-- BEGIN GLOBAL MANDATORY STYLES -->      
		echo $this->Html->css(array('/plugins/font-awesome/css/font-awesome.min',
                                            '/plugins/bootstrap/css/bootstrap.min',
                                            '/plugins/uniform/css/uniform.default',
                                            //<!-- END GLOBAL MANDATORY STYLES -->
                                            //<!-- BEGIN PAGE LEVEL STYLES -->
                                            '/plugins/bootstrap-fileupload/bootstrap-fileupload',
                                            '/plugins/gritter/css/jquery.gritter',
                                            '/plugins/select2/select2_metro',
                                            '/plugins/clockface/css/clockface',
                                            '/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5',
                                            '/plugins/bootstrap-datepicker/css/datepicker',
                                            '/plugins/bootstrap-timepicker/compiled/timepicker',
                                            '/plugins/bootstrap-colorpicker/css/colorpicker',
                                            '/plugins/bootstrap-daterangepicker/daterangepicker-bs3',
                                            '/plugins/bootstrap-datetimepicker/css/datetimepicker',
                                            '/plugins/jquery-multi-select/css/multi-select',
                                            '/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro',
                                            '/plugins/jquery-tags-input/jquery.tagsinput',
                                            '/plugins/data-tables/DT_bootstrap',
                                            '/plugins/fancybox/source/jquery.fancybox',
                                            '/plugins/jquery-file-upload/css/jquery.fileupload-ui',
                    
                                            //<!-- END PAGE LEVEL STYLES -->
                                            //<!-- BEGIN PLUGINS USED BY X-EDITABLE -->
                                            '/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable',
                                            '/plugins/bootstrap-editable/inputs-ext/address/address',
                                            //<!-- END PLUGINS USED BY X-EDITABLE -->
                                            //<!-- BEGIN THEME STYLES --> 
                                            'style-metronic','style','style-responsive','plugins',
                                            'themes/'.$this->Session->read('temaprincipal'),'custom','pages/search', 
                                            //<!-- END THEME STYLES -->   
                                            'datepicker/jquery-ui', 


                                            '/lab/css/style', 
                                                '/lab/plugins/jquery-steps/css/jquery.steps', 
                                                '/lab/plugins/highlightjs/styles/darkula'
                                            )
                                    );            
                
                
                    
                
                                        
                                        //<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
                                        //<!-- BEGIN CORE PLUGINS -->   
                                       /* <!--[if lt IE 9]>
                                        '/plugins/respond.min',
                                        '/plugins/excanvas.min',
                                        <![endif]-->   */
         echo $this->Html->script(array(
                                        '/lab/plugins/common/common.min',
                                        '/lab/js/custom.min',
                                        '/lab/js/settings',
                                        '/lab/js/gleek',
                                        '/lab/js/styleSwitcher',
                                        '/lab/plugins/highlightjs/highlight.pack.min',
                                        '/lab/plugins/select2/js/select2.full.min',
                                        '/lab/plugins/jquery-steps/build/jquery.steps.min',
                                        '/lab/plugins/jquery-validation/jquery.validate.min',
                                        '/lab/js/plugins-init/jquery-steps-init',
                                        '/plugins/jquery-1.10.2.min',
                                        '/plugins/jquery-migrate-1.2.1.min',    
                                        '/plugins/bootstrap/js/bootstrap.min',
                                        '/plugins/bootstrap/js/bootstrap2-typeahead.min',
                                        '/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min',
                                        '/plugins/jquery-slimscroll/jquery.slimscroll.min',
                                        '/plugins/jquery.blockui.min', 
                                        '/plugins/jquery.cookie.min',
                                        '/plugins/uniform/jquery.uniform.min',
                                        //<!-- END CORE PLUGINS -->
                                        //<!-- BEGIN PAGE LEVEL PLUGINS -->
                                        '/plugins/fuelux/js/spinner.min',
                                        '/plugins/ckeditor/ckeditor',
                                        '/plugins/bootstrap-fileupload/bootstrap-fileupload',
                                        '/plugins/select2/select2.min',
                         
                                        '/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0',                                        
                                        '/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5',
                                        '/plugins/bootstrap-wysihtml5/locales/bootstrap-wysihtml5.pt-BR',
                                        
                                        '/plugins/bootstrap-datepicker/js/bootstrap-datepicker',
                                        '/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker',
                                        '/plugins/clockface/js/clockface',
                                        '/plugins/bootstrap-daterangepicker/moment.min',
                                        '/plugins/bootstrap-daterangepicker/daterangepicker',
                                        '/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker',
                                        '/plugins/bootstrap-timepicker/js/bootstrap-timepicker',
                                        '/plugins/jquery-inputmask/jquery.inputmask.bundle.min',
                                        '/plugins/jquery.input-ip-address-control-1.0.min',
                                        '/plugins/jquery-multi-select/js/jquery.multi-select',
                                        '/plugins/jquery-multi-select/js/jquery.quicksearch',
                                        '/plugins/jquery.pwstrength.bootstrap/src/pwstrength',
                                        '/plugins/bootstrap-switch/static/js/bootstrap-switch.min',
                                        '/plugins/jquery-tags-input/jquery.tagsinput.min',
                                        '/plugins/data-tables/jquery.dataTables',
                                        '/plugins/data-tables/DT_bootstrap',                 
                                        '/plugins/fancybox/source/jquery.fancybox.pack',
             
                                        '/plugins/jquery-file-upload/js/vendor/jquery.ui.widget',
                                        '/plugins/jquery-file-upload/js/vendor/tmpl.min',
                                        '/plugins/jquery-file-upload/js/vendor/load-image.min',
                                        '/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min',
                                        '/plugins/jquery-file-upload/js/jquery.iframe-transport',
                                        /*'/plugins/jquery-file-upload/js/jquery.fileupload',
                                        '/plugins/jquery-file-upload/js/jquery.fileupload-process',
                                        '/plugins/jquery-file-upload/js/jquery.fileupload-image',
                                        '/plugins/jquery-file-upload/js/jquery.fileupload-audio',
                                        '/plugins/jquery-file-upload/js/jquery.fileupload-video',
                                        '/plugins/jquery-file-upload/js/jquery.fileupload-validate',
                                        '/plugins/jquery-file-upload/js/jquery.fileupload-ui',*/
             
                                        //<!-- END PAGE LEVEL PLUGINS -->
                                        
                                        '/plugins/moment.min',
                                        '/plugins/jquery.mockjax',
                                        '/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min',
                                        '/plugins/bootstrap-editable/inputs-ext/address/address',
                                        //'/plugins/bootstrap-editable/inputs-ext/wysihtml5/wysihtml5',                                        
             
                                        //<!-- BEGIN PAGE LEVEL SCRIPTS -->
                                        'app','form-components', 'form-samples', 'form-editable', 'table-managed','custom', 'table-managed', 
                                        //'form-fileupload',
                                        //<!-- END PAGE LEVEL SCRIPTS -->
                                        'datepicker/jquery_ui', 'jquery.maskedinput.min',//,'datepicker/jquery-1.9.1.js'
             
                                        )
                                   );    
         
         
		echo $this->fetch('script');   
                
                
		echo $this->fetch('meta');
		echo $this->fetch('css');
	?>
    
</head>
<body >
    <div id="header">
            <?php require_once 'header.ctp'; ?>
    </div>
    <div class="clearfix"></div>
    
			<?php //echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>

    <?php //echo $this->element('sql_dump'); ?>
    
    <div id="footer" align="right">
            <?php echo $this->Html->link(
                            $this->Html->image('gsa_rodape.png', array('alt' => $cakeDescription, 'border' => '0')),
                            'http://www.gsasolucoes.com.br/',
                            array('target' => '_blank', 'escape' => false)
                    );
            ?>
    </div>
       
</body>
</html>

