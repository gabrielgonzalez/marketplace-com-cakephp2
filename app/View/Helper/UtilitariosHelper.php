<?php

class UtilitariosHelper extends Helper {
    
        function export_report_all_format($file_type, $filename, $ahtml, $orientation, $pasta){    


            if($file_type == 'pdf')
            {                
                App::import('Vendor', 'mpdf', array('file' => 'mpdf' . DS . '/vendor/autoload.php'));
                
                if(is_array($ahtml)){
                    $i=0;
                    foreach($ahtml as $html):
                                                
                        $this->mpdf = new \Mpdf\Mpdf();
                        $this->mpdf->WriteHTML($html["html"]);
                        $filename = $html['filename'];
                        $filename = utf8_decode($filename);
                        $filename = preg_replace('/([\200-\277])/e', "'&#'.(ord('\\1')).';'", $filename); 
                        $file_to_save = $pasta."$filename.pdf";
                        $this->mpdf->Output($file_to_save, 'F'); 

                        $i++;                        
                        
                    endforeach;                    

                    echo "<script>alert('Foram geradas $i Boletos. Os arquivos foram salvos em: $pasta'); window.close(); </script>"; 

                }else{
                    
                    // VERIFICO QUAL É A ORIENTAÇÃO DA PÁGINA 
                    if ($orientation == 'landscape') { // PAGINA CONFIGURADA PARA SAIR NO MODO PAISAGEM

                        $orientation = 'A4-L';

                    } else { // PAGINA CONFIGURADA PARA SAIR NO MODO RETRATO

                        $orientation = 'A4';

                    }

                    $this->mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => $orientation]);
                    $this->mpdf->WriteHTML($ahtml);
                                                                
                    //save the pdf file on the server
                    $this->mpdf->output();                    
                    
                }
                die();
            }    
            else if($file_type == "xls")
            {    
                $file = $filename.".xls";
                header('Content-Type: text/html');
                header("Content-type: application/x-msexcel"); //tried adding  charset='utf-8' into header
                header("Content-Disposition: attachment; filename=$file");  
                echo $ahtml;

            }
            else if($file_type == 'word')
            {                
                $file = $filename.".doc";
                header("Content-type: application/vnd.ms-word");
                header("Content-Disposition: attachment;Filename=$file");
                echo $ahtml;

            }


        }

        
        function valorPorExtenso($valor=0) {
                $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
                $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");

                $c = array("", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
                $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
                $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
                $u = array("", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove");

                $z=0;
                $rt = ""; 
                
                
                
                $valor = number_format($valor, 2, ".", ".");
                $inteiro = explode(".", $valor);
                
                
                
                for($i=0;$i<count($inteiro);$i++)
                        for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
                                $inteiro[$i] = "0".$inteiro[$i];

                // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;) 
                $fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
                for ($i=0;$i<count($inteiro);$i++) {
                    
                    //recebe o valor quebrado
                    $valor = $inteiro[$i]; 
                    
                    $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
                    
                    $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
                    
                    //$ru = ($valor > 0) ? (($valor[1] === 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

                    if ( ($valor >= 10) && ($valor[1] == 1) )  {

                        $ru = $d10[$valor[2]];

                    } else {

                        $ru = $u[$valor[2]];

                    }

                    $r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && $ru) ? " e " : "").$ru;
                    $t = count($inteiro)-1-$i;
                    $r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
                    if ($valor == "000")$z++; elseif ($z > 0) $z--;
                    if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t]; 
                    if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;

                }
                
                return($rt ? $rt : "zero");
        }
        
        
        function removeCaracteresEsp($str) {
            $str = preg_replace('/[áàãâä]/ui', 'a', $str);
            $str = preg_replace('/[éèêë]/ui', 'e', $str);
            $str = preg_replace('/[íìîï]/ui', 'i', $str);
            $str = preg_replace('/[óòõôö]/ui', 'o', $str);
            $str = preg_replace('/[úùûü]/ui', 'u', $str);
            $str = preg_replace('/[ç]/ui', 'c', $str);
            // $str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $str);
            //$str = preg_replace('/[^a-z0-9]/i', '_', $str);
            //$str = preg_replace('/_+/', '_', $str); // ideia do Bacco :)
            return strtoupper($str);
        }

        function mesExtenso($mes){
            switch ($mes){
               case '01': $retorno = 'Janeiro'; break;
               case '02': $retorno = 'Fevereiro'; break;
               case '03': $retorno = 'Março'; break;
               case '04': $retorno = 'Abril'; break;
               case '05': $retorno = 'Maio'; break;
               case '06': $retorno = 'Junho'; break;
               case '07': $retorno = 'Julho'; break;
               case '08': $retorno = 'Agosto'; break;
               case '09': $retorno = 'Setembro'; break;
               case '10': $retorno = 'Outubro'; break;
               case '11': $retorno = 'Novembro'; break;
               case '12': $retorno = 'Dezembro'; break;
               default: $retorno = ''; break;
            }
            return $retorno;
        }
}
