
<div class="content-body">

    <div class="container-fluid mt-3">
    
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body pb-0 d-flex justify-content-between">
                                <div>
                                    <h4 class="mb-1">Departamentos</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li class="d-inline-block mr-3">

                                            <!-- Botão para acionar registro -->
                                            <button type="button" class="btn mb-1 btn-sm btn-outline-info" data-toggle="modal" data-target="#ModalAdd" title='Clique para cadastar um novo Departamento'>
                                                <i class="ti-plus"> Novo Departamento</i>
                                            </button>

                                            <!-- ****** Modal Add ****** -->
                                                <div class="modal fade bd-example-modal-lg" id='ModalAdd' tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Cadastro de Departamentos</h5>
                                                                
                                                                <?php echo $this->Html->link(
                                                                            '<i class="ti-close"></i>', array("action" => "index"), 
                                                                            array(                                                  
                                                                                'class' => 'close', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>

                                                            </div>    

                                                            <?php echo $this->Form->create('Departamento', array('url' => array('controller' => 'Departamento', 'action' => 'add'), 
                                                                            'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                'label' => false,
                                                                                                                'class' => 'form-control'
                                                                                                            )) ); ?>                                                     

                                                                <div class="modal-body">                                                                    
                                                                                                                                            
                                                                    <div class="basic-form">
                                                                        <div class="form-row">
                                                                            <div class="form-group col-md-12">
                                                                                <label>Descrição <span class="text-danger">*</span></label>
                                                                                <input type="text" name='descricao' maxlength='100' class="form-control" placeholder="Digite o nome do departamento" required>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-row">
                                                                            <div class="form-group col-md-6">
                                                                                <label>Desc. Abreviada</label>
                                                                                <input type="text" name='descricaoreduzida' maxlength='50' class="form-control" placeholder="Digite o apelido do departamento">
                                                                            </div>
                                                                        </div>  
                                                                    </div> 
                                                                                                                        
                                                                </div> 

                                                                <div class="modal-footer">                                                                    
                                                                                                                                            
                                                                    <button type="submit" class="btn mb-1 btn-outline-info"><i class="ti-check"> Salvar</i></button> 

                                                                    <?php echo $this->Html->link(
                                                                            '<i class="ti-close"> Cancelar</i>', array("action" => "index"), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-outline-dark', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>
                                                                                                                                    
                                                                </div> 

                                                            <?= $this->Form->end() ?>                                                              
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            <!-- ****** Fim Modal Add ****** -->

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th scope="col" width='30%'> </th>
                                                <th scope="col" width='10%'><?= $this->Paginator->sort('idDepartamento', 'Cód.') ?></th>
                                                <th scope="col" width='30%'><?= $this->Paginator->sort('descricao', 'Descrição') ?></th>
                                                <th scope="col" width='30%'><?= $this->Paginator->sort('descricaoreduzida', 'Desc. Reduzida') ?></th>                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($Departamentos as $Departamento): ?>
                                                <tr>
                                                    <td class="actions">
                                                        
                                                        <!-- Botão para visualizar registro -->
                                                        <button type="button" class="btn mb-1 btn-sm btn-outline-warning" data-toggle="modal" data-target="#ModalView<?php echo $Departamento['Departamento']['idDepartamento']; ?>" title='Clique para ver mais informações'>
                                                            <i class="ti-eye"> Ver</i>
                                                        </button>
                                                        
                                                        <!-- Botão para editar registro -->
                                                        <button type="button" class="btn mb-1 btn-sm btn-outline-success" data-toggle="modal" data-target="#ModalEdit<?php echo $Departamento['Departamento']['idDepartamento']; ?>" title='Clique para editar'>
                                                            <i class="ti-pencil-alt"> Editar</i>
                                                        </button>

                                                        <?php echo $this->Html->link(
                                                                            '<i class="ti-trash"> Deletar</i>', array("action" => "delete", $Departamento['Departamento']['idDepartamento']), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-sm btn-outline-danger', 
                                                                                'escape' => false, 
                                                                                'confirm' => 'Tem certeza que deseja apagar este registro?'
                                                                            )
                                                                        );?>
                                                    </td>
                                                    <td><?= $this->Number->format($Departamento['Departamento']['idDepartamento']) ?></td>
                                                    <td><?= h($Departamento['Departamento']['descricao']) ?></td>
                                                    <td><?= h($Departamento['Departamento']['descricaoreduzida']) ?></td>                                                    
                                                </tr>

                                                <!-- ****** Modal Edit ****** -->
                                                    <div class="modal fade bd-example-modal-lg" id='ModalEdit<?php echo $Departamento['Departamento']['idDepartamento']; ?>' tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Departamento | <?= h($Departamento['Departamento']['descricao']) ?> </h5>
                                                                    
                                                                    <?php echo $this->Html->link(
                                                                            '<i class="ti-close"></i>', array("action" => "index"), 
                                                                            array(                                                  
                                                                                'class' => 'close', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>
                                                                    
                                                                </div>    

                                                                <?php echo $this->Form->create('Departamento', array('url' => array('controller' => 'Departamento', 'action' => 'edit', $Departamento['Departamento']['idDepartamento']), 
                                                                                'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                    'label' => false,
                                                                                                                    'class' => 'form-control'
                                                                                                                )) ); ?>                                                     

                                                                    <div class="modal-body">                                                                    
                                                                                                                                                
                                                                        <div class="basic-form">
                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-12">
                                                                                    <label>Descrição <span class="text-danger">*</span></label>
                                                                                    <input type="text" name='descricao' maxlength='100' class="form-control" placeholder="Digite o nome do departamento" value='<?= h($Departamento['Departamento']['descricao']) ?>' required>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-6">
                                                                                    <label>Desc. Abreviada</label>
                                                                                    <input type="text" name='descricaoreduzida' maxlength='50' class="form-control" placeholder="Digite o apelido do departamento" value='<?= h($Departamento['Departamento']['descricaoreduzida']) ?>'>
                                                                                </div>
                                                                            </div>  
                                                                        </div> 
                                                                                                                            
                                                                    </div> 

                                                                    <div class="modal-footer">                                                                    
                                                                                                                                                
                                                                        <button type="submit" class="btn mb-1 btn-outline-info"><i class="ti-check"> Salvar</i></button> 

                                                                        <?php echo $this->Html->link(
                                                                            '<i class="ti-close"> Cancelar</i>', array("action" => "index"), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-outline-dark', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>
                                                                                                                                        
                                                                    </div> 

                                                                <?= $this->Form->end() ?>                                                              
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                <!-- ****** Fim Modal Edit ****** -->

                                                <!-- ****** Modal View ****** -->
                                                    <div class="modal fade bd-example-modal-lg" id='ModalView<?php echo $Departamento['Departamento']['idDepartamento']; ?>' tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Departamento | <?= h($Departamento['Departamento']['descricao']) ?> </h5>
                                                                    <button type="button" class="close" data-dismiss="modal"><span><i class="ti-close"></i></span>
                                                                    </button>
                                                                </div>    

                                                                <?php echo $this->Form->create('Departamento', array('url' => array('controller' => 'Departamento', 'action' => 'edit', $Departamento['Departamento']['idDepartamento']), 
                                                                                'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                    'label' => false,
                                                                                                                    'class' => 'form-control'
                                                                                                                )) ); ?>                                                     

                                                                    <div class="modal-body">                                                                    
                                                                                                                                                
                                                                        <div class="basic-form">
                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-12">
                                                                                    <label>Descrição <span class="text-danger">*</span></label>
                                                                                    <input type="text" disabled class="form-control" placeholder="Digite o nome do departamento" value='<?= h($Departamento['Departamento']['descricao']) ?>'>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-6">
                                                                                    <label>Desc. Abreviada</label>
                                                                                    <input type="text" disabled class="form-control" placeholder="Digite o apelido do departamento" value='<?= h($Departamento['Departamento']['descricaoreduzida']) ?>'>
                                                                                </div>
                                                                            </div>  
                                                                        </div> 
                                                                                                                            
                                                                    </div> 

                                                                    <div class="modal-footer">                                                                    
                                                                                                                                                
                                                                        <button type="button" class="btn mb-1 btn-outline-dark" data-dismiss="modal"><i class="ti-close"> Fechar</i></button>                                                                    
                                                                                                                                        
                                                                    </div> 

                                                                <?= $this->Form->end() ?>                                                              
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                <!-- ****** Fim Modal View ****** -->

                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-between">
                                    
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->first('<< ' . __('Primeiro ')) ?>
                                            <?= $this->Paginator->prev('< ' . __('Anterior ')) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next(__('Próximo') . ' >') ?>
                                            <?= $this->Paginator->last(__('Último]') . ' >>') ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registro(s) de um total de {{count}} ')]) ?></p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>

</div>
