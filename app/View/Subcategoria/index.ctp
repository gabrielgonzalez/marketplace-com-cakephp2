<div class="content-body">

    <div class="container-fluid mt-3">
    
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body pb-0 d-flex justify-content-between">
                                <div>
                                    <h4 class="mb-1">Subcategorias</h4>
                                </div>
                                <div>
                                    <ul>
                                        <li class="d-inline-block mr-3">

                                            <!-- Botão para acionar registro -->
                                            <button type="button" class="btn mb-1 btn-sm btn-outline-info" data-toggle="modal" data-target="#ModalAdd" title='Clique para cadastar um novo Categoria'>
                                                <i class="ti-plus"> Nova Subcategoria</i>
                                            </button>

                                            <!-- ****** Modal Add ****** -->
                                                <div class="modal fade bd-example-modal-lg" id='ModalAdd' tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Cadastro de Subcategorias</h5>
                                                                
                                                                <?php echo $this->Html->link(
                                                                            '<i class="ti-close"></i>', array("action" => "index"), 
                                                                            array(                                                  
                                                                                'class' => 'close', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>

                                                            </div>    

                                                            <?php echo $this->Form->create('Subcategoria', array('url' => array('controller' => 'Subcategoria', 'action' => 'add'), 
                                                                            'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                'label' => false,
                                                                                                                'class' => 'form-control'
                                                                                                            )) ); ?>                                                     

                                                                <div class="modal-body">                                                                    
                                                                                                                                            
                                                                    <div class="basic-form">
                                                                        <div class="form-row">
                                                                            <div class="form-group col-md-12">
                                                                                <label>Descrição <span class="text-danger">*</span></label>
                                                                                <input type="text" name='data[Subcategoria][descricao]' maxlength='100' class="form-control" placeholder="Digite o nome do Categoria" required>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-row">
                                                                            <div class="form-group col-md-6">
                                                                                <label>Desc. Abreviada</label>
                                                                                <input type="text" name='data[Subcategoria][descricaoReduzida]' maxlength='50' class="form-control" placeholder="Digite o apelido do Categoria">
                                                                            </div>
                                                                        </div>  

                                                                        <div class="form-row">
                                                                            <div class="form-group col-md-12">
                                                                                <label>Categoria <span class="text-danger">*</span></label>
                                                                                <?php echo $this->Form->input('idCategoriaFK', ['options' => $optionsCategoria, 'label' => false, 'class' => 'form-control']); ?>
                                                                            </div>
                                                                        </div>
                                                                    </div> 
                                                                                                                        
                                                                </div> 

                                                                <div class="modal-footer">                                                                    
                                                                                                                                            
                                                                    <button type="submit" class="btn mb-1 btn-outline-info"><i class="ti-check"> Salvar</i></button> 

                                                                    <?php echo $this->Html->link(
                                                                            '<i class="ti-close"> Cancelar</i>', array("action" => "index"), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-outline-dark', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>
                                                                                                                                    
                                                                </div> 

                                                            <?= $this->Form->end() ?>                                                              
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            <!-- ****** Fim Modal Add ****** -->

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th scope="col" width='30%'> </th>
                                                <th scope="col" width='10%'><?= $this->Paginator->sort('idSubCategoria', 'Cód.') ?></th>
                                                <th scope="col" width='30%'><?= $this->Paginator->sort('descricao', 'Descrição') ?></th>
                                                <th scope="col" width='30%'><?= $this->Paginator->sort('descricaoReduzida', 'Desc. Reduzida') ?></th>                                                
                                                <th scope="col" width='30%'><?= $this->Paginator->sort('Categoria.descricao', 'Categoria') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($Subcategorias as $Subcategoria): ?>
                                                <tr>
                                                    <td class="actions">
                                                        
                                                        <!-- Botão para visualizar registro -->
                                                        <button type="button" class="btn mb-1 btn-sm btn-outline-warning" data-toggle="modal" data-target="#ModalView<?php echo $Subcategoria['Subcategoria']['idSubCategoria']; ?>" title='Clique para ver mais informações'>
                                                            <i class="ti-eye"> Ver</i>
                                                        </button>
                                                        
                                                        <!-- Botão para editar registro -->
                                                        <button type="button" class="btn mb-1 btn-sm btn-outline-success" data-toggle="modal" data-target="#ModalEdit<?php echo $Subcategoria['Subcategoria']['idSubCategoria']; ?>" title='Clique para editar'>
                                                            <i class="ti-pencil-alt"> Editar</i>
                                                        </button>

                                                        <?php echo $this->Html->link(
                                                                            '<i class="ti-trash"> Deletar</i>', array("action" => "delete", $Subcategoria['Subcategoria']['idSubCategoria']), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-sm btn-outline-danger', 
                                                                                'escape' => false, 
                                                                                'confirm' => 'Tem certeza que deseja apagar este registro?'
                                                                            )
                                                                        );?>
                                                    </td>
                                                    <td><?= $this->Number->format($Subcategoria['Subcategoria']['idSubCategoria']) ?></td>
                                                    <td><?= h($Subcategoria['Subcategoria']['descricao']) ?></td>
                                                    <td><?= h($Subcategoria['Subcategoria']['descricaoReduzida']) ?></td>                                                    
                                                    <td><?= h($Subcategoria['CategoriaFK']['descricao']) ?></td> 
                                                </tr>

                                                <!-- ****** Modal Edit ****** -->
                                                    <div class="modal fade bd-example-modal-lg" id='ModalEdit<?php echo $Subcategoria['Subcategoria']['idSubCategoria']; ?>' tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Categoria | <?= h($Subcategoria['Subcategoria']['descricao']) ?> </h5>
                                                                    
                                                                    <?php echo $this->Html->link(
                                                                            '<i class="ti-close"></i>', array("action" => "index"), 
                                                                            array(                                                  
                                                                                'class' => 'close', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>
                                                                </div>    

                                                                <?php echo $this->Form->create('Subcategoria', array('url' => array('controller' => 'Subcategoria', 'action' => 'edit', $Subcategoria['Subcategoria']['idSubCategoria']), 
                                                                                'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                    'label' => false,
                                                                                                                    'class' => 'form-control'
                                                                                                                )) ); ?>                                                     

                                                                    <div class="modal-body">                                                                    
                                                                                                                                                
                                                                        <div class="basic-form">
                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-12">
                                                                                    <label>Descrição <span class="text-danger">*</span></label>
                                                                                    <input type="text" name='data[Subcategoria][descricao]' maxlength='100' class="form-control" placeholder="Digite o nome do Categoria" value='<?= h($Subcategoria['Subcategoria']['descricao']) ?>' required>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-6">
                                                                                    <label>Desc. Abreviada</label>
                                                                                    <input type="text" name='data[Subcategoria][descricaoReduzida]' maxlength='50' class="form-control" placeholder="Digite o apelido do Categoria" value='<?= h($Subcategoria['Subcategoria']['descricaoReduzida']) ?>'>
                                                                                </div>
                                                                            </div>  

                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-12">
                                                                                    <label>Categoria <span class="text-danger">*</span></label>
                                                                                    <?php echo $this->Form->input('idCategoriaFK', ['options' => $optionsCategoria, 'label' => false, 'value' => $Subcategoria['Subcategoria']['idCategoriaFK'], 'class' => 'form-control']); ?>
                                                                                </div>
                                                                            </div>

                                                                        </div> 
                                                                                                                            
                                                                    </div> 

                                                                    <div class="modal-footer">                                                                    
                                                                                                                                                
                                                                        <button type="submit" class="btn mb-1 btn-outline-info"><i class="ti-check"> Salvar</i></button> 

                                                                        <?php echo $this->Html->link(
                                                                            '<i class="ti-close"> Cancelar</i>', array("action" => "index"), 
                                                                            array(                                                  
                                                                                'class' => 'btn mb-1 btn-outline-dark', 
                                                                                'escape' => false
                                                                            )
                                                                        );?>
                                                                                                                                        
                                                                    </div> 

                                                                <?= $this->Form->end() ?>                                                              
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                <!-- ****** Fim Modal Edit ****** -->

                                                <!-- ****** Modal View ****** -->
                                                    <div class="modal fade bd-example-modal-lg" id='ModalView<?php echo $Subcategoria['Subcategoria']['idSubCategoria']; ?>' tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">Categoria | <?= h($Subcategoria['Subcategoria']['descricao']) ?> </h5>
                                                                    <button type="button" class="close" data-dismiss="modal"><span><i class="ti-close"></i></span>
                                                                    </button>
                                                                </div>    

                                                                <?php echo $this->Form->create('Subcategoria', array('url' => array('controller' => 'Subcategoria', 'action' => 'edit', $Subcategoria['Subcategoria']['idSubCategoria']), 
                                                                                'class' => 'form-horizontal'), array('inputDefaults' => array(
                                                                                                                    'label' => false,
                                                                                                                    'class' => 'form-control'
                                                                                                                )) ); ?>                                                     

                                                                    <div class="modal-body">                                                                    
                                                                                                                                                
                                                                        <div class="basic-form">
                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-12">
                                                                                    <label>Descrição <span class="text-danger">*</span></label>
                                                                                    <input type="text" disabled class="form-control" value='<?= h($Subcategoria['Subcategoria']['descricao']) ?>'>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-6">
                                                                                    <label>Desc. Abreviada</label>
                                                                                    <input type="text" disabled class="form-control" value='<?= h($Subcategoria['Subcategoria']['descricaoReduzida']) ?>'>
                                                                                </div>
                                                                            </div>  

                                                                            <div class="form-row">
                                                                                <div class="form-group col-md-12">
                                                                                    <label>Categoria <span class="text-danger">*</span></label>
                                                                                    <input type="text" disabled class="form-control" value='<?= h($Subcategoria['CategoriaFK']['descricao']) ?>'>
                                                                                </div>
                                                                            </div>

                                                                        </div> 
                                                                                                                            
                                                                    </div> 

                                                                    <div class="modal-footer">                                                                    
                                                                                                                                                
                                                                        <button type="button" class="btn mb-1 btn-outline-dark" data-dismiss="modal"><i class="ti-close"> Fechar</i></button>                                                                    
                                                                                                                                        
                                                                    </div> 

                                                                <?= $this->Form->end() ?>                                                              
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                <!-- ****** Fim Modal View ****** -->

                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-between">
                                    
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->first('<< ' . __('Primeiro ')) ?>
                                            <?= $this->Paginator->prev('< ' . __('Anterior ')) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next(__('Próximo') . ' >') ?>
                                            <?= $this->Paginator->last(__('Último]') . ' >>') ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registro(s) de um total de {{count}} ')]) ?></p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>

</div>
