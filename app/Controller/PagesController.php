<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Subcategoria', 'Departamento', 'Categoria', 'Anuncios', 'FotosProduto', 'Avaliacoes', 'ListaDesejos', 'Carrinho');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('home', 'view', 'subcategoria', 'pesquisa', 'fale_conosco', 'about'); //PERMITINDO QUE OS USUÁRIOS ACESSEM A TELA INICIAL DO SITE
	}

	var $components = array('Email');
/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function home() {
		
		$menu = $this->Subcategoria->menu();
		
		$i = 0 ;
		foreach ($menu as $key => $menu) {
			
			$departamentos[$menu['dep']['idDepartamento']] ['Departamentos'] ['idDepartamento'] = $menu['dep']['idDepartamento'];
			$departamentos[$menu['dep']['idDepartamento']] ['Departamentos'] ['descricao'] = $menu['dep']['descricao'];

			$departamentos[$menu['dep']['idDepartamento']] ['Departamentos'] ['Categoria'] [$menu['cat']['idCategoria']] ['idCategoria'] = $menu['cat']['idCategoria'];
			$departamentos[$menu['dep']['idDepartamento']] ['Departamentos'] ['Categoria'] [$menu['cat']['idCategoria']] ['descricao'] = $menu['cat']['descricao'];

			$departamentos[$menu['dep']['idDepartamento']] ['Departamentos'] ['Categoria'] [$menu['cat']['idCategoria']] ['Subcategoria'] [$menu['subcat']['idSubCategoria']] ['idSubCategoria'] = $menu['subcat']['idSubCategoria'];
			$departamentos[$menu['dep']['idDepartamento']] ['Departamentos'] ['Categoria'] [$menu['cat']['idCategoria']] ['Subcategoria'] [$menu['subcat']['idSubCategoria']] ['descricao'] = $menu['subcat']['descricao'];
			$departamentos[$menu['dep']['idDepartamento']] ['Departamentos'] ['Categoria'] [$menu['cat']['idCategoria']] ['Subcategoria'] [$menu['subcat']['idSubCategoria']] ['descricaoReduzida'] = $menu['subcat']['descricaoReduzida'];
			
			$i++;

		}
		
		$this->Session->write('Departamentos', $departamentos); 
		
		if (empty($this->Session->read('Carrinho'))) {

			$carrionho = array(); 
			$carrionho['Total'] = 0;
			$carrionho['Produtos'] = array();
			
			$this->Session->write('Carrinho', $carrionho);

		}

		$randon = $this->Subcategoria->Randon();
		$this->set('randon', $randon);

		$produtos = $this->Anuncios->find('all', array('conditions' => array('situacao' => 'A', 'idSubcategoriaFK' => $randon['0']['Sub']['idSubCategoria']) ));
		$this->set('produtos', $produtos);

		$AddRecent = $this->Anuncios->find('all', array('conditions' => array('situacao' => 'A'), 'limit' => 5, 'order' => array('Anuncios.dtCriacao DESC') ));
		$this->set('AddRecent', $AddRecent);
		
		
	}


	public function view($id) {
		
		$produto = $this->Anuncios->find('first', array('conditions' => array('idProdutos' => $id) ));
		$this->set('produto', $produto);
		
		$FotosProduto = $this->FotosProduto->find('all', array('conditions' => array('idProdutoFK' => $id, 'situacao' => 'A'), 'order' => array('ordem')));
		$this->set('FotosProduto', $FotosProduto);
		
		$Avaliacoes = $this->Avaliacoes->find('all', array('conditions' => array('idProdutoFK' => $id, 'situacao' => 'A'), 'order' => array('dtCriacao Desc')));
		$this->set('Avaliacoes', $Avaliacoes); 

		$AddRecent = $this->Anuncios->find('all', array('conditions' => array('situacao' => 'A'), 'limit' => 10, 'order' => array('Anuncios.dtCriacao DESC') ));
		$this->set('AddRecent', $AddRecent); 

		$Relacionados = $this->Anuncios->find('all', array('conditions' => array("idProdutos <> $id", 'idSubcategoriaFK' => $produto['Anuncios']['idSubcategoriaFK'], 'situacao' => 'A' ) ));
		$this->set('Relacionados', $Relacionados);


		if (!empty($this->Session->read('User.id'))) {

			$listado = $this->ListaDesejos->find('first', array('conditions' => array("ListaDesejos.idProdutoFK" => $id, 'ListaDesejos.idUsuarioFK' => $this->Session->read('User.id') ) ));
			$this->set('listado', $listado);
			
		}
		
	}


	public function subcategoria($id) {

		$subCategoria = $this->Subcategoria->find('first', array('conditions' => array('idSubcategoria' => $id) ));
		$this->set('subCategoria', $subCategoria);
		
		$produtos = $this->Anuncios->find('all', array('conditions' => array('situacao' => 'A', 'idSubcategoriaFK' => $id) ));
		$this->set('produtos', $produtos);

		$AddRecent = $this->Anuncios->find('all', array('conditions' => array('situacao' => 'A'), 'limit' => 10, 'order' => array('Anuncios.dtCriacao DESC') ));
		$this->set('AddRecent', $AddRecent);
		
		
	}


	public function pesquisa() {
		
		$pesq = $this->request->data['Pesquisa']['pesquisa'];

		$produtos = $this->Anuncios->find('all', array('conditions' => array('situacao' => 'A', "nomeProduto like '%$pesq%'") ));
		$this->set('produtos', $produtos);
		
		$AddRecent = $this->Anuncios->find('all', array('conditions' => array('situacao' => 'A'), 'limit' => 10, 'order' => array('Anuncios.dtCriacao DESC') ));
		$this->set('AddRecent', $AddRecent);
		
		
	} 


	public function add_cart() {

		$this->autorender = false;
		
		if ($this->request->is('post')) {

			$verifica = $this->Carrinho->find('first', array('conditions' => array('Carrinho.situacao' => '0', 'Carrinho.idUsuarioFK' => $this->Session->read('User.id'), 'Carrinho.idProdutoFK' => $this->request->data['Carrinho']['idProdutoFK'] ) ));			

			if (empty($verifica)) {

				$this->request->data['Carrinho']['idUsuarioFK'] = $this->Session->read('User.id');				
				$this->request->data['Carrinho']['dtCriacao'] = date('Y-m-d H:i:s');
				$this->request->data['Carrinho']['situacao'] = '0';				
				
				$this->Carrinho->create();
				$this->Carrinho->save($this->request->data['Carrinho']);

			} else {

				$this->request->data['Carrinho']['idCarrinho'] = $verifica['Carrinho']['idCarrinho'];				
				$this->request->data['Carrinho']['quantidade'] = $verifica['Carrinho']['quantidade'] + $this->request->data['Carrinho']['quantidade'];	
				$this->request->data['Carrinho']['idUsuarioFK'] = $this->Session->read('User.id');				
				$this->request->data['Carrinho']['situacao'] = '0';
				$this->request->data['Carrinho']['dtCriacao'] = date('Y-m-d H:i:s');				
				
				$this->Carrinho->save($this->request->data['Carrinho']);

			}
			

		} 		

		return $this->redirect(array('action' => 'novo_cart'));
		
	}


	public function novo_cart() {
		
		$this->layout = 'cart';

		$produtos = $this->Carrinho->find('all', array('conditions' => array('Carrinho.situacao' => '0', 'Carrinho.idUsuarioFK' => $this->Session->read('User.id') ) ));		
		
		if (!empty($produtos)) {

			foreach ($produtos as $key => $produto) {
			
				$FotosProduto = $this->FotosProduto->find('first', array('conditions' => array('situacao' => 'A', 'idProdutoFK' => $produto['Carrinho']['idProdutoFK']) ));			
	
				$produtos[$key]['Img'] = $FotosProduto['Img'];
	
			}
			
			$this->set('produtos', $produtos);	
			
			$this->Session->delete('Carrinho');
	
			$carrionho = array(); 
			$carrionho['Total'] = count($produtos);
			$carrionho['Produtos'] = $produtos;
			
			$this->Session->write('Carrinho', $carrionho);

		} else {

			$this->Session->delete('Carrinho');
	
			$carrionho = array(); 
			$carrionho['Total'] = 0;
			$carrionho['Produtos'] = array();
			
			$this->Session->write('Carrinho', $carrionho);

		}
				
	}


	public function remove_cart($id) {

		$this->autorender = false;
		
		$this->Carrinho->delete($id);

		return $this->redirect(array('action' => 'novo_cart'));
		
	}


	public function verificar_Item_mais($id) {

		$this->autorender = false;

		$verifica = $this->Carrinho->find('first', array('conditions' => array('Carrinho.idCarrinho' => $id ) ));			
		
		$this->request->data['Carrinho']['quantidade'] = $verifica['Carrinho']['quantidade'] + 1;	
		
		if ($verifica['ProdutoFK']['quantidade'] >= $this->request->data['Carrinho']['quantidade']) {

			$this->request->data['Carrinho']['idCarrinho'] = $verifica['Carrinho']['idCarrinho'];							
			$this->request->data['Carrinho']['idUsuarioFK'] = $this->Session->read('User.id');				
			$this->request->data['Carrinho']['dtCriacao'] = date('Y-m-d H:i:s');				
			
			$this->Carrinho->save($this->request->data['Carrinho']);

		} else {

			$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');

		}		
		
		return $this->redirect(array('action' => 'novo_cart'));

	}


	public function verificar_Item_menos($id) {

		$this->autorender = false;

		$verifica = $this->Carrinho->find('first', array('conditions' => array('Carrinho.idCarrinho' => $id ) ));			
		
		$this->request->data['Carrinho']['idCarrinho'] = $verifica['Carrinho']['idCarrinho'];				
		$this->request->data['Carrinho']['quantidade'] = $verifica['Carrinho']['quantidade'] - 1;	
		$this->request->data['Carrinho']['idUsuarioFK'] = $this->Session->read('User.id');				
		$this->request->data['Carrinho']['dtCriacao'] = date('Y-m-d H:i:s');				
		
		$this->Carrinho->save($this->request->data['Carrinho']);
		
		return $this->redirect(array('action' => 'novo_cart'));

	}


	public function fale_conosco() {
		
		if ($this->request->is('post')) {
			
			//App::uses('CakeEmail', 'Network/Email');
			$Email = new CakeEmail('smtp');
			$Email->to('gabrielgonzalezbs@gmail.com');
			$Email->subject('S-commerce - Fale conosco');

			$mensagem = 'Nome completo: '. $this->request->data['FaleConosco']['nome'] . '\n';
			$mensagem = 'Email: '. $this->request->data['FaleConosco']['email'] . '\n';
			$mensagem = 'Menssagem: \n'. $this->request->data['FaleConosco']['Menssagem'] . '\n';
			
			/*if ($Email->send($mensagem)){

				$this->Session->setFlash('<script> swal("Email enviado com sucesso."); </script>', 'default');

			} else {

				$this->Session->setFlash('<script> swal("Desculpe!", "Não foi possível enviar seu email. Por favor, tente novamente mais tarde!"); </script>', 'default');

			}*/
			
			$this->Session->setFlash('<script> swal("Desculpe!", "Não foi possível enviar seu email. Por favor, tente novamente mais tarde!"); </script>', 'default');
			return $this->redirect(array('action' => 'home'));

		}
		
	}


	public function about() {
	
	}

}
