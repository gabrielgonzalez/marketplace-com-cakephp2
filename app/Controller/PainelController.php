<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PainelController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Painel', 'ItensPedidos', 'Usuario', 'Anuncios');

/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function index() {
		
		$this->set('title', 'Painels');
		
        $this->layout = 'lablayout';
		
		//CALULO O TOTAL DE ITENS VENDIDOS
		$totalItensVendidos = $this->Painel->totalItensVendidos($this->Session->read('User.id'));
		$this->set('totalItensVendidos', $totalItensVendidos[0][0]);

		///PEGO A QUANTIDADE DE NOVOS PEDIDOS
		$NovosItens = $this->ItensPedidos->find('count', array('conditions' => array('ProdutoFK.idUsuarioFK'=> $this->Session->read('User.id'), 'ItensPedidos.situacao' => 0)));
		$this->set('NovosItens', $NovosItens);

		///pego a quantidade de itens entregues
		$ItensEntregues = $this->ItensPedidos->find('count', array('conditions' => array('ProdutoFK.idUsuarioFK'=> $this->Session->read('User.id'), 'ItensPedidos.situacao' => 4)));
		$this->set('ItensEntregues', $ItensEntregues);


		///PEGO AS INFORMAÇÕES DO USUÁRIO LOGADO
		$Usuarios = $this->Usuario->findByIdusuario($this->Session->read('User.id'));		
		$this->set('Usuarios', $Usuarios);
		
		///calculo uma data anterior de 3 meses 
		$timestamp = strtotime(date('Y-m-d') . "-3 months");
		$mesAtual = date('m');
		$mesAntigo = date('m', $timestamp);
		$ano = date('Y', $timestamp);
		
		///MONTO A FUNÇÃO JAVASCRIPT QUE CARREGA AS INFORMAÇÕES PARA MONTAR O GRAFICO DE RESUMO DOS PEDIDOS
		$resumoPedidos = "Morris.Bar({ element: 'bar-pedidos', data: [";
		$i=0;
		while ($mesAntigo < $mesAtual) {
			
			$mesExtenso = $this->Utilitarios->mesExtenso($mesAntigo);
			
			$PedidosMensal = $this->Painel->resumoPedidos($mesAntigo, $ano, $this->Session->read('User.id'));
			
			if(empty($PedidosMensal)){
				$totalPedidos = 0;
			} else {				
				$totalPedidos = $PedidosMensal['0']['0']['TotalPedidos'];
			}

			$resumoPedidos .= "{ y: '$mesExtenso', a: $totalPedidos},";
			
			$mesAntigo++;
			
			if ($mesAntigo >= 13) {
				$mesAntigo = 1;
				$ano++;
			}
		}
		$resumoPedidos .= "], xkey: 'y', ykeys: ['a'], labels: ['Pedidos'] });";		
		$this->set('resumoPedidos', $resumoPedidos);
		

		///Pego o total de itens cadastrados
		$ItensCadastrados = $this->Anuncios->find('count', array('conditions' => array('idUsuarioFK'=> $this->Session->read('User.id'))));
		$this->set('ItensCadastrados', $ItensCadastrados);
		

		$ItemMaisVendido = $this->Painel->ItemMaisVendido($this->Session->read('User.id'));
		$this->set('ItemMaisVendido', $ItemMaisVendido);
		
	}

	public function add() {
		
		if ($this->request->is('post')) {

			$this->Painel->create();
			if ($this->Painel->save($this->request->data)) {  //($this->Contrato->save($this->request->data)) {
				$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
			} else {

					$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
			} 


			return $this->redirect(array('action' => 'index'));

		}
		
	}


	public function edit($id) {
		
		if ($this->request->is('post')) {
			
			$this->request->data['idPainel'] = $id;
			
			if ($this->Painel->save($this->request->data)) {  //($this->Contrato->save($this->request->data)) {
				$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
			} else {

					$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
			} 


			return $this->redirect(array('action' => 'index'));

		}
		
	}


	public function delete($id) {
		
		$this->Painel->delete($id);

		return $this->redirect(array('action' => 'index'));
		
	}

}
