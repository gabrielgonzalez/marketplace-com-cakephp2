<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public $uses = array('Users');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('add', 'logout', 'login');
    }

    public function login() {

        $this->layout = 'login';

        if ($this->request->is('post')) {

            if ($this->Auth->login()) {
             
                $this->Session->write('User.id', $this->Auth->user('idUsuario'));
    
                $this->Session->write('User.username', $this->Auth->user('username'));
    
                $this->redirect($this->Auth->redirect());
    
            } else {
                $this->Session->setFlash('<script> swal("Atenção!", "Usuário ou senha invalidos, tente novamente!"); </script>', 'default');
            }

        }
        
    }

    public function logout() {
        $this->Session->destroy();
        $this->redirect($this->Auth->logout());
    }

    public function index() {
        $this->Users->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function view($id = null) {
        if (!$this->Users->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->Users->findById($id));
    }

    public function add() {

        $this->layout = 'login';
        
        if ($this->request->is('post')) {
            
            $this->Users->create();
            if ($this->Users->save($this->request->data['Users'])) {
                $this->Flash->success(__('The user has been saved'));
                
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }

        return $this->redirect(array('controller' => 'Pages', 'action' => 'home'));
    }

    public function edit($id = null) {
        $this->Users->id = $id;
        if (!$this->Users->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Users->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Users->findById($id);
            unset($this->request->data['Users']['password']);
        }
    }

    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Users->id = $id;
        if (!$this->Users->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->Users->delete()) {
            $this->Flash->success(__('Users deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('Users was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}