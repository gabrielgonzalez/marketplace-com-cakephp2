<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class EnderecoController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Endereco', 'Cidades');

/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function add() {
		
		if ($this->request->is('post')) {
			
			$Cidade = $this->Cidades->findByCodigoibge($this->request->data['Endereco']['ibge']);	

			$this->request->data['Endereco']['idCidadeFK'] = $Cidade['Cidades']['idCidade']; 
			$this->request->data['Endereco']['idEstadoFK'] = $Cidade['Cidades']['idEstadoFK'];
			$this->request->data['Endereco']['idUsuarioFK'] = $this->Session->read('User.id');

			$this->Endereco->create();
			if ($this->Endereco->save($this->request->data['Endereco'])) {  //($this->Contrato->save($this->request->data)) {
				$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
			} else {

					$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
			} 


			return $this->redirect(array('controller' => 'Usuario', 'action' => 'index'));

		}
		
	}


	public function edit($id) {
		
		if ($this->request->is('post')) {
			 
			$this->request->data['Endereco']['idEnderecos'] = $id;

			$Cidade = $this->Cidades->findByCodigoibge($this->request->data['Endereco']['ibge']);	

			$this->request->data['Endereco']['idCidadeFK'] = $Cidade['Cidades']['idCidade']; 
			$this->request->data['Endereco']['idEstadoFK'] = $Cidade['Cidades']['idEstadoFK'];
			
			if ($this->Endereco->save($this->request->data)) {  //($this->Contrato->save($this->request->data)) {
				$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
			} else {

					$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
			} 


			return $this->redirect(array('controller' => 'Usuario', 'action' => 'index'));

		}
		
	}


	public function delete($id) {
		
		//$this->Endereco->delete($id);

		$this->request->data['Endereco']['idEnderecos'] = $id;

		$this->request->data['Endereco']['situacao'] = 'I'; 
		
		if ($this->Endereco->save($this->request->data['Endereco'])) {  //($this->Contrato->save($this->request->data)) {
			$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
		} else {

				$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
		} 


		return $this->redirect(array('controller' => 'Usuario', 'action' => 'index'));

		return $this->redirect(array('controller' => 'Usuario', 'action' => 'index'));
		
	}

}
