<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AnunciosController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $components = array('Paginator');
		var $helpers = array('Time'); 
		
	public $uses = array('Anuncios', 'Departamento', 'Categoria', 'Subcategoria', 'FotosProduto' );

/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function index() {
		
		$this->set('title', 'Anuncios');
		
		$this->layout = 'lablayout';
		
		$this->Paginator->settings = array(
			'conditions' => array('situacao' => 'A', 'idUsuarioFK' => $this->Session->read('User.id'))
		); 
		
        $Anuncios = $this->paginate($this->Anuncios);		
		$this->set('Anuncios', $Anuncios);
				
		
	}

	public function add() {
		
		$this->set('title', 'Anuncios');
		
        $this->layout = 'lablayout';

		if ($this->request->is('post')) {

			$preco = str_replace(".","", $this->request->data['Produto']['preco']);
			$preco = str_replace(",",".", $preco);

			$this->request->data['Produto']['preco'] = $preco;
			$this->request->data['Produto']['idUsuarioFK'] = $this->Session->read('User.id');
			$this->request->data['Produto']['situacao'] = 'A';
			$this->request->data['Produto']['dtCriacao'] = date('Y-m-d H:i:s');
			
			$this->Anuncios->create();
			$this->Anuncios->save($this->request->data['Produto']);
			$id = $this->Anuncios->id;

			$i = 0; 
			foreach ($this->request->data['caminho'] as $key => $caminho) {
				
				$caminho['idProdutoFK'] = $id; //$id
				$caminho['ordem'] = $i;
				$caminho['situacao'] = 'A';
				
				$this->FotosProduto->create();
				$this->FotosProduto->save($caminho);
				
				$i++;
			}

			return $this->redirect(array('action' => 'index'));

		} else {

			$optionsSubcategoria = $this->Subcategoria->find('list', array('order' => 'descricao'));
			$this->set('optionsSubcategoria', $optionsSubcategoria);

			$optionsDepartamento = $this->Departamento->find('list', array('order' => 'descricao'));
			$this->set('optionsDepartamento', $optionsDepartamento);

			$optionsCategoria = $this->Categoria->find('list', array('order' => 'descricao'));
			$this->set('optionsCategoria', $optionsCategoria);

			

		}
		
	}


	public function edit($id) {

		$this->set('title', 'Anuncios');
		
        $this->layout = 'lablayout';
		
		if ($this->request->is('post')) {
			
			$this->request->data['Produto']['idProdutos'] = $id;
			
			$preco = str_replace(".","", $this->request->data['Produto']['preco']);
			$preco = str_replace(",",".", $preco);

			$this->request->data['Produto']['preco'] = $preco;
			$this->request->data['Produto']['situacao'] = 'A';
			$this->request->data['Produto']['dtAlteracao'] = date('Y-m-d H:i:s');
			
			$this->Anuncios->create();
			$this->Anuncios->save($this->request->data['Produto']);
			$id = $this->Anuncios->id;
			
			if (isset($this->request->data['caminho'])) {
				
				$i = 0; 
				foreach ($this->request->data['caminho'] as $key => $caminho) {
					
					$caminho['idProdutoFK'] = $id; //$id
					$caminho['ordem'] = $i;
					$caminho['situacao'] = 'A';
					
					$this->FotosProduto->create();
					$this->FotosProduto->save($caminho);
					
					$i++;
				}

			}
			
			
			if ($this->Anuncios->save($this->request->data['Anuncios'])) {  //($this->Contrato->save($this->request->data)) {
				$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
			} else {

					$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
			} 


			return $this->redirect(array('action' => 'index'));

		} else {

			$produto = $this->Anuncios->find('first', array('conditions' => array('idProdutos' => $id) ));
			$this->set('produto', $produto);
			
			$Subcategoria = $this->Subcategoria->find('first', array('conditions' => array('idSubCategoria' => $produto['Anuncios']['idSubcategoriaFK'])));
			$this->set('Subcategoria', $Subcategoria); 
			
			$Categoria = $this->Categoria->find('first', array('conditions' => array('idCategoria' => $Subcategoria['Subcategoria']['idCategoriaFK'])));
			$this->set('Categoria', $Categoria);
			
			$optionsDepartamento = $this->Departamento->find('list', array('order' => 'descricao'));
			$this->set('optionsDepartamento', $optionsDepartamento);
			
			$FotosProduto = $this->FotosProduto->find('all', array('conditions' => array('idProdutoFK' => $id, 'situacao' => 'A'), 'order' => array('ordem')));
			$this->set('FotosProduto', $FotosProduto);
			
			$this->set('id', $id);

		}
		
	}


	public function view($id) {

		$this->set('title', 'Anuncios');
		
        $this->layout = 'lablayout';
		
		$produto = $this->Anuncios->find('first', array('conditions' => array('idProdutos' => $id) ));
		$this->set('produto', $produto);
		
		$Subcategoria = $this->Subcategoria->find('first', array('conditions' => array('idSubCategoria' => $produto['Anuncios']['idSubcategoriaFK'])));
		$this->set('Subcategoria', $Subcategoria); 
		
		$Categoria = $this->Categoria->find('first', array('conditions' => array('idCategoria' => $Subcategoria['Subcategoria']['idCategoriaFK'])));
		$this->set('Categoria', $Categoria);
		
		$FotosProduto = $this->FotosProduto->find('all', array('conditions' => array('idProdutoFK' => $id, 'situacao' => 'A'), 'order' => array('ordem')));
		$this->set('FotosProduto', $FotosProduto);
		
		$this->set('id', $id);
		
	}


	public function delete($id) {
		
		$this->request->data['Anuncios']['idProdutos'] = $id;
		$this->request->data['Anuncios']['situacao'] = 'I';
		$this->request->data['Anuncios']['dtAlteracao'] = date('Y-m-d H:i:s');
			
		if ($this->Anuncios->save($this->request->data['Anuncios'])) {  //($this->Contrato->save($this->request->data)) {
			$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
		} else {

				$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
		} 


		return $this->redirect(array('action' => 'index'));
		
	}


	function lista_categoria($id = null){
		$this->layout = null;
		$optionsCategoria= $this->Categoria->find('all', array('fields' => array('idCategoria', 'descricao'), 'conditions' => array('idDepartamentoFK' => $id)));                                
		$this->set('optionsCategoria', $optionsCategoria);   
	}


	function lista_subcategoria($id = null){
		$this->layout = null;
		$optionsSubcategoria= $this->Subcategoria->find('all', array('fields' => array('idSubCategoria', 'descricao'), 'conditions' => array('idCategoriaFK' => $id)));                                
		$this->set('optionsSubcategoria', $optionsSubcategoria);   
	}


	public function add_imagem($i = 0) {
        $this->layout = null;

		$this->set('i', $i);
		
	}
	

	public function del_imagem($id) {

		$this->autoRender = false;
		
		$this->request->data['FotosProduto']['idFoto'] = $id;
		$this->request->data['FotosProduto']['situacao'] = 'I';
		
		if ($this->FotosProduto->save($this->request->data['FotosProduto'])) {

            return true;
            
        } else {
            
            return false;

        }

	}

}
