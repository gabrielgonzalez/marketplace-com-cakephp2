<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class VendasController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Vendas', 'Pedidos', 'Categoria', 'Departamento', 'Carrinho', 'FotosProduto', 'Endereco', 'Cidades', 'Anuncios', 'ItensPedidos');

/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function index() {
		
		$this->set('title', 'Meus Pedidos');
		
        $this->layout = 'lablayout';
		
		$itensPendentes = $this->Vendas->itensPedidos(0, $this->Session->read('User.id'));
		$this->set('itensPendentes', $itensPendentes);
		

		$itensFaturar = $this->Vendas->itensPedidos(1, $this->Session->read('User.id'));
		$this->set('itensFaturar', $itensFaturar); 


		$itensEnviar = $this->Vendas->itensPedidos(2, $this->Session->read('User.id'));
		$this->set('itensEnviar', $itensEnviar);


		$itensEnviados = $this->Vendas->itensPedidos(3, $this->Session->read('User.id'));
		$this->set('itensEnviados', $itensEnviados);


		$itensEntregues = $this->Vendas->itensPedidos(4, $this->Session->read('User.id'));
		$this->set('itensEntregues', $itensEntregues);

	}

	public function add() {

		$this->layout = 'checkout';
		
		//SELECIONO OS 
		$produtos = $this->Carrinho->find('all', array('conditions' => array('Carrinho.idUsuarioFK' => $this->Session->read('User.id') ) ));		
		
		if (!empty($produtos)) {

			foreach ($produtos as $key => $produto) {
			
				$FotosProduto = $this->FotosProduto->find('first', array('conditions' => array('idProdutoFK' => $produto['Carrinho']['idProdutoFK']) ));			
	
				$produtos[$key]['Img'] = $FotosProduto['Img'];
	
			}
			
			$this->set('produtos', $produtos);	

		} 

		$verificaEndereco = $this->Pedidos->verificaEndereco($this->Session->read('User.id'));
		
		if (empty($verificaEndereco)) {

			$Endereco = $this->Endereco->find('first', array('conditions' => array('Endereco.idUsuarioFK' => $this->Session->read('User.id') ) ));		

		} else {

			$Endereco = $this->Endereco->find('first', array('conditions' => array('Endereco.idEnderecos' => $verificaEndereco['0']['carrinho']['idenderecofk'] ) ));		

		}
		 
		$this->set('Endereco', $Endereco);	

		$optionsEndereco = $this->Endereco->find('list', array('conditions' => array('Endereco.idUsuarioFK' => $this->Session->read('User.id') ) ));		
		$this->set('optionsEndereco', $optionsEndereco);	
		
		
	}


	public function edit($id) {
		
		if ($this->request->is('post')) {
			
			$this->request->data['Pedidos']['idPedidos'] = $id;
			
			if ($this->Pedidos->save($this->request->data['Pedidos'])) {  //($this->Contrato->save($this->request->data)) {
				$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
			} else {

					$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
			} 


			return $this->redirect(array('action' => 'add'));

		}
		
	}


	public function delete($id) {
		
		$this->Pedidos->delete($id);

		return $this->redirect(array('action' => 'index'));
		
	}


	public function add_endereco($idPedido = null) {

		$this->layout = 'checkout';
		
		if ($this->request->is('post')) {
			
			$Cidade = $this->Cidades->findByCodigoibge($this->request->data['Endereco']['ibge']);	

			$this->request->data['Endereco']['idCidadeFK'] = $Cidade['Cidades']['idCidade']; 
			$this->request->data['Endereco']['idEstadoFK'] = $Cidade['Cidades']['idEstadoFK'];
			$this->request->data['Endereco']['idUsuarioFK'] = $this->Session->read('User.id');
			
			$this->Endereco->create();
			if ($this->Endereco->save($this->request->data['Endereco'])) {  //($this->Contrato->save($this->request->data)) {

				$idEndereco = $this->Endereco->id;

				$this->Pedidos->atualizaEndereco($idEndereco, $this->Session->read('User.id'));

				$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
			} else {

					$this->Session->setFlash('Não foi possível salvar o registro. Por favor, tente novamente!', 'default', array('class' => 'alert alert-warning'));
			} 


			return $this->redirect(array('controller' => 'Pedidos', 'action' => 'add'));

		}
		
	}


	public function atualizaEndereco($idPedido = null) {

		$this->autorender = false;
		
		if ($this->request->is('post')) {
			
			$this->Pedidos->atualizaEndereco($this->request->data['atualizaEndereco']['endereco'], $this->Session->read('User.id'));

			return $this->redirect(array('controller' => 'Pedidos', 'action' => 'add'));

		}
		
	}


	public function confirmar_pedido($FormaPagto) {

		$this->autorender = false;

		if (isset($FormaPagto)) {

			$produtos = $this->Carrinho->find('all', array('conditions' => array('Carrinho.idUsuarioFK' => $this->Session->read('User.id') ) ));
			
			if (!empty($produtos)) {

				///Gravando o pedido
				$pedido = array();
				$itensPedido = array();

				$pedido['idUsuarioFK'] = $this->Session->read('User.id');
				$pedido['formaPGTO'] = $FormaPagto;
				$pedido['situacao'] = '0';
				$pedido['dtPedido'] = date('Y-m-d H:i:s');

				$this->Pedidos->save($pedido);
				$idPedido = $this->Pedidos->id;

				unset($pedido);

				$i = 0;
				$totalPedido = 0; 
				$idEndereco = '';
				foreach ($produtos as $key => $ItemPedido) {

					$produto = $this->Anuncios->findByIdprodutos($ItemPedido['Carrinho']['idProdutoFK']);
					
					$total = $produto['Anuncios']['preco'] * $ItemPedido['Carrinho']['quantidade'];
					
					$totalPedido += $total;

					$itensPedido[$i]['idPedidoFK'] = $idPedido;
					$itensPedido[$i]['idProdutoFK'] = $ItemPedido['Carrinho']['idProdutoFK'];
					$itensPedido[$i]['quantidade'] = $ItemPedido['Carrinho']['quantidade'];
					$itensPedido[$i]['precoUnitario'] = $produto['Anuncios']['preco'];
					$itensPedido[$i]['precoTotal'] = $total;
					$itensPedido[$i]['situacao'] = 'A';

					$this->ItensPedidos->save($itensPedido[$i]);

					$idEndereco = $ItemPedido['Carrinho']['idEnderecoFK'];

					$qtdAtualiza = $produto['Anuncios']['quantidade'] - $ItemPedido['Carrinho']['quantidade']; 					

					$atualizaProduto = array();

					$atualizaProduto['idProdutos'] = $produto['Anuncios']['idProdutos']; 
					$atualizaProduto['quantidade'] = $qtdAtualiza; 

					$this->Anuncios->save($atualizaProduto);

					$i++;

				}


				$pedido['idPedido'] = $idPedido;
				$pedido['idEnderecoFK'] = $idEndereco;
				$pedido['precoTotal'] = $totalPedido;

				$this->Pedidos->save($pedido);

				return $this->redirect(array('controller' => 'Pedidos', 'action' => 'index'));

			}
			

		}

	}


	public function view($idPedido) {

		$this->layout = 'lablayout';

		$pedido = $this->Pedidos->findByIdpedido($idPedido);
		$this->set('pedido', $pedido);
		
		$produtos = $this->ItensPedidos->find('all', array('conditions' => array('idPedidoFK' => $idPedido)));
		$this->set('produtos', $produtos);			

	}


	public function sit_pendente($idPedidoProduto = null, $sit = null) {

		$this->autorender = false;

		$PedidoProduto['idPedidoProduto'] = $idPedidoProduto;
		$PedidoProduto['situacao'] = $sit;

		$this->ItensPedidos->save($PedidoProduto);

		return $this->redirect(array('action' => 'index'));
		
	}

}
