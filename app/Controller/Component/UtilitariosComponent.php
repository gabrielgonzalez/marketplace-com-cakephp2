<?php

class UtilitariosComponent extends Component {
        
        var $components = array('Session', 'RequestHandler');    
        
        function mesExtenso($mes){
            switch ($mes){
               case '01': $retorno = 'Janeiro'; break;
               case '02': $retorno = 'Fevereiro'; break;
               case '03': $retorno = 'Março'; break;
               case '04': $retorno = 'Abril'; break;
               case '05': $retorno = 'Maio'; break;
               case '06': $retorno = 'Junho'; break;
               case '07': $retorno = 'Julho'; break;
               case '08': $retorno = 'Agosto'; break;
               case '09': $retorno = 'Setembro'; break;
               case '10': $retorno = 'Outubro'; break;
               case '11': $retorno = 'Novembro'; break;
               case '12': $retorno = 'Dezembro'; break;
               default: $retorno = ''; break;
            }
            return $retorno;
        }
        
        function formataMoeda4($valor) {
            $valor = str_replace(',','.', str_replace('.','', $valor));

            return $valor;
        }
        
        function formataMoeda($valor) {
            if ( (substr($valor, -3, 1) == ',') || (substr($valor, -3, 1) == '.') ) {
                $centavos = substr($valor, -2);
                $milhar = substr($valor, 0, (strlen($valor) - 3));
            } elseif ( (substr($valor, -2, 1) == ',') || (substr($valor, -2, 1) == '.') ) {
                $centavos = substr($valor, -1).'0';
                $milhar = substr($valor, 0, (strlen($valor) - 2));
            } elseif ( (substr($valor, -1) == ',') || (substr($valor, -1) == '.') ) {
                $centavos = '00';
                $milhar = substr($valor, 0, (strlen($valor) - 1));
            } else {
                $centavos = '00';
                $milhar = $valor;
            }
            $milhar = str_replace('.','',$milhar);
            $milhar = str_replace(',','',$milhar);
            $valor = $milhar.'.'.$centavos;

            return $valor;
        }
        
        function formataData($date){
            //recebe o parâmetro e armazena em um array separado por -
            $date = explode('/', $date);

            $date = $date[2].'-'.$date[1].'-'.$date[0];

            //retorna a string com a data na ordem correta e formatada
            return $date;
        }
        
        function formataData2($date){
            //recebe o parâmetro e armazena em um array separado por -
            $date = explode('-', $date);

            $date = $date[2].'/'.$date[1].'/'.$date[0];

            //retorna a string com a data na ordem correta e formatada
            return $date;
        }
        
      function formataCampo($texto, $tamanho, $acresdireita=false, $caracter=' '){
          $string = substr($texto, 0, $tamanho);
          
          if($acresdireita){
              $string = str_pad($string, $tamanho, $caracter, STR_PAD_RIGHT); 
          }else{
              $string = str_pad($string, $tamanho, $caracter, STR_PAD_LEFT); 
          }
          
          return $string;
      }
            
      function soNumero($str) {
        return preg_replace("/[^0-9]/", "", $str);
    }
    
    function removeCaracteresEsp($str) {
        $str = preg_replace('/[áàãâä]/ui', 'a', $str);
        $str = preg_replace('/[éèêë]/ui', 'e', $str);
        $str = preg_replace('/[íìîï]/ui', 'i', $str);
        $str = preg_replace('/[óòõôö]/ui', 'o', $str);
        $str = preg_replace('/[úùûü]/ui', 'u', $str);
        $str = preg_replace('/[ç]/ui', 'c', $str);
        $str = preg_replace('/[´]/', '', $str);
        $str = str_replace('/', '_', $str);
                
        return strtoupper($str);
    }

    
    /**
	 * isCnpjValid
	 *
	 * Esta função testa se um Cnpj é valido ou não. 
	 *
	 * @author	Raoni Botelho Sporteman <raonibs@gmail.com>
	 * @version	1.0 Debugada em 27/09/2011 no PHP 5.3.8
	 * @param	string		$cnpj			Guarda o Cnpj como ele foi digitado pelo cliente
	 * @param	array		$num			Guarda apenas os números do Cnpj
	 * @param	boolean		$isCnpjValid	Guarda o retorno da função
	 * @param	int			$multiplica 	Auxilia no Calculo dos Dígitos verificadores
	 * @param	int			$soma			Auxilia no Calculo dos Dígitos verificadores
	 * @param	int			$resto			Auxilia no Calculo dos Dígitos verificadores
	 * @param	int			$dg				Dígito verificador
	 * @return	boolean						"true" se o Cnpj é válido ou "false" caso o contrário
	 *
	 */
	 
	 function isCnpjValid($cnpj){
		$num = '';
                //Etapa 1: Cria um array com apenas os digitos numéricos, isso permite receber o cnpj em diferentes formatos como "00.000.000/0000-00", "00000000000000", "00 000 000 0000 00" etc...
                $j=0;
                for($i=0; $i<(strlen($cnpj)); $i++)
                        {
                                if(is_numeric($cnpj[$i]))
                                        {
                                                $num[$j]=$cnpj[$i];
                                                $j++;
                                        }
                        }
                //Etapa 2: Conta os dígitos, um Cnpj válido possui 14 dígitos numéricos.
                if(count($num)!=14)
                        {
                                $isCnpjValid=false;
                                return $isCnpjValid;
                        }
                //Etapa 3: O número 00000000000 embora não seja um cnpj real resultaria um cnpj válido após o calculo dos dígitos verificares e por isso precisa ser filtradas nesta etapa.
                if ( $num[0]==0 && $num[1]==0 && $num[2]==0 && $num[3]==0 && $num[4]==0 && $num[5]==0 && $num[6]==0 && $num[7]==0 && $num[8]==0 && $num[9]==0 && $num[10]==0 && $num[11]==0)
                        {
                                $isCnpjValid=false;
                                return $isCnpjValid;
                        }
                //Etapa 4: Calcula e compara o primeiro dígito verificador.
                else
                        {
                                $j=5;
                                for($i=0; $i<4; $i++)
                                        {
                                                $multiplica[$i]=$num[$i]*$j;
                                                $j--;
                                        }
                                $soma = array_sum($multiplica);
                                $j=9;
                                for($i=4; $i<12; $i++)
                                        {
                                                $multiplica[$i]=$num[$i]*$j;
                                                $j--;
                                        }
                                $soma = array_sum($multiplica);	
                                $resto = $soma%11;			
                                if($resto<2)
                                        {
                                                $dg=0;
                                        }
                                else
                                        {
                                                $dg=11-$resto;
                                        }
                                if($dg!=$num[12])
                                        {
                                                $isCnpjValid=false;
                                                return $isCnpjValid;
                                        } 
                        }
                //Etapa 5: Calcula e compara o segundo dígito verificador.
                if(!isset($isCnpjValid))
                        {
                                $j=6;
                                for($i=0; $i<5; $i++)
                                        {
                                                $multiplica[$i]=$num[$i]*$j;
                                                $j--;
                                        }
                                $soma = array_sum($multiplica);
                                $j=9;
                                for($i=5; $i<13; $i++)
                                        {
                                                $multiplica[$i]=$num[$i]*$j;
                                                $j--;
                                        }
                                $soma = array_sum($multiplica);	
                                $resto = $soma%11;			
                                if($resto<2)
                                        {
                                                $dg=0;
                                        }
                                else
                                        {
                                                $dg=11-$resto;
                                        }
                                if($dg!=$num[13])
                                        {
                                                $isCnpjValid=false;
                                                return $isCnpjValid;
                                        }
                                else
                                        {
                                                $isCnpjValid=true;
                                        }
                        }
                //Trecho usado para depurar erros.
                /*
                if($isCnpjValid==true)
                        {
                                echo "<p><font color="GREEN">Cnpj é Válido</font></p>";
                        }
                if($isCnpjValid==false)
                        {
                                echo "<p><font color="RED">Cnpj Inválido</font></p>";
                        }
                */
                //Etapa 6: Retorna o Resultado em um valor booleano.
                return $isCnpjValid;			
        }
        
        
        /**
        * Cria arquivos compactados .zip
        * 
        * @author Luiz Otávio Miranda <contato@todoespacoonline.com/w>
        * @param string $path Caminho para o arquivo zip que será criado 
        * @param array $files Arquivos que serão adicionados ao zip 
        * @param bool $deleleOriginal Apaga os arquivos originais quando true 
        *
        * Exemplo de uso:
        *
        * $folder_name = 'pasta_com_arquivos';
        * $folder = glob($folder_name . '/*');
        * createZip( 'meu_arquivo.zip', $folder, false );
        */
       function createZip ( 
                            $path = 'arquivo.zip', 
                            $files = array(), 
                            $deleleOriginal = false 
                           ) {
            /**
            * Cria o arquivo .zip
            */
            $zip = new ZipArchive;
            $zip->open( $path, ZipArchive::CREATE);

            /**
            * Checa se o array não está vazio e adiciona os arquivos
            */
            if ( !empty( $files ) ) {
                /**
                * Loop do(s) arquivo(s) enviado(s) 
                */
                foreach ( $files as $file ) {                    
                    
                    /**
                    * Adiciona os arquivos ao zip criado
                    */
                    $zip->addFile( $file, basename( $file ) );
                    
                    /**
                    * Verifica se $deleleOriginal está setada como true,
                    * se sim, apaga os arquivos
                    */
                    if ( $deleleOriginal === true ) {
                        /**
                        * Apaga o arquivo
                        */
                        unlink( $file );

                        /**
                        * Seta o nome do diretório
                        */
                        $dirname = dirname( $file );
                        
                    }
                }

                /**
                * Verifica se $deleleOriginal está setada como true,
                * se sim, apaga a pasta dos arquivos
                */
                if ( $deleleOriginal === true && !empty( $dirname ) ) {
                    rmdir( $dirname );
                }
            }

            /**
            * Fecha o arquivo zip
            */
            $zip->close();
            
            header('Content-type: application/zip');
            header('Content-disposition: attachment; filename="'.basename($path).'"');
            readfile($path);
            unlink($path);
       } 
    
}
