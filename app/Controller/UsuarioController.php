<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsuarioController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Usuario', 'Endereco');

/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function index() {
		
		$this->set('title', 'Usuarios');
		
        $this->layout = 'lablayout';
		
        $Usuarios = $this->Usuario->findByIdusuario($this->Session->read('User.id'));		
		$this->set('Usuarios', $Usuarios);
		
		$Enderecos = $this->Endereco->find('all', array('conditions' => array('Endereco.situacao' => 'A', 'idUsuarioFK' => $this->Session->read('User.id'))));		
		$this->set('Enderecos', $Enderecos);
		
	}

	public function add() {
		
		if ($this->request->is('post')) {

			$this->Usuario->create();
			if ($this->Usuario->save($this->request->data)) {  //($this->Contrato->save($this->request->data)) {
				$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
			} else {

					$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
			} 


			return $this->redirect(array('action' => 'index'));

		}
		
	}


	public function edit() {

		$this->layout = 'lablayoutedit';
		
		if ($this->request->is('post')) {
			
			$this->request->data['Usuario']['idUsuario'] = $this->Session->read('User.id');
			$this->request->data['Usuario']['dtNascimento'] = date('Y-m-d', strtotime($this->request->data['Usuario']['dtNascimento']));
			
			if ($this->Usuario->save($this->request->data['Usuario'])) {  //($this->Contrato->save($this->request->data)) {
				$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
			} else {

				$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
			} 


			return $this->redirect(array('action' => 'index'));

		} else {

			$Usuarios = $this->Usuario->findByIdusuario($this->Session->read('User.id'));		
        	$this->set('Usuarios', $Usuarios);

		}
		
	}


	public function delete($id) {
		
		$this->Usuario->delete($id);

		return $this->redirect(array('action' => 'index'));
		
	}


	public function troca_senha() {

		$this->layout = 'lablayout';
		
		if ($this->request->is(array('post', 'put'))) {
                
			$User = $this->Usuario->find('first', array('conditions' => array('idUsuario' => $this->Session->read("User.id") )));
			
			if( ($this->Auth->password($this->request->data['User']['senha']) == $User["Usuario"]["password"]) and
				($this->request->data['User']['novaSenha'] == $this->request->data['User']['novaSenha2'])  ){                    
				
				$this->request->data['Usuario']['password'] = $this->Auth->password($this->request->data['User']['novaSenha']);
				$this->request->data['Usuario']['idUsuario'] = $this->Session->read("User.id");
				
				if ($this->Usuario->save($this->request->data)) {
					return $this->redirect(array('controller' => 'Painel', 'action' => 'index'));
					$this->Session->setFlash('<script> swal("Tudo certo!", "Sua senha foi alterada com sucesso.", "success"); </script>', 'default', array('class' => 'alert alert-success'));                        
					
				} else {                    
					$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default', array('class' => 'alert alert-warning'));					
					return $this->redirect(array('action' => 'troca_senha'));
				}
			}else{
				$this->Session->setFlash('<script> swal("Atenção!", "Informer sua senha atual corretamente!", "erro"); </script>', 'default', array('class' => 'alert alert-warning'));
				return $this->redirect(array('action' => 'troca_senha'));
			}
			
			
		} else {

			$Usuarios = $this->Usuario->findByIdusuario($this->Session->read('User.id'));		
        	$this->set('Usuarios', $Usuarios);

		}
		
	}

}
