<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ListaDesejosController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('ListaDesejos', 'Anuncios', 'Usuarios'); 
	public $components = array('Paginator');
        var $helpers = array('Time'); 

/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function index() {
		
		$this->set('title', 'ListaDesejos');

		$produtos = $this->Anuncios->find('all', array('conditions' => array('situacao' => 'A', 'idProdutos' => $this->ListaDesejos->find('list', array('conditions' => array('ListaDesejos.idUsuarioFK' => $this->Session->read('User.id') ) ))) ));
		$this->set('produtos', $produtos);
		
	}

	public function add($idProduto) {

		$this->autorender = false;

		$this->request->data['Lista']['idProdutoFK'] = $idProduto;			
		$this->request->data['Lista']['idUsuarioFK'] = $this->Session->read('User.id');				
		
		$this->ListaDesejos->create();
		if ($this->ListaDesejos->save($this->request->data['Lista'])) {  //($this->Contrato->save($this->request->data)) {
			$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
		} else {

			$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
		} 


		return $this->redirect(array('controller' => 'Pages', 'action' => 'view', $idProduto));
		
	}


	public function edit($id) {
		
		if ($this->request->is('post')) {
			
			$this->request->data['ListaDesejos']['idListaDesejos'] = $id;
			
			if ($this->ListaDesejos->save($this->request->data['ListaDesejos'])) {  //($this->Contrato->save($this->request->data)) {
				$this->Session->setFlash('<script> swal("Registro salvo com sucesso."); </script>', 'default');
			} else {

					$this->Session->setFlash('<script> swal("Atenção!", "Não foi possível alterar o registro. Por favor, tente novamente!"); </script>', 'default');
			} 


			return $this->redirect(array('action' => 'index'));

		}
		
	}


	public function remove($id, $idProduto) {
		
		$this->ListaDesejos->delete($id);

		return $this->redirect(array('controller' => 'Pages', 'action' => 'view', $idProduto));
		
	}

}
